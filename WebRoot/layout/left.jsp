<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<link rel="stylesheet" href="${ctx }/resources/style/style.css" media="screen" type="text/css" />
<script type="text/javascript">
	$(function() {
		
	});
</script>
	<div class="right_left" style="background: #F1F1F1;">
		<!-- Contenedor -->
	<ul id="accordion" class="accordion">
	<c:forEach var="top"   items="${activeUser.topMenuList}">
		<li>
			<div class="link"><i class="fa ${top.icon }"></i>${top.name }<i class="fa fa-chevron-down"></i></div>
			<ul class="submenu">
			<c:forEach var="children"   items="${activeUser.menuList}">
				<c:if test="${children.pid == top.id }">
				<li><a href="javascript:addTab('${children.name }', '${ctx}${children.url }');">${children.name }</a></li>
				</c:if>
			</c:forEach>
			</ul>
		</li>
	</c:forEach>
	</ul>
	</div>
<script src="${ctx }/resources/script/leftMemu.js"></script>