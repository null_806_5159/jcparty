<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	.right_tables{width:100%;padding: 15px 20px;}
	.every_title{border-bottom:1px solid #ccc; height:36px; line-height:36px; font-size:16px; color:#164281; font-weight:normal; margin-bottom:20px; margin-top:0; clear:both; text-align:left;}
	.cont_title{ border-bottom:1px solid #164281; display:inline-block;}
	.guide_table_div{margin-bottom:10px;}	
	.home_tab_list{border:1px solid #dfdfdf;border-collapse:collapse;width:100%;margin:0 auto;margin-top:20px}
	.home_tab_list th{border:1px solid #dfdfdf;background:#f3f9fe;color:#333;padding-top:15px;padding-bottom:15px;text-align:left;font-size:16px;width:30%;font-weight:normal; padding-left:2%;}
	.home_tab_list td{border:1px solid #dfdfdf;text-align:center;color:#333;font-size:14px;padding-top:12px;padding-bottom:12px}
	.home_tab_list td.altrow{background-color:#f5f9fc;}
	.home_tab_list td.base{background-color:#fff}
</style>
<div class="top_nav">
		<div class="nav_inner">
			<div class="top_logo">
				<font style="font-size: 25px;">金城学院</font><img src="${ctx }/resources/images/top_nav/line.png" width="1" height="22" style=" margin:4px 0px;"><font>党建信息管理系统</font>
			</div>
			<div class="nav_time">
				<img src="${ctx }/resources/images/top_nav/time_ico.png" width="20" height="20">
				<span id="localtime" style="color: #000;"></span>
				<script>
				//时间js
				function showLocale(objD)
				{
					var str,colorhead,colorfoot;
					var yy = objD.getYear();
					if(yy<1900) yy = yy+1900;
					var MM = objD.getMonth()+1;
					if(MM<10) MM = '0' + MM;
					var dd = objD.getDate();
					if(dd<10) dd = '0' + dd;
					var hh = objD.getHours();
					if(hh<10) hh = '0' + hh;
					var mm = objD.getMinutes();
					if(mm<10) mm = '0' + mm;
					var ss = objD.getSeconds();
					if(ss<10) ss = '0' + ss;
					var ww = objD.getDay();
					if  ( ww==0 )  colorhead="<font color=\"#FF0000\">";
					if  ( ww > 0 && ww < 6 )  colorhead="<font color=\"#373737\">";
					if  ( ww==6 )  colorhead="<font color=\"#008000\">";
					if  (ww==0)  ww="星期日";
					if  (ww==1)  ww="星期一";
					if  (ww==2)  ww="星期二";
					if  (ww==3)  ww="星期三";
					if  (ww==4)  ww="星期四";
					if  (ww==5)  ww="星期五";
					if  (ww==6)  ww="星期六";
					colorfoot="</font>"
					str = colorhead + yy + "-" + MM + "-" + dd + " " + hh + ":" + mm + ":" + ss + "  " + ww + colorfoot;
					return(str);
				}
				function tick()
				{
					var today;
					today = new Date();
					document.getElementById("localtime").innerHTML = showLocale(today);
					window.setTimeout("tick()", 1000);
				}
				tick();
					</script>
						</div>
						
						<div class="nav_set">
				<em>欢迎您:</em><font style="font-weight:bold; padding:0px 10px;">${activeUser.username }</font>
				<i class="left_i">[</i><a href="javascript:top_editPasswd();"><font>修改密码</font></a><i class="right_i">]</i>
				<i class="left_i">[</i><a href="javascript:top_downguide();"><font>操作指南</font></a><i class="right_i">]</i>
				<i class="left_i">[</i><a href="javascript:confirmDialog('确定退出该系统吗 ?','${ctx}/logout.action');"><font>注销登录</font></a><i class="right_i">]</i>
				<i class="left_i">[</i><a href="javascript:confirmDialog('确定返回首页吗 ?','${ctx}/index.action');"><font>返回首页</font></a><i class="right_i">]</i>
			</div>
		</div>
	</div>
	<div id="top_editPasswd_div" style="display: none;"></div>
	<div id="top_downguide_div" style="display: none;">
		<div class="right_tables">
			<div class="every_title">
				<span class="cont_title"><i class="fa fa-files-o" style="padding-right:5px;"></i>操作指南</span>
			</div>
			<div class="guide_table_div">
				<table class="home_tab_list">
				<tr>
					<th scope="row">
						<i class="fa fa-chain" style="padding-right:5px"></i>web操作指南
					</th>
					<td class="altrow">
				       <a href="javascript:top_downFile('5dade587-4607-11e7-b1cc-00163e1a7a5d');">党建系统操作指南</a>
					</td>
				</tr>
				</table>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		function confirmDialog(message,url){
			$.messager.confirm('确认',message,function(r){    
				if (r){    
					window.location.href=url; 
				}    
			}); 
		}
		
		function top_editPasswd(){
			$('#top_editPasswd_div').dialog({    
			    title: '修改密码',    
			    width: 600,    
			    height: 500,    
			    closed: false,    
			    cache: false,    
			    href: '${ctx}/userController/toEditPasswd.action',    
			    modal: true   
			});
		}
		
		//操作指南文件列表
		function top_downguide(){
			$('#top_downguide_div').dialog({    
			    title: ' ',    
			    width: 600,    
			    height: 500,    
			    closed: false,    
			    cache: false,    
			    modal: true   
			});
		}
		
		//操作指南
		function top_downFile(name){
			window.location.href="${ctx}/systemController/downloadFile.action?name="+name;
		}
	</script>