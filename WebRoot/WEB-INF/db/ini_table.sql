/*Table structure for table sys_permission */
DROP TABLE IF EXISTS sys_permission;
CREATE TABLE `sys_permission` (
  `id` varchar(36) COLLATE utf8_bin NOT NULL COMMENT '主键',
  `func_nm` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '权限名称',
  `func_desc` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '权限描述',
  `parentId` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '父级Id',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `creator` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
  `state` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '状态：1有效，0失效',
  `func_lever` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '1为一级菜单，2为二级菜单，3为页面url',
  `func_order` char(2) COLLATE utf8_bin DEFAULT NULL COMMENT '左侧菜单的排列顺序',
  `url` varchar(150) COLLATE utf8_bin DEFAULT NULL COMMENT '访问路径',
  `icon` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '小图标名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin


/*Table structure for table `sys_role` */
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(36) NOT NULL COMMENT '主键Id',
  `name` varchar(128) NOT NULL COMMENT '角色名称',
  `state` char(1) DEFAULT NULL COMMENT '是否可用,1：可用，0不可用',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `creator` varchar(36) DEFAULT NULL COMMENT '创建人',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8



/*Table structure for table `sys_role_permission` */

CREATE TABLE `sys_role_permission` (
  `id` varchar(36) NOT NULL,
  `sys_role_id` varchar(36) NOT NULL COMMENT '角色id',
  `sys_permission_id` varchar(36) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sys_user` */
DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `usercode` varchar(32) NOT NULL COMMENT '登录名',
  `username` varchar(64) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `token` varchar(20) DEFAULT NULL COMMENT '盐',
  `state` char(1) DEFAULT NULL COMMENT '1为有效0为注销',
  `createtime` date DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(36) DEFAULT NULL COMMENT '创建人',
  `counsellor` varchar(36) NOT NULL COMMENT '辅导员',
  `deptId` varchar(36) DEFAULT NULL COMMENT '所属系部',
  `majorId` varchar(36) DEFAULT NULL COMMENT '所属专业',
  `updator` varchar(36) DEFAULT NULL COMMENT '修改人',
  `updatetime` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8



/*Table structure for table `sys_user_role` */
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` varchar(36) NOT NULL,
  `sys_user_id` varchar(32) NOT NULL,
  `sys_role_id` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*部门信息*/
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department` (
  id VARCHAR(36) NOT NULL COMMENT '主键',
  NAME VARCHAR(128) NOT NULL COMMENT '部门名称',
  parentid VARCHAR(36) DEFAULT NULL COMMENT '上级部门ID',
  lever CHAR(2) DEFAULT NULL COMMENT '1为系部2为专业',
  state CHAR(1) DEFAULT NULL COMMENT '是否可用,1：可用，0不可用',
  PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*用户的基本信息*/
DROP TABLE IF EXISTS `jc_user_baseinfo`;
CREATE TABLE `jc_user_baseinfo` (
  `id` varchar(36) COLLATE utf8_bin NOT NULL COMMENT '主键Id',
  `userId` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '用户Id',
  `classNum` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '班级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
/*系统公告*/
CREATE TABLE `sys_notice` (
  `id` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '主键',
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '标题',
  `content` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '内容',
  `creator` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `reviewer` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '审核人',
  `reviewetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '审核时间',
  `review_result` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '审核结果',
  `state` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '状态：0失效1待审核2审核失败3已发布',
  `fit_dept` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '适用部门：通知适用的系部专业'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin

/**文章专题*/
CREATE TABLE `jc_news_type` (
  `id` varchar(36) COLLATE utf8_bin NOT NULL COMMENT '主键Id',
  `name` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '专题名称',
  `creator` varchar(36) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `state` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '状态：1有效0失效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
