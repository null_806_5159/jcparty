<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color:#000;}
</style>
<script type="text/javascript">
	$(function() {
		$('#deptList_deptTree').treegrid({    
		    url:'${ctx}/departmentController/getDeptList.action',    
		    idField:'id',
		    treeField:'name',
		    height:480,
		    onLoadSuccess: function () {
		    	$('#deptList_deptTree').treegrid('collapseAll')
		    },
		    columns:[[    
		        {title:'名称',field:'name',width:200},
		        {title:'编码',field:'deptNum',width:180},
		        {title:'创建时间',field:'createtime',width:180},
		        {title:'创建人',field:'creator',width:180},
		        {title:'管理员',field:'managerNm',width:180},
		        {field:'operate',title:'操作',width:150,align:'center',
		        	formatter: function(value,row,index){
		        		var html = "<div>";
	        			if(row.lever == '2'){
	        				html += "<a href=\"javascript:dept_delete('"+row.id+"')\">[删除]</a>　　<a href=\"javascript:dept_edit('"+row.id+"')\">[编辑]</a>　　<a href=\"javascript:dept_setMa('"+row.id+"')\">[设置负责人]</a></div>";
	        			}else{
	        				html += "<a href=\"javascript:dept_edit('"+row.id+"')\">[编辑]</a>　　<a href=\"javascript:dept_setMa('"+row.id+"')\">[设置负责人]</a></div>";
	        			}
						return html;
					}
		        }  
		    ]]    
		});  
		
	});
	
	//添加部门信息
	function addDept(){ 
		$('#deptList_addDiv').dialog({    
		    title: '添加部门',    
		    width: 600,    
		    height: 500,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/departmentController/toSaveDeptUI.action',    
		    modal: true   
		});
	}
	
	//修改部门信息
	function dept_edit(id){ 
		$('#deptList_addDiv').dialog({    
		    title: '修改部门信息',    
		    width: 600,    
		    height: 500,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/departmentController/toSaveDeptUI.action?id='+id,    
		    modal: true   
		});
	}
	
	//删除部门
	function dept_delete(id){
		$.messager.confirm('确认','您确认想要删除记录吗？',function(r){    
		    if (r){    
		        $.ajax({
		        	type:"post",
		        	url:"${ctx}/departmentController/deleteDept.action",
		        	data:{id:id},
		        	dataType:"json",
		        	success:function(msg){
		        		if(msg.result=='success'){
	                    	$.messager.alert('提示','操作成功！');
	                    	$("#deptList_deptTreeGrid").datatree("reload");
	                    }else{
	                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
	                    }
		        	}
		        })  
		    }    
		}); 
	}
	
	//设置负责人
	function dept_setMa(id){
		$("#deptList_setMa_ma").combobox({
			url:'${ctx}/departmentController/getDeptMa.action',    
		    valueField:'id',    
		    textField:'name',
		    onLoadSuccess:function(){
		    	$('#deptList_setMa').dialog({    
		    	    title: '部门负责人',    
		    	    width: 400,    
		    	    height: 200,    
		    	    closed: false,    
		    	    cache: false,        
		    	    modal: true   
		    	});
		    },
		    onSelect:function(record){
		    	var maId = record.id;
		    	$.ajax({
		    		type:"post",
		    		url:"${ctx}/departmentController/setDeptManager.action",
		    		data:{
		    			maId:maId,
		    			id:id
		    		},
		    		dataType:"json",
		    		success:function(msg){
		    			if(msg.result=='success'){
	                    	$.messager.alert('提示','操作成功！');
	                    	$('#deptList_deptTree').treegrid("reload");
	                    	$('#deptList_setMa').dialog("close");
	                    }else{
	                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
	                    }
		    		}
		    	})
		    }
		})
	}
</script>
<div>
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>系部专业列表</legend>
	</fieldset>
	<div style="padding-left: 90%;margin-bottom: 10px;">
		 <button onclick="addDept()" class="layui-btn layui-btn-small">添加</button>
	</div>
	<table id="deptList_deptTree"></table> 
</div>
<div id="deptList_addDiv" style="display: none;"></div>
<div id="deptList_setMa" style="display: none;padding: 20px 60px;">
	<input id="deptList_setMa_ma" type="text" style="height: 38px;width: 220px;"/>
</div>

