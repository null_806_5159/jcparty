<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		var parentId = "${dept.parentid}";
		var lever = "${dept.lever}";
		if(lever != '0'){
			$("#parentDept").show();
			$('#saveDept_pDept').combobox({    
			    url:'${ctx}/departmentController/getDeptByLever.action?lever='+(lever-1),    
			    valueField:'id',    
			    textField:'name',
			    onLoadSuccess:function(){
			    	if(parentId != null && parentId.length>0){
						$('#saveDept_pDept').combobox("setValue",parentId);
					}
			    }
			});
		}
	})
</script>
<div style="padding: 20px 30px;">
	<form id="saveDept_form" class="layui-form layui-form-pane" action="">
		<input type="hidden" name="id" value="${dept.id }">
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">名称</label>
	    <div class="layui-input-block">
	      <input type="text" name="name" value="${dept.name }" autocomplete="off" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">部门编号</label>
	    <div class="layui-input-block">
	      <input type="text" name="deptNum" value="${dept.deptNum }" autocomplete="off" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">部门类型</label>
	    <div class="layui-input-block">
	      <select lay-filter="deptlever" name="lever">
	        <option value="">==请选择==</option>
	        <option value="1" ${dept.lever=="1"? 'selected':''}>系部</option>
	        <option value="2" ${dept.lever=="2"? 'selected':''}>专业</option>
	      </select>
	    </div>
	  </div>
	  <div id="parentDept" class="layui-form-item"  style="display: none;">
	    <label class="layui-form-label" style="width: 112px;">上级部门</label>
	    <div class="layui-input-block">
	      	 <input type="text" id="saveDept_pDept" name="parentid" style="width: 263px;height: 38px;"/>
	    </div>
	  </div>
	  
	  <div class="layui-form-item  layui-form-text">
	     <label class="layui-form-label">部门描述</label>
	    <div class="layui-input-block">
	      <textarea placeholder="请输入内容" value="" name="dept_desc" class="layui-textarea">${dept.dept_desc }</textarea>
	    </div>
	  </div>
	  <div class="layui-form-item" style="text-align: center;margin-top: 40px;">
	    <a href="javascript:saveDept_save();" class="layui-btn">保存</a>
    	<a href="javascript:saveDept_back()" class="layui-btn layui-btn-normal">取消</a>
	  </div>
  </form>
</div>
<script type="text/javascript">
	layui.use('form', function(){
	  	var form = layui.form();
	  	form.render();
	  	form.on("select(deptlever)", function(data){
		  	switch(data.value)
			{
	    		case '1':
	    			$("#parentDept").show();
	    			getPDept('0');
	    			break;
	    		case '2':
	    			$("#parentDept").show();
	    			getPDept('1');
	    			break;
			}
	  	});
	});
	
	//保存提交
	function saveDept_save(){
		$.ajax({
			type:"post",
			url:"${ctx}/departmentController/saveDept.action",
			data:$("#saveDept_form").serialize(),
			dataType:"json",
			success:function(msg){
				if(msg.result=='success'){
	            	$.messager.alert('提示','操作成功！');
	            	window.parent.$('#deptList_deptTree').treegrid("reload");
	            	$('#deptList_addDiv').dialog("close");   
	            }else{
	            	$.messager.alert('提示','操作失败，请联系管理员！'); 
	            }
			}
		})
	}
	
	//返回
	function saveDept_back(){
		$.messager.confirm('确认','您确认想要退出当前编辑吗？',function(r){    
		    if (r){    
		    	$('#deptList_addDiv').dialog("close");
		    }    
		}); 
	}
	
	//获取上级部门
	function getPDept(lever){
		$('#saveDept_pDept').combobox({    
		    url:'${ctx}/departmentController/getDeptByLever.action?lever='+lever,    
		    valueField:'id',    
		    textField:'name'   
		});
	}
</script>