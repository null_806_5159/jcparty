<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color:#000;}
</style>
<script type="text/javascript">
	$(function() {
		$('#roleList').datagrid({  
	   		fitColumns:true,
	   		url:"${ctx}/roleController/getRoleDataGrid.action",
	   		height:440,
	   		idField:'id', 
    		pagination:true,
    		singleSelect:true,
    		rownumbers:true,
		    columns:[[  
		        {field:'id',title:'主键',hidden:true},  
		        {field:'name',title:'角色名称',width:150,align:'center'},  
		        {field:'creator',title:'创建人',width:150,align:'center'},  
		        {field:'createtime',title:'创建时间',width:150,align:'center',
		        	formatter: function(value,row,index){
						if(value.length>0){
							return value.substring(0,19);
						}else{
							return "";
						}
					}
		        },  
		        {field:'state',title:'状态',width:150,align:'center',
		        	formatter: function(value,row,index){
						if(value.length>0){
							if(value == '0'){
								return "<font style='color:red;'>无效</font>";
							}else if(value == '1'){
								return "<font style='color:green;'>有效</font>";
							}
						}else{
							return "";
						}
					}
		        },
		        {field:'remark',title:'备注',width:150},
		        {field:'operate',title:'操作',width:150,align:'center',
		        	formatter: function(value,row,index){
							return "<div>"
								  +"<a href='javascript:deleteRole(\""+row.id+"\")' style='cursor: pointer;'>[删除]</a>"
								  +"</div>";
					}
		        }  
	    	]] 
		});  
	});
	/*查询*/
	function queryData(){
		var roleNm = $("#roleList_roleNm").val();
		$("#roleList").datagrid("load",{
			roleNm:roleNm
		});
    } 
	
	/*重置*/
	function resetRoleList(){
		$("#roleList_roleNm").val("");
		$("#roleList").datagrid("reload",{
			roleNm:$('#roleList_roleNm').val()
		});
	}
	/*修改*/
	function editRole(){
		var rows = $('#roleList').datagrid('getSelected');
		if(!rows){
			$.messager.alert('提示','尚未选择要操作的行！'); 
		}else{
			var id = rows.id;
			$.ajax({
				type:"post",
				url:"${ctx}/roleController/getRoleInfoById.action",
				data:{id:id},
				dataType:"json",
				success:function(data){
					$("#roleList_saveId").val(data.id);
					$('#roleList_saveRoleNm').val(data.name);
					$("#roleList_saveRemark").val(data.remark);
					saveRole("角色修改");
				}
			})
		}
	}
	
	/*新建角色*/
	function addRole(){
		saveRole("新建角色");
	}
	/**根据角色Id删除*/
	function deleteRole(id){
		$.messager.confirm('确认','您确认想要删除角色吗？',function(r){    
		    if (r){    
		        $.ajax({
		        	type:"post",
		        	url:"${ctx}/roleController/deleteRoleById.action",
		        	data:{id:id},
		        	dataType:"json",
		        	success:function(data){
		        		if(data.result=='success'){
	                    	$.messager.alert('提示','操作成功！'); 
	                    }else{
	                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
	                    }
                    	resetRoleList();
		        	}
		        })
		    }    
		});
	}
	
	/*保存角色*/
	function saveRole(title){
		$('#roleList_saveRole').dialog({    
		    title: title,    
		    width: 400,    
		    height: 300,    
		    closed: false,
		    cache:false,
		    modal: true,
		    buttons:[{
				text:'保存',
				handler:function(){
					var id = $("#roleList_saveId").val();
					var name = $("#roleList_saveRoleNm").val();
					var remark = $("#roleList_saveRemark").val();
					$.ajax({
						type:"post",
						url:"${ctx}/roleController/saveRole.action",
						data:{
							id:id,
							name:name,
							remark:remark
						},
						dataType:"json",
						error: function(request) {
							$.messager.alert('提示','系统异常，请联系管理员！'); 
		                },
		                success: function(data) {
		                    if(data.result=='success'){
		                    	$.messager.alert('提示','操作成功！');
		                    	$('#roleList_saveRole').dialog("close");
		                    	resetRoleList();
		                    }else{
		                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
		                    }
		                    
		                }
						
					})
				}
			},{
				text:'关闭',
				handler:function(){
					$('#roleList_saveRole').dialog('close');  
				}
			}]
		});
	}
	//角色授权
	function authToRole(){
		var rows = $('#roleList').datagrid('getSelected');
		if(!rows){
			$.messager.alert('提示','尚未选择要操作的行！'); 
		}else{
			var id = rows.id;

			//创建tree
			$('#roleList_authTree_tree').tree({    
				url:'${ctx}/permissionController/getPermissionTree.action',
			    idField:'id',
			    treeField:'func_nm',
			    checkbox:true,
			    animate:true,
			    lines:true,
			    onLoadSuccess:function(node, data){
			    	//获取权限回显
			    	$.ajax({
						type:"post",
						url:"${ctx}/permissionController/getRolePeermission.action",
						data:{id:id},
						dataType:"json",
						success:function(result){
							for (var i = 0; i < result.length; i++) {
								var n = $('#roleList_authTree_tree').tree('find', result[i]);
								$('#roleList_authTree_tree').tree('check', n.target);
							}
						}
					})
			    }
			}); 
			$('#roleList_authTree').dialog({    
			    title: "角色授权",    
			    width: 400,    
			    height: 300,    
			    closed: false,
			    cache:false,
			    modal: true,
			    buttons:[{
					text:'保存',
					handler:function(){
						var nodes = $('#roleList_authTree_tree').tree('getChecked',['checked','indeterminate']);
						if(nodes.length>0){
							var s = '';
							for(var i=0; i<nodes.length; i++){
								if (s != '') s += ',';
								s += nodes[i].id;
							}
							$.ajax({
								type:"post",
								url:"${ctx}/roleController/authToRole.action",
								data:{
									funcIds:s,
									roleId:id
								},
								dataType:"json",
								success:function(msg){
									if(msg.result == 'success'){
										$.messager.alert('提示','操作成功！'); 
										$('#roleList_authTree').dialog("close");
									}
								}
							})
						}else{
							$.messager.alert('提示','至少选择一个权限！');    
						}
					}
				},{
					text:'关闭',
					handler:function(){
						$.messager.confirm('确认','确认退出授权页面吗？',function(r){    
						    if (r){    
						    	$('#roleList_authTree').dialog("close");
								var root=$("#roleList_authTree_tree").tree('getRoot');  
						        $("#roleList_authTree_tree").tree('uncheck',root.target);  
						    }    
						});  
						
					}
				}]
			});
		}
	}
</script>
<div style="background-color: #fafafa;">
	<form class="layui-form" action="" method="post">
	<div style="height: 20px;"></div>
		<div class="layui-form-item" style="height: 60px;">
		    <div class="layui-inline">
		      <label class="layui-form-label">角色名称</label>
		      <div class="layui-input-inline">
		        <input type="text" id="roleList_roleNm" name="roleNm" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		    <div class="layui-inline" style="float: right;">
			 	 <a href="javascript:queryData();" class="layui-btn layui-btn-primary layui-btn-radius"><i class="fa fa-search" style="margin-right: 10px;"></i>查询</a>
			  	 <a href="javascript:addRole();" class="layui-btn layui-btn-radius"><i class="fa fa-plus-circle" style="margin-right: 10px;"></i>新增</a>
				 <a href="javascript:resetRoleList();" class="layui-btn layui-btn-normal layui-btn-radius"><i class="fa fa-refresh" style="margin-right: 10px;"></i>重置</a>
				 <a href="javascript:authToRole();" class="layui-btn layui-btn-warm layui-btn-radius"><i class="fa fa-key" style="margin-right: 10px;"></i>授权</a>
				 <a href="javascript:editRole();" class="layui-btn layui-btn-danger layui-btn-radius"><i class="fa fa-edit" style="margin-right: 10px;"></i>修改</a>
		 	</div>
		 </div>
	</form>
	<table id="roleList"></table>
	</div>
<div id="roleList_saveRole" style="display: none;padding-top: 20px;">
	<form class="layui-form" action="">
		<input id="roleList_saveId" name="id" type="hidden" value="">
		<div class="layui-form-item">
		    <label class="layui-form-label">角色名称</label>
		    <div class="layui-input-inline">
		      <input type="text" id="roleList_saveRoleNm" name="name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
		    </div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">描述</label>
		    <div class="layui-input-inline">
		      <textarea id="roleList_saveRemark" name="remark" placeholder="请输入内容" class="layui-textarea"></textarea>
		    </div>
		</div>
	 </form>
</div>
<div id="roleList_authTree" style="display: none;">
	<div class="layui-form-item">
	<ul id="roleList_authTree_tree"></ul> 
</div>
</div>
<script>
layui.use('form', function(){
  var form = layui.form();
});
</script>