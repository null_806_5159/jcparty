<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color:#000;}
</style>
<script type="text/javascript">
	$(function(){
		$('#historyList_tableList').datagrid({  
			fitColumns:true,
			url:"${ctx}/thinkController/getThinkUploadList.action",
			height:470,
			singleSelect:true,
			idField:'id',
			rownumbers:true,
			pageSize:5,
			pageList:[5],
			pagination:true,
		    columns:[[   
		        {field:'title',title:'思想汇报标题',width:150,align:'center'},  
		        {field:'createtime',title:'上传时间',width:150,align:'center'},  
		        {field:'path',title:'内容',width:150,align:'center',
		            formatter:function(value,row,index){
		            	var pathArray = value.split(";");
		            	return '<img  height=\"80\" width=\"58\" src="/upload'+pathArray[0]+'" />';
		          	}
		        },  
		        {field:'operate',title:'操作',width:150,
		        	formatter: function(value,row,index){
							return "<div class=\"tableOpt\">"
								   +"<a href=\"javascript:showFullImag('"+row.id+"')\">[查看完整]</a>"
								   +"</div>";
					}
		        }  
			]] 
		});
		
		initDate('historyList_startDate','historyList_endDate');
	})
	
	//查看全部思想汇报扫描件
	function showFullImag(id){
		var url = "${ctx}/thinkController/toShowFullUI.action?id="+id;
		addTab('思想汇报详情', url)
	}
</script>
<div style="padding-left:30px;background: #fafafa;padding-right: 30px;">
  <fieldset class="layui-elem-field layui-field-title">
	  <legend>思想汇报上传记录</legend>
  </fieldset>
  <form id="historyList_query_form" class="layui-form" action="">
  	<div class="layui-form-item">
	    <div class="layui-inline">
	      <label class="layui-form-label">上传标题</label>
	      <div class="layui-input-inline">
	        <input type="text" name="title" id="historyList_title" lay-verify="required|number" autocomplete="off" class="layui-input">
	      </div>
	    </div>
	    <div class="layui-inline">
	      <label class="layui-form-label">时间区间</label>
	      <div class="layui-input-inline">
	        <input type="text" id="historyList_startDate" name="startDate" readonly autocomplete="off" class="layui-input">
	      </div>
	      <div class="layui-form-mid">-</div>
	      <div class="layui-input-inline">
	        <input type="text" id="historyList_endDate" name="endDate" readonly autocomplete="off" class="layui-input">
	      </div>
	    </div>
	    <div class="layui-inline">
	      	<a href="javascript:historyList_query();" class="layui-btn layui-btn-small">查询</a>
	      	<a href="javascript:historyList_reset();" class="layui-btn layui-btn-small">重置</a>
	    </div>
	  </div>
  </form>
  <table id="historyList_tableList"></table>
</div>
<script type="text/javascript">
	//查询
	function historyList_query(){
		$('#historyList_tableList').datagrid("load",{
			title:$("#historyList_title").val(),
			startDate:$("#historyList_startDate").val(),
			endDate:$("#historyList_endDate").val()
		})
	}
	
	//重置
	function historyList_reset(){
		$("#historyList_query_form")[0].reset();
		$('#historyList_tableList').datagrid("load",{
			title:$("#historyList_title").val(),
			startDate:$("#historyList_startDate").val(),
			endDate:$("#historyList_endDate").val()
		})
	}
</script>