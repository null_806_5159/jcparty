<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		var imagepath = "${think.path }";
		var pathArray = imagepath.split(";");
		var html = "";
		for (var i = 0; i < pathArray.length; i++) {
			html += "<div>"
				 + "<img height=\"600\" width=\"400\" src=\"/upload"+pathArray[0]+"\" />"
			     + "</div>";
		}
		$("#imageDiv").html("");
		$("#imageDiv").html(html);
	})
</script>
<div style="padding: 20px 30px;">
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>${think.title }</legend>
	</fieldset>
	<div id="imageDiv" style="margin: 10px 270px;">
	
	</div>
</div>