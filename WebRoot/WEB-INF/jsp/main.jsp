<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<!--inner右侧-->
		<div class="inner_right">
			<div class="inner_main">
				<!--待办事项-->
				<div class="square_inner">
					<div class="square_inner_title">
						<i class="fa fa-calendar-plus-o"></i>
						<font>审批事项</font>
						<span id="scheduleCount">3</span>
					</div>
					<div class="square_cont">
						<table class="square_cont_table" id="schedules">
							<tr class="base">
								<td>
									<i class="fa fa-circle"></i>
									<font title="查看详情" onclick="getDetial('20170106165806471e297081c8477','求职申请')" style="cursor: pointer;">
										[请假申请]因个人原因无法参加两学一做系列讲座
									</font>
									<span><img src="${ctx }/resources/images/new_ico.png" width="22" height="14"></span>
								</td>
								<td>申请人:聂卫星</td>
								<td>2017.01.06</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-circle"></i>
									<font title="查看详情" onclick="getDetial('201701061657462615d24507c88f2','求职申请')" style="cursor: pointer;">
										[党校申请]本人申请参加17年党校
									</font>
								</td>
								<td>申请人:聂卫星
								</td>
								<td>2017.01.06</td>
							</tr>
							<tr class="base">
								<td>
									<i class="fa fa-circle"></i>
									<font title="查看详情" onclick="getDetial('201701061657462615d24507c88f2','求职申请')" style="cursor: pointer;">
										[请假申请]因个人原因无法参加三严三实系列讲座
									</font>
								</td>
								<td>申请人:聂卫星
								</td>
								<td>2017.01.06</td>
							</tr>
						</table>
					</div>
				</div>
				<!--重要提醒-->
				<div class="square_inner" style="margin: 0 !important;width: 48% !important;">
					<div class="square_inner_title">
						<i class="fa fa-warning"></i>
						<font>系统公告</font>
						<span id="remindCount">3</span>
					</div>
					<div class="square_cont">
						<table class="square_cont_table" id="reminds">
							<tr class="base">
								<td>
									<i class="fa fa-circle"></i>
									<font title="查看详情" onclick="getDetial('20170106165806471e297081c8477','求职申请')" style="cursor: pointer;">
										[讲座通知]本周三在203开展两学一做系列讲座
									</font>
									<span><img src="${ctx }/resources/images/new_ico.png" width="22" height="14"></span>
								</td>
								<td>2017.01.06</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-circle"></i>
									<font title="查看详情" onclick="getDetial('201701061657462615d24507c88f2','求职申请')" style="cursor: pointer;">
										[入党推荐]经研究决定，推举一下几名同学加入中国共产党
									</font>
								</td>
								<td>2017.01.06</td>
							</tr>
							<tr class="base">
								<td>
									<i class="fa fa-circle"></i>
									<font title="查看详情" onclick="getDetial('201701061657462615d24507c88f2','求职申请')" style="cursor: pointer;">
										[通报批评]xxx因多次缺席先关活动，党组织将不予以入党考虑
									</font>
								</td>
								<td>2017.01.06</td>
							</tr>
						</table>
					</div>
				</div>
				
				<!-- 统计图表  共10个系部-->
				<div id="pieArea" style="height:400px;width:98%; border:1px solid #d7d7d7;margin-top: 270px;"></div>
				
			</div>
		</div>
<script type="text/javascript">
	//路径配置
	require.config({
	    paths: {
	        echarts: '${ctx}/resources/plug-in/echarts-2.2.7/build/dist'
	    }
	});

	// 使用
	require(
    	[
        	'echarts',
        	'echarts/chart/pie' // 使用环形图
    	],
    	function (ec) {
        	// 基于准备好的dom，初始化echarts图表
        	var myChart = ec.init(document.getElementById('pieArea')); 
        
        	/****************************************/
        	var labelTop = {//上层样式
			    normal : {
			        label : {
			            show : true,
			            position : 'center',
			            formatter : '{b}',
			            textStyle: {
			                baseline : 'bottom'
			            }
			        },
			        labelLine : {
			            show : false
			        }
			    }
			};
			var labelFromatter = {//环内样式
			    normal : {
			        label : {
			            formatter : function (params){
			                return 100 - params.value + '%'
			            },
			            textStyle: {
			                baseline : 'top'
			            }
			        }
			    },
			}
			var labelBottom = {
			    normal : {
			        color: '#ccc',
			        label : {
			            show : true,
			            position : 'center'
			        },
			        labelLine : {
			            show : false
			        }
			    },
			    emphasis: {
			        color: 'rgba(0,0,0,0)'
			    }
			};
			var radius = [40, 55];//内外半径百分比
			var x = ['0%','20%','40%','60%','80%','0%','20%','40%','60%','80%'];
	        var option = {
	        		 legend: {
	        		        x : 'center',
	        		        y : 'center',
	        		        data:[
	        		            '机电工程系','信息工程系','自动化系','英语系','经济系',
	        		            '管理系', '民用航空系', '土木工程系', '艺术系', '车辆工程系'
	        		        ]
	        		    },
	        		    title : {
	        		        text: '金城学院各系部入党情况',
	        		        x: 'center'
	        		    },
	        		    
	        		    series : [
	        		       
	        		        {
	        		            type : 'pie',
	        		            center : ['30%', '30%'],
	        		            radius : radius,
	        		            x:x, // for funnel
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:56, itemStyle : labelBottom},
	        		                {name:'机电工程系', value:44,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['50%', '30%'],
	        		            radius : radius,
	        		            x:x, // for funnel
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:65, itemStyle : labelBottom},
	        		                {name:'信息工程系', value:35,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['70%', '30%'],
	        		            radius : radius,
	        		            x:x, // for funnel
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:70, itemStyle : labelBottom},
	        		                {name:'自动化系', value:30,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['90%', '30%'],
	        		            radius : radius,
	        		            x:x, // for funnel
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:73, itemStyle : labelBottom},
	        		                {name:'英语系', value:27,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['10%', '70%'],
	        		            radius : radius,
	        		            y: '55%',   // for funnel
	        		            x: x,    // for funnel
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:78, itemStyle : labelBottom},
	        		                {name:'经济系', value:22,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['30%', '70%'],
	        		            radius : radius,
	        		            y: '55%',   // for funnel
	        		            x:x,    // for funnel
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:78, itemStyle : labelBottom},
	        		                {name:'管理系', value:22,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['50%', '70%'],
	        		            radius : radius,
	        		            y: '55%',   // for funnel
	        		            x:x, // for funnel
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:78, itemStyle : labelBottom},
	        		                {name:'民用航空系', value:22,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['70%', '70%'],
	        		            radius : radius,
	        		            y: '55%',   // for funnel
	        		            x:x, // for funnel
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:83, itemStyle : labelBottom},
	        		                {name:'土木工程系', value:17,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['90%', '70%'],
	        		            radius : radius,
	        		            y: '55%',   // for funnel
	        		            x:x,
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:89, itemStyle : labelBottom},
	        		                {name:'艺术系', value:11,itemStyle : labelTop}
	        		            ]
	        		        },
	        		        {
	        		            type : 'pie',
	        		            center : ['10%', '30%'],
	        		            radius : radius,
	        		            x: x, 
	        		            itemStyle : labelFromatter,
	        		            data : [
	        		                {name:'other', value:46, itemStyle : labelBottom},
	        		                {name:'车辆工程系', value:54,itemStyle : labelTop}
	        		            ]
	        		        }
	        		    ]      
	        }
	    myChart.hideLoading();  
        myChart.setOption(option);
        /****************************************/
    }
   )
</script>