<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>金城党建</title>
<link rel="shortcut icon" href="" />
<link href="${ctx }/resources/style/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="${ctx }/resources/plug-in/layui/css/layui.css">
<link href="${ctx }/resources/style/global.css" rel="stylesheet" />
<script type="text/javascript" src="${ctx }/resources/plug-in/easyUI/jquery.min.js"></script>
<link rel="stylesheet" href="${ctx}/resources/plug-in/fontawesome/css/font-awesome.css"> 
<script src="${ctx }/resources/plug-in/layui/layui.js"></script>
<link href="${ctx }/resources/style/homeindex.css" rel="stylesheet" />
<link href="${ctx }/resources/style/slide.css" rel="stylesheet" />
<script src="${ctx }/resources/script/slide.js"></script>
<style type="text/css">
/*滚动条样式 start*/
::-webkit-scrollbar {
	width: 0.2em;
}

::-webkit-scrollbar:horizontal {
	height: 0.2em;
}
/*滚动条样式 end*/
</style>
</head>
<script type="text/javascript">
/*跳转到后台管理页面*/
function toManageSys(){
	//window.location.href="${ctx}/manage.action";
	window.location.href="${ctx}/manage.action";
}

function toLoginSys(){
	window.location.href="${ctx}/login.action";
}
</script>
<!-- <body onselectstart="return false;" oncontextmenu="return false;" > -->
<body>
<div>
    <div id="show" rel="autoPlay">
      <div class="img">
          <span>
              <a href="#"><img src="${ctx }/resources/images/lunbo/20170408181238_82987.jpg" width="960" height="320px;"/></a>
              <a href="#"><img src="${ctx }/resources/images/lunbo/20170408181316_26188.jpg" width="960" height="320px;"/></a>
              <a href="#"><img src="${ctx }/resources/images/lunbo/20170408181336_23227.jpg" width="960" height="320px;"/></a>
          </span>
        <div class="masks mk1"></div>
        <div class="masks mk2"></div>
      </div>
    </div>
  </div>
<div class="blog-body">
<div class="blog-container">
    <div class="blog-main">
        <!--左边文章列表-->
        <div class="blog-main-left">
        	<div id="newsList_div">
        	<c:if test="${newsList!= null && fn:length(newsList) > 0}">
        	<c:forEach var="news"   items="${newsList}">
                <div class="article shadow">
                    <div class="article-left">
                        <img src="/upload${news.path }" alt="" />
                    </div>
                    <div class="article-right">
                        <div class="article-title">
                               <!--  <i class="icon-stick">顶</i> -->
                            <a href="javascript:showNewsInfo('${news.id }')">${news.title }</a>
                        </div>
                        <div class="article-abstract">
                            
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="article-footer">
                        <span><i class="fa fa-clock-o"></i>&nbsp;&nbsp;${fn:substring(news.createtime, 0, 19)}</span>
                        <span class="article-author"><i class="fa fa-user"></i>&nbsp;&nbsp;${news.creator }</span>
                        <span><i class="fa fa-tag"></i>&nbsp;&nbsp;<a href="/Article/Category/LY02212035253489">${news.type }</a></span>
                    </div>
                </div>
                </c:forEach>
               </c:if>
                </div> 
            <!-- <div id="paging" style="margin-left: 300px;"></div> -->
        </div>
        
        <!--右边小栏目-->
        <div class="blog-main-right">
            <div class="blogerinfo shadow" style="height: 183px;">
                <p class="blogerinfo-nickname">南京航空航天大学金城学院<br/>党建管理系统</p>
                <c:if test="${activeUser != null }">
                	<button class="layui-btn layui-btn-primary layui-btn-radius" onclick="toManageSys()">访问后台</button>
                </c:if>
                <c:if test="${activeUser == null }">
                	<button class="layui-btn layui-btn-primary layui-btn-radius" onclick="toLoginSys()">登录后台</button>
                </c:if>
                
            </div>
            <div></div><!--占位-->
            <div class="blog-module shadow">
                <div class="blog-module-title">相关专题</div>
                <ul class="fa-ul blog-module-ul">
                <c:if test="${newsTypeList!= null && fn:length(newsTypeList) > 0}">
        			<c:forEach var="type"   items="${newsTypeList}">
                        <li><i class="fa-li fa fa-hand-o-right"></i><a href="javascript:getTypeNewsList('${type.id }')">${type.name}</a></li>
                 </c:forEach>
               </c:if>
                </ul>
            </div>
            <div class="blog-module shadow">
                <div class="blog-module-title">友情链接</div>
                <ul class="blogroll">
                    <li><a target="_blank" href="http://jc.nuaa.edu.cn/" title="页签">南京航空航天大学金城学院</a></li>
                    <li><a target="_blank" href="http://jcjx.nuaa.edu.cn/EaWeb/Manager/HomePage/Index.aspx" title="Layui">金城教务</a></li>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>
    <footer class="blog-footer">
        <p><span>Copyright</span><span>&copy;</span><span>2017</span><span>南京航空航天大学金城学院</span></p>
        <p><a href="#">信息工程系</a></p>
    </footer>
    <!--遮罩-->
    <div class="blog-mask animated layui-hide"></div>
<script type="text/javascript">
	layui.use(['laypage', 'layer'], function(){
	  var laypage = layui.laypage
	  ,layer = layui.layer;
	  laypage({
		    cont: 'paging'
		    ,pages: 5
		    ,groups: 0
		    ,first: false
		    ,last: false
		    ,jump: function(obj, first){
		      if(!first){
		        var page = obj.curr;
		        /* $.ajax({
		        	type:"post"
		        }) */
		      }
		    }
		  });
	});
	
	//获取专题下的文章列表
	function getTypeNewsList(id){
		$.ajax({
			type:"post",
			url:"${ctx}/newsController/getNewsListBytype.action",
			data:{typeId:id},
			dataType:"json",
			success:function(data){
				if(data.length>0){
					var html = "";
					for(var i=0;i<data.length;i++){
						html += "<div class=\"article shadow\"><div class=\"article-left\">";
						html += "<img src=\"/upload"+data[i].path+"\" alt=\"\" /></div><div class=\"article-right\">";
						html += "<div class=\"article-title\"><a href=\"javascript:showNewsInfo('"+data[i].id+"')\">"+data[i].title+"</a>";
						html += "</div><div class=\"article-abstract\"></div></div><div class=\"clear\"></div><div class=\"article-footer\">";
						html += "<span><i class=\"fa fa-clock-o\"></i>&nbsp;&nbsp;"+data[i].createtime.substring(0,19)+"</span>";
						html += "<span class=\"article-author\"><i class=\"fa fa-user\"></i>&nbsp;&nbsp;"+data[i].creator+"</span>";
						html += "<span><i class=\"fa fa-tag\"></i>&nbsp;&nbsp;<a href=\"/Article/Category/LY02212035253489\">"+data[i].type+"</a></span>";
						html += "</div></div>";
					}
					
					$("#newsList_div").html("");
					$("#newsList_div").html(html);	
				}else{
					$("#newsList_div").html("");
					layer.msg('该专题没有相关文章材料！');
				}
			},
			error:function(){
				layer.msg('系统异常，请联系管理员！');
			}
		})
	}
	
	//查看文章的详情
	function showNewsInfo(id){
		window.location.href="${ctx}/newsController/showIndexNewsDetails.action?id="+id;
	}
	
</script>
</body>
</html>  