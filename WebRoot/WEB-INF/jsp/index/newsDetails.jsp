<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>金城党建</title>
<link rel="shortcut icon" href="" />



<link href="${ctx }/resources/style/animate.min.css" rel="stylesheet" />
<link rel="stylesheet" href="${ctx }/resources/plug-in/layui/css/layui.css">
<link href="${ctx }/resources/style/global.css" rel="stylesheet" />
<script type="text/javascript" src="${ctx }/resources/plug-in/easyUI/jquery.min.js"></script>
<link rel="stylesheet" href="${ctx}/resources/plug-in/fontawesome/css/font-awesome.css"> 
<script src="${ctx }/resources/plug-in/layui/layui.js"></script>
<link href="${ctx }/resources/style/homeindex.css" rel="stylesheet" />
<link href="${ctx }/resources/style/slide.css" rel="stylesheet" />
<link href="${ctx }/resources/style/articledetail.css" rel="stylesheet" />


<script src="${ctx }/resources/script/slide.js"></script>
<script type="text/javascript" src="${ctx}/resources/plug-in/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/resources/plug-in/ueditor/ueditor.all.js"></script>
<script type="text/javascript">
	UE.delEditor("newsDetails_content");//在页面每次初始化的时候，先删除掉以前的编辑器，再次进行初始化,否则会出现渲染失败的情景
	var ue = UE.getEditor('newsDetails_content', {
	    toolbars: [],
	    autoHeightEnabled: true,
	    autoFloatEnabled: true,
	    wordCount:false,
	    elementPathEnabled:false,
	    zIndex :900
	});
	ue.addListener('ready', function () {
        ue.setDisabled();
    });
</script>
<style type="text/css">
/*滚动条样式 start*/
::-webkit-scrollbar {
	width: 0.2em;
}

::-webkit-scrollbar:horizontal {
	height: 0.2em;
}
/*滚动条样式 end*/
</style>
</head>
<body>
<div>
    <div id="show" rel="autoPlay">
      <div class="img">
          <span>
              <a href="#"><img src="${ctx }/resources/images/lunbo/20170408181238_82987.jpg" width="960" height="320px;"/></a>
              <a href="#"><img src="${ctx }/resources/images/lunbo/20170408181316_26188.jpg" width="960" height="320px;"/></a>
              <a href="#"><img src="${ctx }/resources/images/lunbo/20170408181336_23227.jpg" width="960" height="320px;"/></a>
          </span>
        <div class="masks mk1"></div>
        <div class="masks mk2"></div>
      </div>
    </div>
  </div>
<div class="blog-body">
	<div class="blog-container">
    	<div class="blog-main">
        	
<div style="background: #fafafa;">
	<div class="blog-body" style="height: 100%;width:100%; margin-top: 0px;">
		<div class="blog-container">
			<div class="blog-main" style="text-align: center;">
				<div>
					<div class="article-detail shadow">
						<div>
							<img src="/upload${news.path }" width="400px" height="300px">
						</div>
						<div class="article-detail-title">${news.title }</div>
						<div class="article-detail-info">
							 <span>发布人：${news.creatorNm }</span><font style="margin: 0px 10px;">|</font><span>编辑时间：${fn:substring(news.createtime, 0, 19)}</span>
						</div>
						<!-- 文章内容 -->
						<div id="newsDetails_content" style="width: 70%;margin-left: 13%;">${news.content }</div>
					</div>
				</div>
			</div>
		</div>
		
		<div style="margin-left: 40%;margin-bottom: 50px;">
			<button class="layui-btn" onclick="javascript:history.go(-1);">返回</button>
		</div>
	</div>
</div>
        	
        </div>
    </div>
</div>
</body>
</html>  