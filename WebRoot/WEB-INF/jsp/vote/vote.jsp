<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<link href="${ctx }/resources/style/vote.css" rel="stylesheet" />
<div style="background: #f9f9f9; padding: 10px 30px;">
	<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
	  <legend>2013级信息工程系党员选举</legend>
	</fieldset>
	<form id="vote_form" class="layui-form" action="">
		<input id="vote_voteId" type="hidden" value="${voteId }">
		<c:if test="${(optionList)!= null && fn:length(optionList) > 0}">
  			<c:forEach var="option" items="${ optionList}" varStatus="status">
				<div class="vote shadow">
					<div class="vote-left">
						<img src="/upload${option.path }" />
					</div>
					<div class="vote-right">
						<div class="vote-title">
							<i class="icon-stick">${ status.index + 1}</i> 
							<a>${option.candition }</a>
						</div>
						<div class="vote-abstract">
							${option.remark }
						</div>
						<div class="vote-footer">
							<input type="checkbox" name="voteResult" value="${option.id }" title="投一票">
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</c:forEach>
  		</c:if>
  		<c:if test="${type == '1' }">
  			<a class="layui-btn" href="javascript:vote_saveVote();">投票</a>
  		</c:if>
  		<c:if test="${type == '2' }">
  			<a class="layui-btn layui-btn-disabled">已投票</a>
  		</c:if>
    	<a class="layui-btn layui-btn-primary" href="javascript:vote_back();">返回</a>
	</form>
</div>
<script>
	layui.use('form', function() {
		var form = layui.form();
		form.render();
	});
	
	/* 保存投票结果 */
	function vote_saveVote(){
		var chk_value =[];
	    $('input[name="voteResult"]:checked').each(function(){
	       chk_value.push($(this).val());
	    });//获取到已选择的值
	    if(chk_value.length==0){
	    	$.messager.confirm('确认','您确认弃权？',function(r){    
	    	    if (r){    
	    	        alert('已提交');    
	    	        return false;
	    	    }    
	    	});  
	    }
	    if(chk_value.length > 3 ){
	    	$.messager.alert('提示','每人最多投3票！');
	    	return false;
	    }
	    
	    var chk_valueIds = "";
	    var voteId = $("#vote_voteId").val();
	    for (var i = 0; i < chk_value.length; i++) {
	    	if(i != (chk_value.length-1)){
	    		chk_valueIds += chk_value[i]+",";
			}else{
				chk_valueIds += chk_value[i];
			}
		}
	    $.ajax({
	    	type:"post",
	    	url:"${ctx}/voteController/saveVoteResult.action",
	    	data:{
	    		chk_valueIds:chk_valueIds,
	    		voteId:voteId
	    	},
	    	dataType:"json",
	    	success:function(msg){
	    		if(msg.result=='success'){
	    			$.messager.alert('提示','操作成功！'); 
	    			$('#myVote_voteDiv').dialog("close");
	    			refreshTab("${ctx}/voteController/toMyVote.action")
                }else{
                	$.messager.alert('提示','操作失败，请联系管理员！'); 
                }
	    	},
	    	error:function(){
	    		$.messager.alert('提示','系统异常，请联系管理员！'); 
	    	}
	    	
	    })
	}
	
	//返回
	function vote_back(){
		$.messager.confirm('确认','您确认想要退出操作吗？',function(r){    
		    if (r){    
		    	$('#myVote_voteDiv').dialog("close"); 
		    }    
		}); 
	}
</script>