<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color: #000;}
</style>
<script type="text/javascript">
	$("#myapply_applyList").datagrid({
		fitColumns : true,
		url : "${ctx }/voteController/getVoteList.action",
		singleSelect : true,
		height : 390,
		idField : 'id',
		toolbar: '#myVote_tools',
		rownumbers : true,
		pagination : true,
		columns : [ [ {
			field : 'title',
			title : '标题',
			width : 150,
			align : 'center'
		}, {
			field : 'applydate',
			title : '申请时间',
			width : 150,
			align : 'center'
		}, {
			field : 'acceptorNm',
			title : '受理人',
			width : 150,
			align : 'center'
		}, {
			field : 'result',
			title : '受理结果',
			width : 150,
			align : 'center'
		}, {
			field : 'operate',
			title : '操作',
			width : 150,
			align : 'center',
			formatter : function(value, row, index) {
				return "<span><a style='cursor: pointer;'>[我的选择]</a>　　<a style='cursor: pointer;'>[投票结果]</a></span>";
			}
		} ] ]
	});
</script>
<div style="padding: 15px 30px;">
	<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
	  <legend>等待投票</legend>
	</fieldset>
	<div class="layui-form">
		  <table class="layui-table">
		    <colgroup>
		      <col width="30%">
		      <col width="20%">
		      <col width="30%">
		      <col width="20%">
		    </colgroup>
		    <thead>
		      <tr>
		        <th style="text-align: center;">投票主题</th>
		        <th style="text-align: center;">发起时间</th>
		        <th style="text-align: center;">投票描述</th>
		        <th style="text-align: center;">操作</th>
		      </tr> 
		    </thead>
		    <tbody>
		     <c:if test="${(voteList)!= null && fn:length(voteList) > 0}">
  				<c:forEach var="vote"   items="${ voteList}">
			      <tr>
			        <td>${vote.title }</td>
			        <td>${fn:substring(vote.releasetime, 0, 19)}</td>
			        <td>${vote.vote_desc }</td>
			        <td style="text-align: center;">
				      	<a href="javascript:myVote_abstained('${vote.id }');" style="cursor: pointer;text-decoration:none;">[弃权]</a>　　
				      	<a href="javascript:myVote_vote('${vote.id }');" style="cursor: pointer;text-decoration:none;">[开始投票]</a>
				    </td>
			      </tr>
		        </c:forEach>
  			</c:if>
		    </tbody>
		  </table>
	</div>
	<%-- <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
	  <legend>往期投票</legend>
	</fieldset>
	<div style="width: 100%;">
	<table id="myapply_applyList"></table>
	</div>
	<div id="myVote_tools" style="padding-top: 15px;">
			<form id="myapply_query_form" class="layui-form"
				action="${ctx }/applicationController/getApplyList.action">
				<div class="layui-form-item">
				    <div class="layui-inline">
				      <label class="layui-form-label">投票主题</label>
				      <div class="layui-input-inline">
				        <input type="text" name="title" class="layui-input">
				      </div>
				    </div>
				    <div class="layui-inline">
						<a href="javascript:myapply_query();" class="layui-btn  layui-btn-small">查询</a> 
					</div>
				  </div>
			</form>
		</div> --%>
</div>
<div id="myVote_voteDiv" style="display: none;"></div>
<script type="text/javascript">
	layui.use('form', function() {
		var form = layui.form();
		form.render();
	});
	
	//弃权投票
	function myVote_abstained(id){
		$.messager.confirm('确认','您确认想要弃权该投票吗？',function(r){    
		    if (r){    
		    	$.ajax({
		    		type:"post",
		    		url:"${ctx}/voteController/abstained.action",
		    		data:{id:id},
		    		dataType:"json",
		    		success:function(msg){
		    			if(msg.result=='success'){
			    			$.messager.alert('提示','操作成功！'); 
			    			refreshTab("${ctx}/voteController/toMyVote.action")
		                }else{
		                	$.messager.alert('提示','操作失败，请联系管理员！'); 
		                }
		    		}
		    	})    
		    }    
		});  
	}
	
	//投票
	function myVote_vote(id){
		$('#myVote_voteDiv').dialog({    
    	    title: '我的投票',    
    	    width: 800,    
    	    height: 600,    
    	    closed: false,    
    	    cache: false,    
    	    href: '${ctx}/voteController/toVote.action?id='+id,    
    	    modal: true   
    	});  
	}
</script>