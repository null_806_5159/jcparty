<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		$("#addCandidate_nm").combobox({
			 url:'${ctx}/voteController/getVoteOption.action',    
			 valueField:'id',    
			 textField:'name'
		})
	})
</script>
<div style="padding: 10px 30px;">
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>添加候选人</legend>
	</fieldset>
	<form class="layui-form layui-form-pane" action="">
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">候选人</label>
	    <div class="layui-input-block">
	      <input type="text" id="addCandidate_nm" style="height: 38px;width: 220px;"/>
	    </div>
	  </div>
	  <div class="layui-form-item layui-form-text">
	    <label class="layui-form-label">竞选宣言</label>
	    <div class="layui-input-block">
	      <textarea placeholder="请输入" id="candidateDesc" class="layui-textarea"></textarea>
	    </div>
	  </div>
	  <div class="layui-form-item" style="text-align: center;">
	   	<a href="javascript:addCandidate_saveCandidate();" class="layui-btn layui-btn-small">确认</a>
	   	<a href="javascript:addCandidate_back();" class="layui-btn layui-btn-small">返回</a>
	  </div>
	 </form>
</div>
<script type="text/javascript">
	//返回
	function addCandidate_back(){
		$.messager.confirm('确认','您确认退出当前操作吗？',function(r){    
		    if (r){    
		    	$('#addCandidate_div').dialog("close");
		    }    
		}); 
	}
	
	//保存所选的候选人
	function addCandidate_saveCandidate(){
		var candidateId = $("#addCandidate_nm").combobox("getValue");
		var candidateNm = $("#addCandidate_nm").combobox("getText");
		var candidateDesc = $("#candidateDesc").val();
		if(candidateId == null || candidateId.length==0){
			$.messager.alert('提示','请选择候选人！');
			return false;
		}
		if(candidateDesc == null || candidateDesc.length==0){
			$.messager.alert('提示','请选择候选人竞选宣言！');
			return false;
		}
		window.parent.saveVote_setCandidate(candidateId,candidateNm,candidateDesc);
		$('#addCandidate_div').dialog("close");
	}
</script>