<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		$("#vote_dept").combobox({
			url:'${ctx}/departmentController/getDeptCombobox.action',    
		    valueField:'id',    
		    textField:'name'   
		})
		
		var start = {  
	        elem: "#saveVote_invalidtime", //选择ID为START的input  
	        format: 'YYYY-MM-DD', //自动生成的时间格式  
	        min: laydate.now(), //最大日期  
	        istime: true, //必须填入时间  
	        istoday: false
		}; 
		laydate(start);  
	})
</script>
<div style="padding: 20px 30px;background: #fafafa;">
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>编辑投票</legend>
	</fieldset>
	<form class="layui-form layui-form-pane" action="">
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">投票主题</label>
	    <div class="layui-input-block">
	      <input type="text" name="title" autocomplete="off" style="width: 54%;" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	   <div class="layui-form-item">
	    <div class="layui-inline">
	      <label class="layui-form-label" style="width: 112px;">适用系部</label>
	      <div class="layui-input-inline">
	        <input type="text" id="vote_dept" name="dept" style="height: 38px;width: 220px;"/>
	      </div>
	    </div>
	    <div class="layui-inline" style="margin-left: 20px;">
	      <label class="layui-form-label" style="width: 112px;">失效时间</label>
	      <div class="layui-input-inline">
	        <input type="text" id="saveVote_invalidtime" name="invalidtime" class="layui-input">
	      </div>
	    </div>
	  </div>
	  <div class="layui-form-item layui-form-text">
	    <label class="layui-form-label">描述</label>
	    <div class="layui-input-block">
	       <textarea placeholder="请输入内容" name="vote_desc" class="layui-textarea"></textarea>
	    </div>
	  </div>
	  <fieldset class="layui-elem-field">
		  <legend>内容 <a href="javascript:saveVote_addCandidate();" class="layui-btn layui-btn-small">添加候选人</a></legend>
		  <input type="hidden" id="saveVote_ids" name="voteIds" value="" />
		  <div class="layui-field-box" id="saveVote_candidate">
				<!-- <fieldset class="layui-elem-field">
					<legend></legend>
					<div class="layui-field-box">
						<div class="layui-form-item">
							<label class="layui-form-label" style="width: 230px;"><i class='fa fa-user'></i>候选人：<font style="font-size: 14px;font-weight: 1px;">[2013023324]聂卫星</font></label>
							<a style="margin-left: 5px; margin-top: 5px;" href="javascript:saveVote_deleteCandidate();"
								class="layui-btn layui-btn-warm layui-btn-small">删除</a>
						</div>
						<div class="layui-form-item layui-form-text">
							<label class="layui-form-label">个人简介</label>
							<div class="layui-input-block">
								<textarea placeholder="请输入内容" class="layui-textarea"></textarea>
							</div>
						</div>
					</div>
				</fieldset> -->
			</div>
	  </fieldset>
	  <div class="layui-form-item">
	    <a class="layui-btn layui-btn-small" href="javascript:saveVote_save();">保存</a>
	    <a class="layui-btn layui-btn-small" href="javascript:saveVote_back();">返回</a>
	  </div>
	</form>
</div>
<div id="addCandidate_div" style="display: none;padding: 20px 30px;"></div>
<script type="text/javascript">
	layui.use('form', function() {
		var form = layui.form();
		form.render();
	});
	//添加候选人
	function saveVote_addCandidate(){
		$('#addCandidate_div').dialog({    
		    title: '添加候选人',    
		    width: 500,    
		    height:450,    
		    closed: false,
		    href:"${ctx}/voteController/toAddCandidateUI.action",
		    cache: false,       
		    modal: true   
		});
	}
	
	//插入所选候选人的信息
	function saveVote_setCandidate(candidateId,candidateNm,candidateDesc){
		var html = "<fieldset class=\"layui-elem-field\" id=\""+candidateId+"\"><legend></legend>"
				 + "<div class=\"layui-field-box\"><div class=\"layui-form-item\">"
			     + "<label class=\"layui-form-label\" style=\"width: 230px;\"><i class='fa fa-user'></i>候选人："
			     + "<font style=\"font-size: 14px;font-weight: 1px;\">"+candidateNm+"</font></label>"
			     + "<a style=\"margin-left: 5px; margin-top: 5px;\" href=\"javascript:saveVote_deleteCandidate('del_"+candidateId+"');\""
			     + "class=\"layui-btn layui-btn-warm layui-btn-small\">删除</a></div>"
			     + "<div class=\"layui-form-item layui-form-text\"><label class=\"layui-form-label\">个人简介</label>"
			     + "<div class=\"layui-input-block\"><textarea name=\"remark\" class=\"layui-textarea\">"+candidateDesc+"</textarea>"
			     + "</div></div></div></fieldset>";
		$("#saveVote_candidate").append(html);
		var ids = $("#saveVote_ids").val();
		if(ids.length>0){
			$("#saveVote_ids").val(ids+","+candidateId);
		}else{
			$("#saveVote_ids").val(candidateId);
		}
		
	}
	
	//删除已选择的候选人
	function saveVote_deleteCandidate(idStr){
		$.messager.confirm('确认','您确认删除该候选人吗？',function(r){    
		    if (r){    
		    	var arry = idStr.split("_");
		    	//alert(arry.length);
		    	$("#"+arry[1]).remove(); 
		    	var ids = $("#saveVote_ids").val();
		    	if(ids.indexOf(",")>0){
		    		ids = ids.replace(","+arry[1],"");
		    	}else{
		    		ids = ids.replace(arry[1],"");
		    	}
		    	$("#saveVote_ids").val(ids);
		    }    
		}); 	
	}
	
	//保存投票
	function saveVote_save(){
		var title = $('input[name="title"').val();
		var invalidtime = $('input[name="invalidtime"').val();
		var vote_desc = $('textarea[name="vote_desc"').val();
		var dept = $("#vote_dept").combobox("getValue");
		var contentArray = $('textarea[name="remark"').map(function(){ return $(this).val(); }).get(); 
		var candidateIds = $("#saveVote_ids").val();
		var contents = "";
		if(title == null || title.length==0){
			$.messager.alert('提示','请填写本次投票主题');
			return false;
		}
		
		if(invalidtime == null || invalidtime.length==0){
			$.messager.alert('提示','请选择投票失效时间！');
			return false;
		}
		
		if(dept == null || dept.length==0){
			$.messager.alert('提示','请选择适用系部！');
			return false;
		}
		for (var i = 0; i < contentArray.length; i++) {
			if(i != (contentArray.length-1)){
				contents += contentArray[i]+"||";
			}else{
				contents += contentArray[i];
			}
		}
		if(candidateIds == null || candidateIds.length==0){
			$.messager.alert('提示','请添加候选人信息！');
			return false;
		}
		
		$.ajax({
			type:"post",
			url:"${ctx}/voteController/saveVote.action",
			data:{
				title:title,
				invalidtime:invalidtime,
				vote_desc:vote_desc,
				dept:dept,
				candidateIds:candidateIds,
				contents:contents
			},
			dataType:"json",
			success:function(msg){
				if(msg.result=='success'){
					$.messager.alert('','操作成功','info',function(){
						localtionTab('编辑投票','${ctx}/voteController/toVoteManage.action','投票管理');
					}); 
                }else{
                	$.messager.alert('提示','操作失败，请联系管理员！'); 
                }
			}
			
		})
	}
	
	//返回投票列表页面
	function saveVote_back(){
		$.messager.confirm('确认','您确认退出当前操作吗？',function(r){    
		    if (r){    
		    	localtionTab("编辑投票","${ctx}/voteController/toVoteManage.action","投票管理");
		    }    
		}); 
		
	}
</script>