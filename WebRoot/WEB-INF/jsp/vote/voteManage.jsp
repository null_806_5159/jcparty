<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color: #000;}
</style>
<script type="text/javascript">
	$(function() {
		$('#vateManage_voteList').datagrid({  
	   		url:"${ctx}/voteController/getVoteList.action",
	   		height:440,
	   		idField:'id', 
    		pagination:true,
    		singleSelect:true,
    		rownumbers:true,
		    columns:[[   
		        {field:'title',title:'投票标题',width:'15%',align:'center'},  
		        {field:'releasetime',title:'创建日期',width:'10%',align:'center'},  
		        {field:'creator',title:'创建人',width:'10%',align:'center'},  
		        {field:'invalidtime',title:'失效时间',width:'10%',align:'center'},  
		        {field:'state',title:'状态',width:'5%',align:'center',
		        	formatter: function(value,row,index){
		        		switch(value){
			        		case '1':
			        			return "<font style=\"color:#F7BF3A\">待发布</font>";
			        			break;
			        		case '2':
			        			return "<font style=\"color:green;\">待发布</font>";
			        			break;
			        		case '3':
			        			return "<font style=\"color:red;\">已失效</font>";
			        			break;
		        		}
					}
		        },
		        {field:'dept',title:'适用院系',width:'10%',align:'center'},
		        {field:'vote_desc',title:'描述',width:'25%',align:'center',
		        	formatter: function(value,row,index){
						if(value.length>0){
							return "<span title='" + value + "'>" + value + "</span>";	
						}else{
							return "";
						}
					}
		        },
		        {field:'operate',title:'操作',width:'15%',align:'center',
		        	formatter: function(value,row,index){
		        		return "<span><a href=\"javascript:voteManage_issue('"+row.id+"')\" style='cursor: pointer;'>[发布]</a> "
		        		      +"<a href=\"javascript:voteManage_down('"+row.id+"')\" style='cursor: pointer;'>[失效]</a>"
		        		      +" <a href=\"javascript:voteManage_details('"+row.id+"')\" style='cursor: pointer;'>[详情]</a> "
		        		      +"<a href=\"javascript:voteManage_showResult('"+row.id+"')\" style='cursor: pointer;'>[结果]</a></span>";
					}
		        }  
	    	]] 
		});  
	});
</script>
<div style="background-color: #fafafa;">
	<form class="layui-form" action="" method="post">
	<div style="height: 20px;"></div>
		<div class="layui-form-item" style="height: 60px;">
		    <div class="layui-inline">
		      <label class="layui-form-label">测试标题</label>
		      <div class="layui-input-inline">
		        <input type="text" id="roleList_roleNm" name="roleNm" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">状态</label>
		      <div class="layui-input-inline">
		        <select name="interest">
			        <option value="">==请选择==</option>
			        <option value="1">待发布</option>
			        <option value="2">有效</option>
			        <option value="3">失效</option>
			      </select>
		      </div>
		    </div>
		    <div class="layui-inline">
			 	 <a href="javascript:voteManage_query();" class="layui-btn layui-btn-primary layui-btn-radius layui-btn-small"><i class="fa fa-search" style="margin-right: 10px;"></i>查询</a>
			  	 <a href="javascript:voteManage_add();" class="layui-btn layui-btn-radius layui-btn-small"><i class="fa fa-plus-circle" style="margin-right: 10px;"></i>新增</a>
				 <a href="javascript:voteManage_reset();" class="layui-btn layui-btn-normal layui-btn-radius layui-btn-small"><i class="fa fa-refresh" style="margin-right: 10px;"></i>重置</a>
				 <a href="javascript:voteManage_edit();" class="layui-btn layui-btn-danger layui-btn-radius layui-btn-small"><i class="fa fa-edit" style="margin-right: 10px;"></i>修改</a>
		 	</div>
		 </div>
	</form>
	<table id="vateManage_voteList"></table>
	</div>
<div id="voteManage_saveDiv" style="display: none;"></div>
<script>
	layui.use('form', function(){
	  var form = layui.form();
	  form.render();
	});
	
  	//新增投票
  	function voteManage_add(){
	 	localtionTab('投票管理','${ctx}/voteController/toSaveVoteUI.action',"编辑投票")
  	}
  	
  	//发布投票  2
  	function voteManage_issue(id){
  		voteManage_setVoteState(id,"2"); 
  	}
  	
  	//失效 3
  	function voteManage_down(id){
  		$.messager.confirm('确认','您确认想要终止该投票吗？',function(r){    
  		    if (r){    
  		    	voteManage_setVoteState(id,"3");  
  		    }    
  		}); 
  	}
  	
  	function voteManage_setVoteState(id,type){
  		 $.ajax({
  	  	   type:"post",
  	  	   url:"${ctx}/voteController/updateVoteState.action",
  	  	   data:{id:id,type:type},
  	  	   dataType:"json",
  	  	   success:function(msg){
  	  		   if(msg.result=='success'){
  	             	$.messager.alert('提示','操作成功！');
  	             	$('#vateManage_voteList').datagrid('reload');
  	             }else{
  	             	$.messager.alert('提示','操作失败，请联系管理员！'); 
  	             }
  	  	   },
  	  	   error:function(){
  	  		   $.messager.alert('提示','系统异常，请联系管理员！');    
  	  	   }
  	     }) 
  	}
  	
  	//查看详情
  	function voteManage_details(id){
  		alert(id);
  	}
  	
  	//投票结果
  	function voteManage_showResult(id){
  		alert(id);
  	}
</script>