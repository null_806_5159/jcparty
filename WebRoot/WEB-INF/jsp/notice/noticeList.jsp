<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color: #000;}
</style>
<script type="text/javascript">
	$(function() {
		$('#noticeList').datagrid({  
	   		fitColumns:true,
	   		url:"${ctx}/noticeController/getNoticeList.action",
	   		height:480,
	   		idField:'id', 
    		pagination:true,
    		singleSelect:true,
    		rownumbers:true,
		    columns:[[  
		        {field:'id',title:'主键',hidden:true},  
		        {field:'title',title:'标题',width:150,
		        	formatter: function(value,row,index){
						if(value.length>0){
							return "<span title='" + value + "'>" + value + "</span>";	
						}else{
							return "";
						}
					}
		        },  
		        {field:'creator',title:'创建人',width:150},  
		        {field:'createtime',title:'创建时间',width:150,
		        	formatter: function(value,row,index){
						if(value.length>0){
							return value.substring(0,19);
						}else{
							return "";
						}
					}
		        },    
		        {field:'reviewer',title:'审批人',width:150},  
		        {field:'reviewetime',title:'审批时间',width:150,
		        	formatter: function(value,row,index){
						if(typeof(value)!="undefined"){
							return value.substring(0,19);
						}else{
							return "";
						}
					}
		        },   
		        {field:'review_result',title:'审批结果',width:150},  
		        {field:'fit_dept',title:'适用系部',width:150},  
		        {field:'state',title:'状态',width:150,
		        	formatter: function(value,row,index){
						if(value == '1'){
							return "<font style='color:#F7BF3A'>待审核</font>";
						}else if(value == '2' || value == '2'){
							return "<font style='color:red;'>无效</font>";
						}else if(value == '3'){
							return "<font style='color:green;'>有效</font>";
						}
					}
		        
		        },
		        {field:'operate',title:'操作',width:150,align:'center',
		        	formatter: function(value,row,index){
						var html = "<div>";	
						html+="<span><a href='javascript:showNoticeDetail(\""+row.id+"\")' style='cursor: pointer;margin-left: 10px'>[详情]</a></span>";
						if(row.state == '3'){
							html+="<span><a href='javascript:downNotice(\""+row.id+"\")' style='cursor: pointer;margin-left: 10px;'>[下线]</a></span>";
						}else if(row.state == '1'){
							html+="<span><a href='javascript:checkNotice(\""+row.id+"\")' style='cursor: pointer;margin-left: 10px;'>[审核]</a></span>";
						}
						html +="</div>";	  
					    return html;
					}
		        }  
	    	]] 
		});  
	});
	/*查询*/
	function noticeList_query(){
		 reloadNoticelist();
    } 
	
	//重新加载
	function reloadNoticelist(){
		$("#noticeList").datagrid("reload",{
			title:$("input[name='title']").val(),  
            state:$("select[name='state']").val()
		});
	}
	
	/*重置*/
	function noticeList_reset(){
		$("#noticeList_query_form")[0].reset();
		$("#noticeList").datagrid("reload",{
			title:$("input[name='title']").val(),  
            state:$("select[name='state']").val()
		});
	}

	/*新建通知*/
	function noticeList_addNotice(){
		$('#noticeList_savediv').dialog({    
		    title: "新增公告",    
		    width: 800,    
		    height: 600,    
		    closed: false,
		    href: '${ctx}/noticeController/toSaveUI.action',    
		    cache:false,
		    modal: true
		});
	}
	
	/*查看通知详情*/
	function showNoticeDetail(id){
		addTab("通知详情","${ctx}/noticeController/getNoticeById.action?id="+id);
	}
	
	//修改通知内容
	function noticeList_editNotice(){
		var rows = $('#noticeList').datagrid('getSelected');
		if(!rows){
			$.messager.alert('提示','尚未选择要操作的行！'); 
		}else{
			var id = rows.id;
			$('#noticeList_savediv').dialog({    
			    title: "修改公告",    
			    width: 800,    
			    height: 600,    
			    closed: false,
			    href: '${ctx}/noticeController/toSaveUI.action?id='+id,    
			    cache:false,
			    modal: true
			});
		}
	}
	
	//审核通知公告
	function checkNotice(id){
		$('#noticeList_checkdiv').dialog({    
		    title: "审核公告",    
		    width: 800,    
		    height: 600,    
		    closed: false,
		    href: '${ctx}/noticeController/toCheckUI.action?id='+id,    
		    cache:false,
		    modal: true
		});
	}
	
	//通知下线
	function downNotice(id){
		$.messager.confirm('确认','您确认想要下线该通知吗？',function(r){    
		    if (r){    
		    	$.ajax({
					type:"post",
					url:"${ctx }/noticeController/downNotice.action",
					data:{id:id},
					dataType:"json",
					success:function(msg){
						if(msg.result=='success'){
			            	$.messager.alert('提示','操作成功！');
			            	reloadNoticelist();
			            }else{
			            	$.messager.alert('提示','操作失败，请联系管理员！'); 
			            }
					},
					error:function(){
						$.messager.alert('提示','系统异常，请联系管理员！'); 
					}
				})  
		    }    
		});
	}
</script>
<div>
	<div style="padding-top:20px;">
		<form id="noticeList_query_form" class="layui-form" action="">
			<div class="layui-form-item">
			    <div class="layui-inline">
			      <label class="layui-form-label">公告标题</label>
			      <div class="layui-input-inline">
			        <input type="text" name="title" autocomplete="off" class="layui-input">
			      </div>
			    </div>
			    <div class="layui-inline">
			      <label class="layui-form-label">状态</label>
			      <div class="layui-input-inline">
			        <select name="state" lay-filter="aihao">
				        <option value="">==请选择==</option>
				        <option value="0">失效</option>
				        <option value="1">待审核</option>
				        <option value="2">审核不通过</option>
				        <option value="3">已发布</option>
				      </select>
			      </div>
			    </div>
			    <div class="layui-inline">
			      <div class="layui-input-inline" style="width: 220px;">
			        <a class="layui-btn layui-btn-small" href="javascript:noticeList_query();">查询</a>
			        <a class="layui-btn layui-btn-small" href="javascript:noticeList_addNotice();">新增</a>
			        <a class="layui-btn layui-btn-small" href="javascript:noticeList_reset();">重置</a>
			        <a class="layui-btn layui-btn-small" href="javascript:noticeList_editNotice();">修改</a>
			      </div>
			    </div>
			  </div>
		  </form>
	</div>
	<table id="noticeList"></table>
</div>
<div id="noticeList_savediv" style="display: none;"></div>
<div id="noticeList_checkdiv" style="display: none;"></div>
<script type="text/javascript">
layui.use('form', function(){
  var form = layui.form();
  form.render();
});
</script>