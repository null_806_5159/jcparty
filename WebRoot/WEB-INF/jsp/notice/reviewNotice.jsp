<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	//实例化编辑器
	UE.delEditor("checkNoticeContent");//在页面每次初始化的时候，先删除掉以前的编辑器，再次进行初始化,否则会出现渲染失败的情景
	var ue = UE.getEditor('checkNoticeContent', {
	    toolbars: [],
	    wordCount:false,
	    autoHeightEnabled: true,
	    autoFloatEnabled: true,
	    elementPathEnabled:false
	});
	
</script>
<div style="padding: 20px 20px;background: #fafafa;">
	<form class="layui-form layui-form-pane" action="">
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">标&nbsp;&nbsp;题</label>
	    <div class="layui-input-block">
	      <input type="text" id="noticeList_title" name="title" value="${notice.title }" style="width: 80%;" readonly placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">适用系部</label>
	    <div class="layui-input-block">
	      <input id="noticeList_fit_dept" name="fit_dept" value="${notice.fit_dept }" readonly style="width: 40%;" class="layui-input"/>
	    </div>
	  </div>
	  <div class="layui-form-item">
	  		<label class="layui-form-label" style="width: 112px;">通知内容</label>
		    <div class="layui-input-block">
		      <textarea id="checkNoticeContent" name="content" readonly style="width: 90%;height: 300px;">${notice.content }</textarea>
		    </div>
      </div>
	  <div class="layui-form-item" pane="">
	    <label class="layui-form-label" style="width: 112px;">审核结果</label>
	    <div class="layui-input-block">
	      <input type="radio" name="state" value="2" title="不通过">
	      <input type="radio" name="state" value="3" title="通过">
	    </div>
	  </div>
	  <div class="layui-form-item layui-form-text">
	    <label class="layui-form-label">审批理由</label>
	    <div class="layui-input-block">
	      <textarea placeholder="请输入内容" name="review_result" class="layui-textarea"></textarea>
	    </div>
	  </div>
	  <div class="layui-form-item" style="text-align: center;">
		    <div class="layui-input-block">
		     <a class="layui-btn layui-btn-small" href="javascript:reviewNotice_sub();">提交</a>
		     <a class="layui-btn layui-btn-normal layui-btn-small" href="javascript:reviewNotice_back();">返回</a>
		    </div>
      </div>
  </form>
</div>
<script type="text/javascript">
	layui.use('form', function(){
	  var form = layui.form();
	  form.render();
	});
	
	//提交审核人
	function reviewNotice_sub(){
		var id = "${notice.id}";
		var state = $("input[name='state']:checked").val();
		var review_result = $("textarea[name='review_result']").val();
		if(state == null || state.length==0){
			$.messager.alert('提示','请选择审核结果！'); 
			return false;
		}
		if(review_result == null || review_result.length==0){
			$.messager.alert('提示','请填写审核理由！'); 
			return false;
		}
		
		$.ajax({
			type:"post",
			url:"${ctx}/noticeController/checkNotice.action",
			data:{
				id:id,
				state:state,
				review_result:review_result
			},
			dataType:"json",
			success:function(msg){
				if(msg.result=='success'){
	            	$.messager.alert('','操作成功！','info',function(){
	            		window.parent.reloadNoticelist();
	            		$('#noticeList_checkdiv').dialog("close");
	            	});
	            	
	            }else{
	            	$.messager.alert('提示','操作失败，请联系管理员！'); 
	            }
			}
		})
	}
</script>
