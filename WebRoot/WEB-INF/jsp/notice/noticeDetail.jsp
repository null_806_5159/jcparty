<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	UE.delEditor("noticeDetail");//在页面每次初始化的时候，先删除掉以前的编辑器，再次进行初始化,否则会出现渲染失败的情景
	var ue = UE.getEditor('noticeDetail', {
	    toolbars: [],
	    autoHeightEnabled: true,
	    wordCount:false,
	    autoFloatEnabled: true,
	    elementPathEnabled:false,
	    initialFrameHeight:400,
	    initialFrameWidth:800 
	});
</script>
<div style="padding: 20px 100px;background: #fafafa;">
	<div style="text-align: center;">
		<span style="font-size: 36px; text-align: center;">${notice.title}</span>
		<br /> 发布人：${notice.creatorNm } <font>|</font>发布时间：${fn:substring(notice.createtime, 0, 19)}
	</div>
	<hr>
	<textarea style="margin-left: 50px;" rows="5" cols="" id="noticeDetail">${notice.content }</textarea>
</div>