<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	//实例化编辑器
	UE.delEditor("noticeContent");//在页面每次初始化的时候，先删除掉以前的编辑器，再次进行初始化,否则会出现渲染失败的情景
	var ue = UE.getEditor('noticeContent', {
	    toolbars: [[
			'bold', //加粗
			'indent', //首行缩进
			'fontfamily', //字体
	        'fontsize', //字号
	        'paragraph', //段落格式
	        'justifyleft', //居左对齐
	        'justifyright', //居右对齐
	        'justifycenter', //居中对齐
	        'justifyjustify', //两端对齐
	        'forecolor', //字体颜色
	    ]],
	    autoHeightEnabled: true,
	    autoFloatEnabled: true,
	    elementPathEnabled:false,
	    maximumWords:2000
	});
	
	function saveNotice(){
		var title = $("#noticeList_title").val();
		var fit_dept = $("#noticeList_fit_dept").combobox("getValue");
		var content = UE.getEditor('noticeContent').getAllHtml();
		if(title.length==0){
			$.messager.alert('提示','请填写公告标题！'); 
			return false;
		}
		if(fit_dept.length==0){
			$.messager.alert('提示','请选择公告适用的系部！'); 
			return false;
		}
		if(content.length==0){
			$.messager.alert('提示','公告内容尚未填写！'); 
			return false;
		}
		$.ajax({
			type:"post",
			url:"${ctx }/noticeController/saveNotice.action",
			data:$("#noticeList_saveform").serialize(),
			dateType:"json",
			success:function(data){
				var msg = eval("("+data+")");
				if(msg.result=='success'){
	            	$.messager.alert('提示','操作成功！');
	            	reloadNoticelist();
	            	$('#noticeList_savediv').dialog("close");
	            }else{
	            	$.messager.alert('提示','操作失败，请联系管理员！'); 
	            }
			},
			error:function(){
				$.messager.alert('提示','系统异常，请联系管理员！'); 
			}
		})
	}
	
	$(function(){
		$("#noticeList_fit_dept").combobox({
			url:'${ctx}/departmentController/getDeptCombobox.action',    
		    valueField:'id',    
		    textField:'name'   
		})
		
		var fit_dept = "${notice.fit_dept}";
		if(fit_dept.length>0){
			$("#noticeList_fit_dept").combobox("setValue",fit_dept);
		}
	})
</script>
<div style="padding-top: 20px;">
	<form id="noticeList_saveform" class="layui-form" action="">
	  <input type="hidden" name="id" value="${notice.id }"/>
	  <div class="layui-form-item">
	    <label class="layui-form-label">标&nbsp;&nbsp;题</label>
	    <div class="layui-input-block">
	      <input type="text" id="noticeList_title" name="title" value="${notice.title }" style="width: 80%;" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label">适用系部</label>
	    <div class="layui-input-block">
	      <input id="noticeList_fit_dept" name="fit_dept" style="width:200px;height:38px" />
	    </div>
	  </div>
	  <div class="layui-form-item">
	  		<label class="layui-form-label">通知内容</label>
		    <div class="layui-input-block">
		      <textarea id="noticeContent" name="content" style="width: 90%;height: 300px;">${notice.content }</textarea>
		    </div>
      </div>
	  <div class="layui-form-item">
		    <div class="layui-input-block">
		     <a onclick="saveNotice()" class="easyui-linkbutton c3" style="width:80px;">保存</a>
		     <a onclick="addRole()" class="easyui-linkbutton c3" style="width:80px;">返回</a>
		    </div>
      </div>
  </form>
</div>
<script type="text/javascript">
	layui.use('form', function(){
	  var form = layui.form();
	  form.render();
	});
</script>