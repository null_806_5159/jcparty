<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<div style="padding: 20px 30px;">
	<form class="layui-form layui-form-pane" action="">
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">用户名</label>
	    <div class="layui-input-block">
	      <input type="text" name="usercode" value="${activeUser.usercode }" readonly class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">原密码</label>
	    <div class="layui-input-block">
	      <input type="password" name="oldpassword" value="${activeUser.password }" readonly class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">新密码</label>
	    <div class="layui-input-block">
	      <input type="password" name="newpassword" onclick="javascript:$('#pass_tip').hide();" value="" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">确认密码</label>
	    <div class="layui-input-block">
	      <input type="password" name="confirmpassword" onclick="javascript:$('#pass_tip').hide();" value="" class="layui-input">
	    </div>
	    <span id="pass_tip" style="color: red;display: none;">两次密码输入不一致!</span>
	  </div>
	  <div class="layui-form-item">
	    <div class="layui-input-block">
	      <a class="layui-btn" href="javascript:sub_editPass();">立即提交</a>
      	  <button type="reset" class="layui-btn layui-btn-primary">重置</button>
	    </div>
	  </div>
	</form> 
</div> 
<script type="text/javascript">
	function sub_editPass(){
		var newpassword = $("input[name='newpassword']").val();
		var confirmpassword = $("input[name='confirmpassword']").val();
		var usercode = "${activeUser.usercode }";
		if(newpassword != confirmpassword){
			$("#pass_tip").show();
			return false;
		}
		$.ajax({
			type:"post",
			url:"${ctx}/userController/editPasswd.action",
			data:{
				newpassword:newpassword,
				usercode:usercode
			},
			dataType:"json",
			success:function(msg){
				if(msg.result=='success'){
                	$.messager.alert('', '操作成功,请重新登陆!', 'info',function(){
                		window.location.href="${ctx}/login.action";
                	});
                }else{
                	$.messager.alert('提示','操作失败，请联系管理员！'); 
                }
			}
		})
	}
</script>