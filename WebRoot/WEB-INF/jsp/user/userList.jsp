<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function() {
		iniTable();
		//角色选择的下拉框
		$('#userList_roleId').combobox({    
		    url:"${ctx}/roleController/getRoleList.action",
		    editable:false,
		    valueField:'id',    
		    textField:'name'   
		});
		//专业
		$('#userList_major').combotree({    
		    url: '${ctx}/departmentController/getDeptCombotree.action',
		    onBeforeSelect: function(node) {  
		            // 判断是否是叶子节点  
		            var isLeaf = $(this).tree('isLeaf', node.target);  
		            if (!isLeaf) {  
		                return false;  
		            }  
		    }
		});
	});
	
	function iniTable(){
		$('#userList').datagrid({  
	   		fitColumns:true,
	   		url:"${ctx}/userController/getUserInfoList.action",
	   		height:440,
	   		singleSelect:true,
	   		idField:'id',
	   		rownumbers:true,
	   		pageSize:15,
	   		pageList:[15],
    		pagination:true,
		    columns:[[   
		        {field:'dept',title:'系部',width:150,align:'center'},  
		        {field:'username',title:'姓名',width:150,align:'center'},  
		        {field:'usercode',title:'学号(工号)',width:150,align:'center'},  
		        {field:'major',title:'专业',width:150,align:'center'},  
		        {field:'classNm',title:'班级',width:150,align:'center'},
		        {field:'roleName',title:'身份',width:150,align:'center'}, 
		        {field:'state',title:'是否毕业',width:150,align:'center', 
		        	formatter: function (value) {
	                	if(value == '0'){
	                		return "<font style='color:red;'>已毕业</font>";
	                	}else if(value == '1'){
	                		return "<font style='color:green;'>未毕业</font>";
	                	}
	                }
		        },
		        {field:'operate',title:'操作',width:150,
		        	formatter: function(value,row,index){
							var html = "<div class=\"tableOpt\">";
							if(row.state == '1'){
								html += "<i title=\"毕业\" onclick=\"deleteUser('"+row.id+"')\" class=\"fa fa-arrow-circle-down\"></i>";
							}
							html += "<i title=\"修改\" onclick=\"editUser('"+row.id+"')\" class=\"fa fa-edit\"></i>";
							html += "<i title=\"查看详细信息\" onclick=\"viewDetails('"+row.id+"')\" class=\"fa fa-eye\"></i>";
							html += "<i title=\"初始化密码\" onclick=\"initPasswd('"+row.id+"')\" class=\"fa fa-undo\"></i>";
							html += "</div>";
							return html;
					}
		        }  
	    	]],
	    	onDblClickRow: function(index, row){
	    		viewDetails(row.id);
	    	}
		});  
		
	}
	
	
</script>
	<div style="background-color: #fafafa;">
	<form id="userList_queryForm" class="layui-form" action="" method="post">
		<div style="height: 10px;"></div>
		<div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label">学/工号</label>
		      <div class="layui-input-inline">
		        <input type="text" id="userList_stuNum" name="stuNum" lay-verify="phone" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">姓　　名</label>
		      <div class="layui-input-inline">
		        <input type="text" id="userList_name" name="name" lay-verify="email" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">角　　色</label>
		      <div class="layui-input-inline">
		        <input type="text" id="userList_roleId" name="roleId" style="height: 38px;width: 190px;">
		      </div>
		    </div>
		  </div>
		  <div class="layui-form-item">
		  	<div class="layui-inline">
		  		<label class="layui-form-label">系部专业</label>
		      	<div class="layui-input-inline">
		        	<input type="text" id="userList_major" name="majorId" style="height: 38px;width: 190px;"/>
		      	</div>
		    </div>
		  	<div class="layui-inline">
		  		<label class="layui-form-label">是否毕业</label>
		      	<div class="layui-input-inline">
		        	<select name="state" id="userList_state">
				        <option value=""></option>
				        <option value="0">已毕业</option>
				        <option value="1">未毕业</option>
				      </select>
		      	</div>
		    </div>
		  	<div class="layui-inline" style="float: right;">
		  		<a href="javascript:queryUserList();" class="layui-btn layui-btn-primary layui-btn-radius layui-btn-small"><i class="fa fa-search" style="margin-right: 10px;"></i>查　　询</a>
		  		<a href="javascript:resetUserList();" class="layui-btn layui-btn-radius layui-btn-small"><i class="fa fa-refresh" style="margin-right: 10px;"></i>重　　置</a>
			    <a href="javascript:addUser();" class="layui-btn layui-btn-normal layui-btn-radius layui-btn-small"><i class="fa fa-plus-circle" style="margin-right: 10px;"></i>新　　建</a>
			    <a href="javascript:exportUser()" class="layui-btn layui-btn-warm layui-btn-radius layui-btn-small"><i class="fa fa-arrow-circle-down" style="margin-right: 10px;"></i>批量导出</a>
			    <!-- <a href="#" class="layui-btn layui-btn-danger layui-btn-radius"><i class="fa fa-arrow-circle-up" style="margin-right: 10px;"></i>批量导入</a> -->
		    </div>
		  </div>	
		  </form>		
		<table id="userList"></table>
	</div>
	<div id="userList_saveUser" style="display: none;">
		<form id="userList_userForm" class="layui-form" action="" style="margin-top: 10px;">
			<input id="userList_userForm_id" type="hidden" name="id">
			 <div class="layui-form-item">
			    <label class="layui-form-label">用户名</label>
			    <div class="layui-input-inline">
			      <input type="text" id="userList_userForm_username" name="username" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			    </div>
			    
			 </div>
			 <div class="layui-form-item">
			    <label class="layui-form-label">学/工号</label>
			    <div class="layui-input-inline">
			      <input type="text" id="userList_userForm_usercode" name="usercode" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
			    </div>
			     <div id="userCheck" class="layui-form-mid layui-word-aux"></div>
			 </div>
			 <div class="layui-form-item">
			    <label class="layui-form-label">角　色</label>
				<input class="easyui-combobox" id="userList_userForm_roleId" name="roleId" style="width:190px;height:38px" 
    						data-options="valueField:'id',textField:'name',url:'${ctx}/roleController/getRoleList.action'" />
			</div>
			 <div class="layui-form-item">
			    <label class="layui-form-label">院　系</label>
				<input class="easyui-combobox" id="userList_userForm_deptId" name="deptId" style="width:190px;height:38px" 
    						data-options="url:'${ctx}/departmentController/getDeptCombobox.action',    
										    valueField:'id',    
										    textField:'name',
										    editable:false" />
			</div>
			 <div class="layui-form-item">
			    <label class="layui-form-label">专　业</label>
				<select class="easyui-combotree" id="userList_userForm_majorId" name="majorId" style="width:190px;height:38px" 
    						data-options="
    							url:'${ctx}/departmentController/getDeptCombotree.action',
    							onBeforeSelect: function(node) {  
							            // 判断是否是叶子节点  
							            var isLeaf = $(this).tree('isLeaf', node.target);  
							            if (!isLeaf) {  
							                return false;  
							            }  
							    },
							    editable:true
    							
    							" ></select>
    			
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">备　注</label>
			    <div class="layui-input-inline">
			      <textarea id="userList_userForm_remark" style="width: 250px;" name="remark" placeholder="100字以内" class="layui-textarea"></textarea>
			    </div>
			 </div>
			 <div class="layui-form-item" style="padding-left: 150px;">
			 	<a href="javascript:saveUserForm();" class="layui-btn layui-btn-radius"><i class="fa fa-floppy-o" style="margin-right: 10px;"></i>保　存</a>
			 	<a href="javascript:cancelSaveUser();" class="layui-btn layui-btn-normal layui-btn-radius"><i class="fa fa-floppy-o" style="margin-right: 10px;"></i>返　回</a>
			 </div>
		</form>
	</div>
<script>
layui.use('form', function(){
  var form = layui.form();
  form.render();
});

/*保存用户*/
function saveUserForm(){
	var id = $("#userList_userForm_id").val();//主键id
	var username = $("#userList_userForm_username").val();//姓名
	var usercode = $("#userList_userForm_usercode").val();//学号
	var roleId = $("#userList_userForm_roleId").combobox("getValue");//角色
	var majorId = $("#userList_userForm_majorId").combotree("getValue");//专业
	var deptId = $("#userList_userForm_deptId").combotree("getValue");//院系
	var remark = $("#userList_userForm_remark").val();//备注
	
	if(username == null || username.length == 0){
		$.messager.alert('提示','姓名不能为空！');
		return false;
	}
	if(usercode == null || usercode.length == 0){
		$.messager.alert('提示','学/工号不能为空！');
		return false;
	}
	if(roleId == null || roleId.length == 0){
		$.messager.alert('提示','请选择角色！');
		return false;
	}
	if(deptId == null || deptId.length == 0){
		$.messager.alert('提示','请选择系部院系！');
		return false;
	}
	
	$.messager.confirm('确认','您确认创建当前用户吗？',function(r){    
	    if (r){    
	       	$.ajax({
	       		type:"post",
	       		url:"${ctx}/userController/saveUser.action",
	       		data:{
	       			id:id,
	       			username:username,
	       			usercode:usercode,
	       			roleId:roleId,
	       			majorId:majorId,
	       			deptId:deptId,
	       			remark:remark
	       		},
	       		dataType:"json",
	       		success:function(msg){
	       			if(msg.result=='success'){
                    	$.messager.alert('提示','操作成功！');
                    	reloadUserlist();
                    	$('#userList_saveUser').dialog("close");
                    	//重置form表单
                    	$("#userList_userForm_username").val("");//姓名
						$("#userList_userForm_usercode").val("");//学号
						$("#userList_userForm_roleId").combobox("setValue","");//角色
						$("#userList_userForm_deptId").combotree("setValue","");//院系
						$("#userList_userForm_majorId").combotree("setValue","");//专业
						$("#userList_userForm_remark").val("");//备注
                    }else{
                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
                    }
	       		}
	       	})   
	    }    
	}); 
	
}
/*查询*/
function queryUserList(){
	reloadUserlist();
}
//删除用户
function deleteUser(id){
	$.messager.confirm('确认','确定设置该用户为毕业状态吗？',function(r){    
	    if (r){    
			$.ajax({
	       		type:"post",
	       		url:"${ctx}/userController/deleteUserByPK.action",
	       		data:{id:id},
	       		dataType:"json",
	       		success:function(msg){
	       			if(msg.result=='success'){
                    	$.messager.alert('提示','操作成功！');
                    	reloadUserlist();
                    }else{
                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
                    }
	       		}
	       	})
	    }    
	});
	
}
//修改用户
function editUser(id){
	$.ajax({
		type:"post",
		url:"${ctx}/userController/getUserById.action",
		data:{id:id},
		dataType:"json",
		error: function(request) {
			$.messager.alert('提示','操作异常！'); 
        },
        success: function(data) {
        	$("#userList_userForm_id").val(data.id);//主键id
        	$("#userList_userForm_username").val(data.username);//姓名
        	$("#userList_userForm_usercode").val(data.usercode);//学号
        	$("#userList_userForm_roleId").combobox("setValue",data.roleId);//角色
        	$("#userList_userForm_deptId").combotree("setValue",data.majorId);//专业
        	$("#userList_userForm_remark").val(data.remark);//备注
        	saveUser("修改用户");
        }  
	})
	
}
//查看用户详细信息
function viewDetails(id){
	addTab('详细信息', '${ctx}/userController/toDetails.action?id='+id);
}
//初始化密码
function initPasswd(id){
	$.messager.confirm('确认','您确认想要将密码初始化为123456吗？',function(r){    
	    if (r){    
	    	$.ajax({
	       		type:"post",
	       		url:"${ctx}/userController/initPasswd.action",
	       		data:{id:id},
	       		dataType:"json",
	       		success:function(msg){
	       			if(msg.result=='success'){
                    	$.messager.alert('提示','操作成功！');
                    	reloadUserlist();
                    }else{
                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
                    }
	       		}
	       	})  
	    }    
	});
}
/*添加用户*/
function addUser(){
	saveUser("创建用户");
}

/*重新加载*/
function resetUserList(){
	$('#userList_stuNum').val("");
	$('#userList_major').combotree("setValue","");
	$('#userList_name').val("");
	$("#userList_roleId").combobox("setValue","");
	$('#userList').datagrid('load');
	reloadUserlist();
}

/*按条件重新加载用户列表*/
function reloadUserlist(){
	$('#userList').datagrid('load',{  
		stuNum : $('#userList_stuNum').val(), //学号（登录名）
		majorId : $('#userList_major').combotree("getValue"),//专业
		username : $('#userList_name').val(),//姓名
		roleId : $("#userList_roleId").combobox("getValue"),//角色
		state: $('#userList_state option:selected') .val()
	});
}

/*保存用户*/
function saveUser(title){
	$('#userList_saveUser').dialog({    
	    title: title,    
	    width: 500,    
	    height: 450,    
	    closed: false,
	    cache:false,
	    modal: true
	});
}

/*按照当前条件导出用户列表*/
function exportUser(){
	$.messager.confirm('确认','您确认导出当前用户清单吗？',function(r){    
	    if (r){    
	       $("#userList_queryForm").attr("action","${ctx}/userController/exportUserList.action").submit();
	    }    
	});
}
</script>