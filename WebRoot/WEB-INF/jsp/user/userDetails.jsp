<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		// 路径配置
	  	require.config({
	        paths: {
	                echarts: '${ctx}/resources/plug-in/echarts-2.2.7/build/dist'
	        }
	    });
	    // 使用
	    require(
	         [
	             'echarts',
	             'echarts/chart/line',
	             'echarts/chart/pie'
	         ],
	       	 drawEcharts          
	    );
			
		function drawEcharts(ec){
	        drawAttendance(ec);//活动出勤率
	        drawTestScore(ec);//测试成绩分布
		};
	})
	
	//活动出勤图表
	function drawAttendance(ec){
		attendanceChart = ec.init(document.getElementById('userDetails_attendance'));
		attendanceOption = {
			tooltip : {
			        trigger: 'item',
			        formatter: "{a} <br/>{b} : {c} ({d}%)"
			},
			calculable : true,
			series : [
			    {
			       type:'pie',
			       radius : '55%',
			       center: ['50%', '60%'],
			       data:[
			           {value:20, name:'出勤'},
			           {value:5, name:'缺勤'},
			       ]
			     }
			]
		};
		attendanceChart.setOption(attendanceOption);
	}
	
	//获取成绩折线统计
	function drawTestScore(ec){
		testScoreChart = ec.init(document.getElementById('userDetails_testScore'));
		testScoreoption = {
			tooltip : {
			   trigger: 'axis'
			},
			calculable : true,
			grid:{
		    	x:'10%',
		    	y:'12%',
		    	x2:'7%',
		    	y2:'12%'
		    },
			xAxis : [
			     {
			        type : 'category',
			        boundaryGap : false,
			        data : ['活动一','活动二','活动三','活动四','活动五','活动六','活动七']
			     }
			],
			yAxis : [
			     {
			        type : 'value',
			        axisLabel : {
			        formatter: '{value} 分'
			     	}
			     }
			],
			series : [
			   {
			       name:'成绩',
			       type:'line',
			       data:[11, 11, 15, 13, 12, 13, 10],
			       markPoint : {
			          data : [
			             {type : 'max', name: '最大值'},
			             {type : 'min', name: '最小值'}
			          ]
			       },
			       markLine : {
			          data : [
			             {type : 'average', name: '平均值'}
			          ]
			       }
			    }
			]
		};
		testScoreChart.setOption(testScoreoption);
	}
</script>
<div class="blog-body" style="background-color: #f1f1f1;margin-top: 0px;;">
<div class="blog-container">
    <div class="blog-main">
        <!--左边文章列表-->
        <div class="blog-main-left">
                <div class="article shadow">
                	<span><i class="fa fa-tag" style="margin-right: 10px;"></i><font style="font-size: 14px;">基本信息</font></span>
                    <table class="data_tab_list">
						<tr>
							<td class="altrow">
							姓名
							</td>
							<td class="altrow">
								${userDetailVo.sysUser.username }
							</td>
							<td class="altrow">
								性别
							</td>
							<td class="altrow">  
								 <c:if test="${userDetailVo.baseInfoShowVo.sex == '1'}">
								 	男
								 </c:if>
								 <c:if test="${userDetailVo.baseInfoShowVo.sex == '2'}">
								 	女
								 </c:if>
							</td>
							<td class="altrow">
								 出生年月 
							</td>
							<td class="altrow">
								 ${userDetailVo.baseInfoShowVo.birthday }
							</td>
						</tr>
						<tr>
							<td class="base">
									民族
							</td>
							<td class="base">
									${userDetailVo.baseInfoShowVo.national }
							</td>
							<td class="base">
									籍贯
							</td>
							<td class="base">
									${userDetailVo.baseInfoShowVo.native_place }
							</td>
							<td class="base">
									婚姻状况
							</td>
							<td class="base">
								 <c:if test="${userDetailVo.baseInfoShowVo.marital_status== '1' }">
								 	未婚
								 </c:if>
								 <c:if test="${userDetailVo.baseInfoShowVo.marital_status== '2' }">
								 	已婚
								 </c:if>
								 <c:if test="${userDetailVo.baseInfoShowVo.marital_status== '3' }">
								 	离异
								 </c:if>
							</td>
						</tr>
						<tr>
							<td class="altrow">
						        	学号(工号)
							</td>
							<td class="altrow">
						        ${userDetailVo.baseInfoShowVo.usercode }
							</td>
							<td class="altrow">
						        	班级
							</td>
							<td class="altrow">
							<c:if test="${(userDetailVo.baseInfoShowVo.usercode)!= null && fn:length(userDetailVo.baseInfoShowVo.usercode) > 0}">
						        ${fn:substring(userDetailVo.baseInfoShowVo.usercode, 0, 7)}
						    </c:if>
							</td>
							<td class="altrow">
						        	班级职务
							</td>
							<td class="altrow">
						        	${userDetailVo.baseInfoShowVo.class_duty }
							</td>
						</tr>
						<tr>
							<td class="base">
									学历
							</td>
							<td class="base">
								<c:if test="${userDetailVo.baseInfoShowVo.diploma }== '1'">
								 	大专
								 </c:if>
								<c:if test="${userDetailVo.baseInfoShowVo.diploma }== '2'">
								 	本科
								 </c:if>
								<c:if test="${userDetailVo.baseInfoShowVo.diploma }== '3'">
								 	硕士研究生
								 </c:if>
								<c:if test="${userDetailVo.baseInfoShowVo.diploma }== '4'">
								 	博士研究生
								 </c:if>
							</td>
							<td class="base">
								所属系部
							</td>
							<td class="base">
								${userDetailVo.baseInfoShowVo.dept }
							</td>
							<td class="base">
								专业
							</td>
							<td class="base">
								${userDetailVo.baseInfoShowVo.major }
							</td>
						</tr>
						<tr>
							<td class="altrow">
								<i class="fa fa-graduation-cap" style="padding-right:5px;"></i>毕业院校
							</td>
							<td class="altrow" colspan="5">
								${userDetailVo.baseInfoShowVo.university }
							</td>
						</tr>
						<tr>
							<td class="base">
								<i class="fa fa-user" style="padding-right:5px;"></i>身份证号
							</td>
							<td class="base" colspan="3">
									${userDetailVo.baseInfoShowVo.id_card }
							</td>
							<td class="base">
								参加工作时间
							</td>
							<td class="base">
								${userDetailVo.baseInfoShowVo.job_time }
							</td>
						</tr>
						<tr>
							<td class="altrow">
								<i class="fa fa-suitcase" style="padding-right:5px;"></i>工作单位
							</td>
							<td class="altrow" colspan="3">
								${userDetailVo.baseInfoShowVo.job_org }
							</td>
							<td class="altrow">
								工作职务
							</td>
							<td class="altrow">
								${userDetailVo.baseInfoShowVo.job_duty }
							</td>
						</tr>
						<tr>
							<td class="base">
								<i class="fa fa-map-marker" style="padding-right:5px;"></i>户籍所在
							</td>
							<td class="base" colspan="5">
									${userDetailVo.baseInfoShowVo.province_hj}${userDetailVo.baseInfoShowVo.city_hj}${userDetailVo.baseInfoShowVo.county_hj}
							</td>
						</tr>
						<tr>
							<td class="altrow">
								<i class="fa fa-home" style="padding-right:5px;"></i>现居住地
							</td>
							<td class="altrow" colspan="5">
									${userDetailVo.baseInfoShowVo.province_zj}${userDetailVo.baseInfoShowVo.city_zj}${userDetailVo.baseInfoShowVo.county_zj}${userDetailVo.baseInfoShowVo.current_place_4}
							</td>
						</tr>
					</table>
                </div>
                <div class="article shadow">
                    <span><i class="fa fa-tag" style="margin-right: 10px;"></i><font style="font-size: 14px;">党内信息</font></span>
                	<table class="data_tab_list">
						<tr>
							<td class="altrow">
								入党日期
							</td>
							<td class="altrow">
								${userDetailVo.partyInfo.party_time }
							</td>
							<td class="altrow">
								转正日期
							</td>
							<td class="altrow">
								 ${userDetailVo.partyInfo.regular_time }
							</td>
							<td class="altrow">
								 进入党支部日期
							</td>
							<td class="altrow">
								${userDetailVo.partyInfo.branch_time }
							</td>
						</tr>
						<tr>
							<td class="base">
								现任党内职务
							</td>
							<td class="base">
									${userDetailVo.partyInfo.party_duty }
							</td>
							<td class="base">
									分配去向
							</td>
							<td class="base">
									${userDetailVo.partyInfo.to_where }
							</td>
							<td class="base">
									党组织关系接受部门
							</td>
							<td class="base">
									${userDetailVo.partyInfo.accept_dept }
							</td>
						</tr>
						<tr>
							<td class="altrow">
						        	所在党支部
							</td>
							<td class="altrow" colspan="2">
						        	${userDetailVo.partyInfo.branch }
							</td>
							<td class="altrow">
						        	入党介绍人
							</td>
							<td class="altrow" colspan="2">
						       ${userDetailVo.partyInfo.referencess }
							</td>
						</tr>
						<tr>
							<td class="base">
									入党所在支部
							</td>
							<td class="base" colspan="2">
								${userDetailVo.partyInfo.party_branch }
							</td>
							<td class="base">
								转正时所在支部
							</td>
							<td class="base" colspan="2">
								${userDetailVo.partyInfo.regular_branch }
							</td>
						</tr>
						<tr>
							<td class="altrow">
									组织关系所在单位
							</td>
							<td class="altrow" colspan="5">
									${userDetailVo.partyInfo.party_org }
							</td>
						</tr>
					</table>
                </div>
                <div class="article shadow">
                    <span><i class="fa fa-tag" style="margin-right: 10px;"></i><font style="font-size: 14px;">家庭主要成员</font></span>
                	<table class="data_tab_list">
						<thead>
							<tr>
								<td class="altrow">称谓</td>
								<td class="altrow">姓名</td>
								<td class="altrow">年龄</td>
								<td class="altrow">政治面貌</td>
								<td class="altrow">工作单位</td>
								<td class="altrow">职务</td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${(userDetailVo.homeNumList)!= null && fn:length(userDetailVo.homeNumList) > 0}">
								<c:forEach var="homeNum" items="${userDetailVo.homeNumList}">
									<tr>
										<td>${homeNum.relation }</td>
										<td>${homeNum.name}</td>
										<td>${homeNum.age }</td>
										<td>
											<c:if test="${homeNum.political_status == '01' }">
					 							 中共党员
			  								</c:if> 
			  								<c:if test="${homeNum.political_status == '02' }">
					 							 中共预备党员
			  								</c:if> 
			  								<c:if test="${homeNum.political_status == '03' }">
					 							 共青团员
			  								</c:if> 
			  								<c:if test="${homeNum.political_status == '04' }">
					 							 群众
			  								</c:if> 
			  								<c:if test="${homeNum.political_status == '05' }">
					 	 						其他
			  								</c:if>
			  							</td>
										<td>${homeNum.job_org }</td>
										<td>${homeNum.job_duty }</td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${(userDetailVo.homeNumList)== null || fn:length(userDetailVo.homeNumList) == 0}">
							  	<tr>
							  		<td colspan="6" style="text-align: center;">当前用户没有相关记录！</td>
							  	</tr>
							</c:if>
						</tbody>

					</table>
                </div>
                <div class="article shadow">
                    <span><i class="fa fa-tag" style="margin-right: 10px;"></i><font style="font-size: 14px;">奖惩情况</font></span>
                	<table class="data_tab_list">
                		<thead>
							<tr>
								<td class="altrow">获奖时间</td>
								<td class="altrow">奖项</td>
								<td class="altrow">授奖单位</td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${(userDetailVo.rewardList)!= null && fn:length(userDetailVo.rewardList) > 0}">
							  	<c:forEach var="ward"   items="${userDetailVo.rewardList}">
							  		<tr>
								      <td>${ward.reward_time_start }至${ward.reward_time_end }</td>
								      <td>${ward.reward}</td>
								      <td>${ward.reward_org }</td>
								    </tr>
							  	</c:forEach>
							  </c:if>
							  <c:if test="${(userDetailVo.rewardList)== null || fn:length(userDetailVo.rewardList) == 0}">
							  	<tr>
							  		<td colspan="3" style="text-align: center;">当前用户没有相关获奖记录！</td>
							  	</tr>
							  </c:if>
						</tbody>
					</table>
                </div>
        </div>
        <!--右边小栏目-->
        <div class="blog-main-right">
            <div class="blogerinfo shadow">
                <div class="blogerinfo-figure">
                    <c:if test="${(userDetailVo.baseInfoShowVo.path)!= null && fn:length(userDetailVo.baseInfoShowVo.path) > 0}">
						<img alt="" width="60" height="80" src="/upload${userDetailVo.baseInfoShowVo.path }">
					</c:if>
					<c:if test="${(userDetailVo.baseInfoShowVo.path)== null || fn:length(userDetailVo.baseInfoShowVo.path) == 0}">
						<img alt="" width="60" height="80" src="${ctx }/resources/images/login/user.png">
					</c:if>
                </div>
                <p class="blogerinfo-nickname">${userDetailVo.sysUser.username }</p>
                <p class="blogerinfo-introduce">${userDetailVo.roleNm }</p>
                <p class="blogerinfo-location"><i class="fa fa-location-arrow"></i>&nbsp;${userDetailVo.baseInfoShowVo.native_place }</p>
                <hr />
                <div class="blogerinfo-contact">
                	<span style="font-size: 14px;"><i class="fa fa-mobile"></i><font>${userDetailVo.baseInfoShowVo.phoneNum }</font></span>
                	<span style="font-size: 14px;margin-left: 15px;"><i class="fa fa-qq"></i><font>${userDetailVo.baseInfoShowVo.qq }</font></span>
                	<span style="font-size: 14px;margin-left: 15px;"><i class="fa fa-wechat"></i><font>${userDetailVo.baseInfoShowVo.weixin }</font></span>
                </div>
            </div>
            <div></div><!--占位-->
            <div class="blog-module shadow">
                <div class="blog-module-title">活动出勤</div>
                <!-- echarts 区域 -->
                <div id="userDetails_attendance" style="height: 250px;">
                
                </div>
            </div>
            <div class="blog-module shadow">
                <div class="blog-module-title">思想汇报</div>
                <ul class="fa-ul blog-module-ul">
                    <!-- <li><i class="fa-li fa fa-hand-o-right"></i><a href="javscript:;">此版块暂未上线</a></li>
                    <li><i class="fa-li fa fa-hand-o-right"></i><a href="javscript:;">此版块暂未上线</a></li>
                    <li><i class="fa-li fa fa-hand-o-right"></i><a href="javscript:;">此版块暂未上线</a></li>
                    <li><i class="fa-li fa fa-hand-o-right"></i><a href="javscript:;">此版块暂未上线</a></li>
                    <li><i class="fa-li fa fa-hand-o-right"></i><a href="javscript:;">此版块暂未上线</a></li> -->
                </ul>
            </div>
            <div class="blog-module shadow">
                <div class="blog-module-title">测试成绩</div>
                <div id="userDetails_testScore" style="height: 250px;">
                
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
    </div>
    <!--遮罩-->
    <div class="blog-mask animated layui-hide"></div>