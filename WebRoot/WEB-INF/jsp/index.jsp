<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>金城党建-首页</title>
<%@ include file="/WEB-INF/jsp/common_css.jsp"%>
<%@ include file="/WEB-INF/jsp/common_js.jsp"%>
<script type="text/javascript">
	
	function addTab(title, url){
		if ($('#tabs').tabs('exists', title)){
			$('#tabs').tabs('select', title);
		} else {
			$('#tabs').tabs('add',{
				title:title,
				href : url,
				closable:true
			});
		}
	}
	
	//在当前的tab页中加载新的页面
	function localtionTab(oldTitle,newUrl,newTitle){
		$('#tabs').tabs('close',oldTitle);
		$('#tabs').tabs('add',{
			title:newTitle,
			href : newUrl,
			closable:true
		});
	}
	
	$.extend($.fn.tree.methods,{  
	    getLeafChildren:function(jq, params){  
	        var nodes = [];  
	        $(params).next().children().children("div.tree-node").each(function(){  
	            nodes.push($(jq[0]).tree('getNode',this));  
	        });  
	        return nodes;  
	    }  
	});  
	
	//刷新当前的tab页
	function refreshTab(url){
		var current_tab = $('#tabs').tabs('getSelected');
		current_tab.panel('refresh', url);
	}
</script>
<style type="text/css">
	/*滚动条样式 start*/
::-webkit-scrollbar {
   width: 0.2em;
}

::-webkit-scrollbar:horizontal 
{
   height: 0.2em;
}
tabs-selected{
	background-color: #fafafa;
}

.datagrid-row-selected {
	background: #aebdc5;
}
/*滚动条样式 end*/
</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'north'" style="height:71px">
		<%@ include file="/layout/top.jsp"%>
	</div>
	<div data-options="region:'west',split:true" title="系统菜单" style="width:200px;background: #fafafa;">
		<%@ include file="/layout/left.jsp"%>
	</div>
	<div data-options="region:'center'" style="background: #eee;">
		<div id="tabs" class="easyui-tabs" data-options="fit:true">
			<div title="首页">
				<%@ include file="/WEB-INF/jsp/main.jsp"%>
			</div>
		</div>
	</div>
</body>
</html>