<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	function update(){
		localtionTab('基本信息','${ctx}/personInfoController/toSaveUI.action','信息完善');
	}
</script>
<div class="table_div">
	<table class="data_tab_list">
		<tr>
			<th scope="row" rowspan="4">
			<c:if test="${(baseInfo.path)!= null && fn:length(baseInfo.path) > 0}">
				<img alt="" width="60" height="80" src="/upload${baseInfo.path }">
			</c:if>
			<c:if test="${(baseInfo.path)== null || fn:length(baseInfo.path) == 0}">
				<img alt="" width="60" height="80" src="${ctx }/resources/images/login/user.png">
			</c:if>
			</th>
			<td class="altrow">
				姓名
			</td>
			<td class="altrow">
				${baseInfo.username }
			</td>
			<td class="altrow">
				性别
			</td>
			<td class="altrow">
				 <c:if test="${baseInfo.sex=='1'}">
				 	 男
				 </c:if>
				 <c:if test="${baseInfo.sex == '2'}">
				 	 女
				 </c:if>
			</td>
			<td class="altrow">
				 出生年月 
			</td>
			<td class="altrow">
				 ${baseInfo.birthday }
			</td>
		</tr>
				<tr>
					<td class="base">
							民族
					</td>
					<td class="base">
							 ${baseInfo.national }
					</td>
					<td class="base">
							籍贯
					</td>
					<td class="base">
							${baseInfo.native_place }
					</td>
					<td class="base">
							婚姻状况
					</td>
					<td class="base">
							<c:if test="${baseInfo.marital_status=='1'}">
							 	 未婚
							 </c:if>
							 <c:if test="${baseInfo.marital_status == '2'}">
							 	 已婚
							 </c:if>
							 <c:if test="${baseInfo.marital_status == '3'}">
							 	 离异
							 </c:if>
					</td>
				</tr>
				<tr>
					<td class="altrow">
				        	学号
					</td>
					<td class="altrow">
				        ${baseInfo.usercode }
					</td>
					<td class="altrow">
				        	班级
					</td>
					<td class="altrow">
				        ${baseInfo.classNum }
					</td>
					<td class="altrow">
				        	班级职务
					</td>
					<td class="altrow">
				        	 ${baseInfo.class_duty }
					</td>
				</tr>
				<tr>
					<td class="base">
							学历
					</td>
					<td class="base">
						<c:choose> 
						  <c:when test="${baseInfo.diploma == '1'}">   
						   		 大专
						  </c:when> 
						  <c:when test="${baseInfo.diploma=='2'}">   
						    本科
						  </c:when> 
						  <c:when test="${baseInfo.diploma=='3'}">   
						    	硕士研究生
						  </c:when> 
						  <c:when test="${baseInfo.diploma=='4'}">   
						    	博士研究生
						  </c:when> 
						</c:choose> 
					</td>
					<td class="base">
						所属系部
					</td>
					<td class="base">
						信息工程系
					</td>
					<td class="base">
						专业
					</td>
					<td class="base">
						软件工程
					</td>
				</tr>
				<tr>
					<td class="altrow">
						<i class="fa fa-graduation-cap" style="padding-right:5px;"></i>毕业院校
					</td>
					<td class="altrow" colspan="6">
							${baseInfo.university }
					</td>
				</tr>
				<tr>
					<td class="base">
						<i class="fa fa-user" style="padding-right:5px;"></i>身份证号
					</td>
					<td class="base" colspan="2">
							${baseInfo.id_card }
					</td>
					<td class="base">
						QQ/微信
					</td>
					<td class="base">
						${baseInfo.qq }/${baseInfo.weixin}
					</td>
					<td class="base">
						手机号
					</td>
					<td class="base">
						${baseInfo.phoneNum }
					</td>
				</tr>
				<tr>
					<td class="altrow">
						<i class="fa fa-suitcase" style="padding-right:5px;"></i>工作单位
					</td>
					<td class="altrow" colspan="2">
						${baseInfo.job_org }
					</td>
					<td class="altrow">
						参加工作时间
					</td>
					<td class="altrow">
						${baseInfo.job_time }
					</td>
					<td class="altrow">
						工作职务
					</td>
					<td class="altrow">
						${baseInfo.job_duty }
					</td>
				</tr>
				<tr>
					<td class="base">
						<i class="fa fa-map-marker" style="padding-right:5px;"></i>户籍所在
					</td>
					<td class="base" colspan="6">
							${baseInfo.province_hj}${baseInfo.city_hj}${baseInfo.county_hj}
					</td>
				</tr>
				<tr>
					<td class="altrow">
						<i class="fa fa-home" style="padding-right:5px;"></i>现居住地
					</td>
					<td class="altrow" colspan="6">
							${baseInfo.province_zj}${baseInfo.city_zj}${baseInfo.county_zj}${baseInfo.current_place_4}
					</td>
				</tr>
		</table>
</div>
<div style="margin-top: 20px;text-align: center;">
     <a href="javascript:update();" class="layui-btn">更新</a>
</div>