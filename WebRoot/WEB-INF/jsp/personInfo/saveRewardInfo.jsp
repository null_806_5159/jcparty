<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		var saveReward_start = {
			elem: "#saveRewardInfo_startDate", 
		    format: 'YYYY-MM'
		}
		var saveReward_end = {
			elem: "#saveRewardInfo_endDate",
		    format: 'YYYY-MM'
		    
		}
		laydate(saveReward_start);
		laydate(saveReward_end);
	})
</script>
<div style="padding: 30px 30px;">
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>个人获奖信息</legend>
	</fieldset>
	<form id="saveReward_form" class="layui-form layui-form-pane" action="">
		<input type="hidden" name="id" value="${rewardInfo.id }"/>
		<div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label" style="width: 112px;">获奖日期</label>
		      <div class="layui-input-inline">
		        <input type="text" id="saveRewardInfo_startDate" name="reward_time_start" value="${rewardInfo.reward_time_start }" readonly class="layui-input">
		      </div>
		      <div class="layui-form-mid">-</div>
		      <div class="layui-input-inline">
		        <input type="text" id="saveRewardInfo_endDate" name="reward_time_end" value="${rewardInfo.reward_time_start }" readonly class="layui-input">
		      </div>
		    </div>
		 </div>
		 <div class="layui-form-item">
		    <label class="layui-form-label" style="width: 112px;">获得奖项</label>
		    <div class="layui-input-block">
		      <input type="text" name="reward" value="${rewardInfo.reward }" class="layui-input">
		    </div>
  		 </div>
		 <div class="layui-form-item">
		    <label class="layui-form-label" style="width: 112px;">授奖单位</label>
		    <div class="layui-input-block">
		      <input type="text" name="reward_org" value="${rewardInfo.reward_org }" class="layui-input">
		    </div>
  		 </div>
		 <div class="layui-form-item" style="text-align: center;">
		     <a href="javascript:saveReward_save()" class="layui-btn">保存</a>
    		 <a href="javascript:saveReward_back();" class="layui-btn layui-btn-normal">返回</a>
  		 </div>
	</form>
</div>
<script>
layui.use(['form', 'laydate'], function(){
  var form = layui.form()
  ,laydate = layui.laydate;
  form.render();
});

//提交信息
function saveReward_save(){
	var startDate = $("input[name='reward_time_start']").val();
	var endDate = $("input[name='reward_time_end']").val();
	var reward = $("input[name='reward']").val();
	var org = $("input[name='reward_org']").val();
	if(startDate == null || startDate.length == 0){
		$.messager.alert('提示','请完善获奖日期！'); 
		return false;
	}
	if(endDate == null || endDate.length == 0){
		$.messager.alert('提示','请完善获奖日期！'); 
		return false;
	}
	if(reward == null || reward.length == 0){
		$.messager.alert('提示','请填写获得的奖项！'); 
		return false;
	}
	if(org == null || org.length == 0){
		$.messager.alert('提示','请填写授奖单位！'); 
		return false;
	}
	$.ajax({
		type:"post",
		url:"${ctx }/personInfoController/saveRewardInfo.action",
		data:$("#saveReward_form").serialize(),
		dataType:"json",
		success:function(msg){
			if(msg.result=='success'){
            	$.messager.alert('提示','操作成功！');
            	//刷新当前的tab页，关闭dialog
            	parent.window.refreshTab("${ctx}/applicationController/toApprove.action");
            	$('#rewardInfo_save').dialog("close");
            }else{
            	$.messager.alert('提示','操作失败，请联系管理员！'); 
            }
		}
	})
}

//关闭dialog
function saveReward_back(){
	$.messager.confirm('确认','您确认想要退出当前操作吗？',function(r){    
	    if (r){    
	    	$('#rewardInfo_save').dialog("close");
	    }    
	});
}
</script>