<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	function partyInfo_update(){
		localtionTab('党内信息','${ctx}/personInfoController/toSavePartyUI.action','党内信息完善');
	}
</script>
<div class="table_div">
	<table class="data_tab_list">
		<tr>
			<td class="altrow">
				姓名
			</td>
			<td class="altrow">
				${partyInfo.username }
			</td>
			<td class="altrow">
				学号/工号
			</td>
			<td class="altrow">
				${partyInfo.usercode }
			</td>
			<td class="altrow">
				 进入党支部日期
			</td>
			<td class="altrow">
				${partyInfo.branch_time }
			</td>
		</tr>
				<tr>
					<td class="base">
						现任党内职务
					</td>
					<td class="base">
							${partyInfo.party_duty }
					</td>
					<td class="base">
							入党日期
					</td>
					<td class="base">
							${partyInfo.party_time }
					</td>
					<td class="base">
							转正日期
					</td>
					<td class="base">
							${partyInfo.regular_time }
					</td>
				</tr>
				<tr>
					<td class="altrow">
				        	所在党支部
					</td>
					<td class="altrow" colspan="2">
				        	${partyInfo.branch }
					</td>
					<td class="altrow">
				        	入党介绍人
					</td>
					<td class="altrow" colspan="2">
				        	${partyInfo.referencess }
					</td>
				</tr>
				<tr>
					<td class="base">
							入党所在支部
					</td>
					<td class="base" colspan="2">
						${partyInfo.party_branch }
					</td>
					<td class="base">
						转正时所在支部
					</td>
					<td class="base" colspan="2">
						${partyInfo.regular_branch }
					</td>
				</tr>
				<tr>
					<td class="altrow">
							分配去向
					</td>
					<td class="altrow" colspan="2">
						${partyInfo.to_where }
					</td>
					<td class="altrow">
						党组织关系接受部门
					</td>
					<td class="altrow" colspan="2">
						${partyInfo.accept_dept }
					</td>
				</tr>
				<tr>
					<td class="base">
							组织关系所在单位
					</td>
					<td class="base" colspan="5">
							${partyInfo.party_org }
					</td>
				</tr>
		</table>
</div>
<div style="margin-top: 20px;text-align: center;">
     <a href="javascript:partyInfo_update();" class="layui-btn">更新</a>
</div>