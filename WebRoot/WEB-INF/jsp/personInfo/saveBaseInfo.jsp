<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function() {
		var birthday = {
			elem : '#birthday', //选择ID为START的input  
			format : 'YYYY-MM-DD', //自动生成的时间格式  
			max : laydate.now(-1), //最大日期  
			istime : true, //必须填入时间  
			istoday : false, //是否是当天  
		};
		var worktime = {
			elem : '#worktime', //选择ID为START的input  
			format : 'YYYY-MM-DD', //自动生成的时间格式  
			max : laydate.now(), //最大日期  
			istime : true, //必须填入时间  
			istoday : false, //是否是当天  
		};
		laydate(birthday);
		laydate(worktime);

		initCityLinkage('saveBaseInfo_province_hj', 'saveBaseInfo_city_hj',
				'saveBaseInfo_county_hj');
		initCityLinkage('saveBaseInfo_province_jz', 'saveBaseInfo_city_jz',
				'saveBaseInfo_county_jz');
		
		var domicile_1 = "${baseInfo.domicile_1 }";//户籍所在省
		var domicile_2 = "${baseInfo.domicile_2 }"; //户籍所在市
		var domicile_3 = "${baseInfo.domicile_3 }"; //户籍所在区县		
		var current_place_1 = "${baseInfo.current_place_1 }";//居住地所在省
		var current_place_2 = "${baseInfo.current_place_2 }"; //居住地在市
		var current_place_3 = "${baseInfo.current_place_3 }"; //居住地在区县
		$("#saveBaseInfo_province_hj").combobox("setValue",domicile_1);
		$("#saveBaseInfo_province_jz").combobox("setValue",current_place_1);
		if(domicile_2 != null && domicile_2.length>0){
			$("#saveBaseInfo_city_hj").combobox({
				url : '${ctx}/systemController/getCity.action?pcode='+domicile_1,
				valueField : 'id',
				textField : 'name',
				onLoadSuccess : function() {
					$("#saveBaseInfo_city_hj").combobox('setValue',domicile_2);
				}
			})	
		}
		if(current_place_2 != null && current_place_2.length>0){
			$("#saveBaseInfo_city_jz").combobox({
				url : '${ctx}/systemController/getCity.action?pcode='+current_place_1,
				valueField : 'id',
				textField : 'name',
				onLoadSuccess : function() {
					$("#saveBaseInfo_city_jz").combobox('setValue',current_place_2);
				}
			})	
		}
		if(domicile_3 != null && domicile_3.length>0){
			$("#saveBaseInfo_county_hj").combobox({
				url : '${ctx}/systemController/getCounty.action?pcode='+domicile_2,
				valueField : 'id',
				textField : 'name',
				onLoadSuccess : function() {
					$("#saveBaseInfo_county_hj").combobox('setValue',domicile_3);
				}
			})
		}
		if(current_place_3 != null && current_place_3.length>0){
			$("#saveBaseInfo_county_jz").combobox({
				url : '${ctx}/systemController/getCounty.action?pcode='+current_place_2,
				valueField : 'id',
				textField : 'name',
				onLoadSuccess : function() {
					$("#saveBaseInfo_county_jz").combobox('setValue',current_place_3);
				}
			})
		}
	})
	
	function initCityLinkage(province, city, county) {
		//省
		$("#" + province).combobox({
				url : '${ctx}/systemController/getProvince.action',
				valueField : 'id',
				textField : 'name',
				onSelect : function(record) {
					$("#" + city).combobox('clear');
					$("#" + county).combobox('clear');
					$("#" + city).combobox('reload','${ctx}/systemController/getCity.action?pcode='+ record.id),
					$("#" + county).combobox('reload','${ctx}/systemController/getCounty.action?pcode=')
				}
		})
		//市
		$("#" + city).combobox({
			url : '${ctx}/systemController/getCity.action',
			valueField : 'id',
			textField : 'name',
			onSelect : function(record) {
				$("#" + county).combobox('reload','${ctx}/systemController/getCounty.action?pcode='+ record.id);
			}
		})
		//区县
		$("#" + county).combobox({
			url : '${ctx}/systemController/getCounty.action',
			valueField : 'id',
			textField : 'name'
		})
	}
</script>
<div
	style="background: #fafafa; padding-top: 30px; padding-left: 100px;">
	<form id="saveBaseInfo_form" class="layui-form layui-form-pane" action="">
		<input type="hidden" name="id" value="${baseInfo.id }">
		<div class="layui-form-item">
			<div class="layui-input-inline" style="margin-top: 20px;">
				<label class="layui-form-label">头像</label> 
				<input type="file" name="file" class="layui-upload-file" id="saveBaseInfo_picture">
				<input type="hidden" id="saveBaseInfo_picture_addr" name="path" value=""/>
			</div>
			<div class="layui-input-inline">
			<c:if test="${(baseInfo.path)!= null && fn:length(baseInfo.path) > 0}">
				<img id="saveBaseInfo_img_show" width="60" height="80" src="/upload${baseInfo.path }">
			</c:if>
			<c:if test="${(baseInfo.path)== null || fn:length(baseInfo.path) == 0}">
				<img id="saveBaseInfo_img_show" width="60" height="80" src="${ctx }/resources/images/login/user.png">
			</c:if>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">身份证号</label>
			<div class="layui-input-block" style="margin-left: 79px;">
				<input type="text" id="saveBaseInfo_id_card" value="${baseInfo.id_card }" name="id_card" style="width: 25%;" autocomplete="off"
					class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">手机号</label>
			<div class="layui-input-inline">
				<input type="text" id="saveBaseInfo_phoneNum" value="${baseInfo.phoneNum }" name="phoneNum" placeholder="请输入标题" class="layui-input">
			</div>
			<div class="layui-input-inline" style="padding-top: 10px;">
				<span style="margin-left: 5%; color: red;">*&nbsp;&nbsp;请填写绑定微信的手机号</span>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">QQ</label>
				<div class="layui-input-inline">
					<input type="text" name="qq" class="layui-input" value="${baseInfo.qq }" />
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">微信号</label>
				<div class="layui-input-inline">
					<input type="text" name="weixin" autocomplete="off" class="layui-input" value="${baseInfo.weixin }">
				</div>
			</div>
		</div>
		<div class="layui-form-item" pane="">
		    <label class="layui-form-label">性别</label>
		    <div class="layui-input-block">
		      <input type="radio" name="sex" value="1" title="男" ${baseInfo.sex=="1"? 'checked':''}/>
		      <input type="radio" name="sex" value="2" title="女" ${baseInfo.sex=="2"? 'checked':''}>
		    </div>
		 </div>
		<div class="layui-form-item" pane="">
				<label class="layui-form-label">婚姻状况</label>
				<div class="layui-input-block">
					<input type="radio" name="marital_status" value="1" title="未婚" ${baseInfo.marital_status=="1"? 'checked':''}> 
					<input type="radio" name="marital_status"
						value="2" title="已婚" ${baseInfo.marital_status=="2"? 'checked':''}> 
					<input type="radio"
						name="marital_status" value="3" title="离异" ${baseInfo.marital_status=="3"? 'checked':''}>
				</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">出生年月</label>
			<div class="layui-input-inline">
				<input type="text" id="birthday" name="birthday" readonly="readonly"
					placeholder="点击" autocomplete="off" class="layui-input" value="${baseInfo.birthday}">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">民族</label>
				<div class="layui-input-inline">
					<input type="text" name="national" class="layui-input" value="${baseInfo.national}">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">籍贯</label>
				<div class="layui-input-inline">
					<input type="text" name="native_place" class="layui-input" value="${baseInfo.native_place}">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">班级</label>
				<div class="layui-input-inline">
					<input type="text" name="classNum" class="layui-input" value="${baseInfo.classNum}">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">班级职务</label>
				<div class="layui-input-inline">
					<input type="text" name="class_duty" class="layui-input" value="${baseInfo.class_duty}">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">学历</label>
				<div class="layui-input-inline">
					<select name="diploma">
				        <option value="">==请选择==</option>
				        <option value="1" ${baseInfo.diploma == "1" ? "selected" : ""}>大专</option>
				        <option value="2" ${baseInfo.diploma == "2" ? "selected" : ""}>本科</option>
				        <option value="3" ${baseInfo.diploma == "3" ? "selected" : ""}>硕士研究生</option>
				        <option value="4" ${baseInfo.diploma == "4" ? "selected" : ""}>博士研究生</option>
				    </select>
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">学位</label>
				<div class="layui-input-inline">
					<select name="degrees">
				        <option value="">==请选择==</option>
				        <option value="1" ${baseInfo.degrees == "1" ? "selected" : ""}>学士</option>
				        <option value="2" ${baseInfo.degrees == "2" ? "selected" : ""}>硕士</option>
				        <option value="3" ${baseInfo.degrees == "3" ? "selected" : ""}>博士</option>
				    </select>
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">毕业院校</label>
				<div class="layui-input-inline">
					<input type="text" name="university" class="layui-input" value="${baseInfo.university}">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">户籍所在地</label>
			<div class="layui-input-inline">
				<input id="saveBaseInfo_province_hj" name="domicile_1"
					style="height: 38px; width: 180px;" />
			</div>
			<div class="layui-input-inline">
				<input id="saveBaseInfo_city_hj" name="domicile_2"
					style="height: 38px; width: 180px;" />
			</div>
			<div class="layui-input-inline">
				<input id="saveBaseInfo_county_hj" name="domicile_3"
					style="height: 38px; width: 180px;" />
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">现居住地</label>
			<div class="layui-input-inline">
				<input id="saveBaseInfo_province_jz" name="current_place_1"
					style="height: 38px; width: 180px;" />
			</div>
			<div class="layui-input-inline">
				<input id="saveBaseInfo_city_jz" name="current_place_2"
					style="height: 38px; width: 180px;" />
			</div>
			<div class="layui-input-inline">
				<input id="saveBaseInfo_county_jz" name="current_place_3"
					style="height: 38px; width: 180px;" />
			</div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">详细地址</label>
		    <div class="layui-input-block" style="margin-left: 80px;">
		      <input type="text" name="current_place_4" autocomplete="off" placeholder="请输入地址" class="layui-input" value="${baseInfo.current_place_4}">
		    </div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">工作时间</label>
				<div class="layui-input-inline">
					<input type="text" id="worktime" name="job_time" readonly class="layui-input" value="${baseInfo.job_time}">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">工作职务</label>
				<div class="layui-input-inline">
					<input type="text" name="job_duty" class="layui-input" value="${baseInfo.job_duty}">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">工作单位</label>
			<div class="layui-input-block" style="margin-left: 79px;">
				<input type="text" name="job_org" style="width: 55%;" class="layui-input" value="${baseInfo.job_org}">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<a class="layui-btn" href="javascript:saveBaseInfo();">立即提交</a>
				<c:if test="${baseInfo.id == null }">
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</c:if>
			</div>
		</div>
	</form>
</div>
<script>
	layui.use([ 'form', 'upload' ], function() {
		var form = layui.form();
		form.render();
		layui.upload({
		    url: '${ctx}/personInfoController/avatarUpload.action'
		    ,elem: '#saveBaseInfo_picture' //指定原始元素，默认直接查找class="layui-upload-file"
		    ,method: 'post' //上传接口的http类型
		    ,success: function(res){
		      $("#saveBaseInfo_picture_addr").val(res.data.src);//返回上传图片存储的路径
		      $("#saveBaseInfo_img_show").attr("src","/upload"+res.data.src);//显示上传头像
		    }
	  });
	});
	
	//保存基本信息
	function saveBaseInfo(){
		var id_card = $("#saveBaseInfo_id_card").val();
		if(id_card.length>0){
			var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/; 
			if(reg.test(id_card) === false){  
				$.messager.alert('提示','请输入合法的身份证号！');  
			    return  false;  
			}
		}
		var phone = $("#saveBaseInfo_phoneNum").val();
		if(phone.length>0){
		    if(!(/^1(3|4|5|7|8)\d{9}$/.test(phone))){ 
		    	$.messager.alert('提示','请输入合法的手机号！');  
			    return  false;  
		    } 
		}
		
		$.ajax({
			type:"post",
			url:"${ctx }/personInfoController/saveBaseInfo.action",
			data:$("#saveBaseInfo_form").serialize(),
			dateType:"json",
			success:function(data){
				var msg = eval("("+data+")");
				if(msg.result=='success'){
                	$.messager.alert('提示','操作成功！');
                	localtionTab('信息完善','${ctx}/personInfoController/toBaseInfo.action','基本信息');
                }else{
                	$.messager.alert('提示','操作失败，请联系管理员！'); 
                }
			},
			error:function(){
				$.messager.alert('提示','系统异常，请联系管理员！'); 
			}
		})
	}
</script>