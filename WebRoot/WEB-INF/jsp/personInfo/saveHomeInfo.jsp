<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	
</script>
<div style="padding: 30px 30px;">
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>家庭主要成员</legend>
	</fieldset>
	<form id="saveReward_form" class="layui-form layui-form-pane" action="">
		<input type="hidden" name="id" value="${homeNum.id }"/>
		<div class="layui-form-item">
		    <label class="layui-form-label" style="width: 112px;">称谓</label>
		    <div class="layui-input-block">
		      <input type="text" name="relation" value="${homeNum.relation }" class="layui-input">
		    </div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label" style="width: 112px;">姓名</label>
		    <div class="layui-input-block">
		      <input type="text" name="name" value="${homeNum.name }" class="layui-input">
		    </div>
		</div>
		 <div class="layui-form-item">
		    <label class="layui-form-label" style="width: 112px;">年龄</label>
		    <div class="layui-input-block">
		      <input type="number" name="age" value="${homeNum.age }" class="layui-input">
		    </div>
  		 </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label" style="width: 112px;">政治面貌</label>
		    <div class="layui-input-block">
		      <select id="saveHomeNum_political_status" name="political_status" value="">
		        <option value="">==请选择==</option>
		        <option value="01" ${homeNum.political_status=="01"? 'selected':''}>中共党员</option>
		        <option value="02" ${homeNum.political_status=="02"? 'selected':''}>中共预备党员</option>
		        <option value="03" ${homeNum.political_status=="03"? 'selected':''}>共青团员</option>
		        <option value="04" ${homeNum.political_status=="04"? 'selected':''}>群众</option>
		        <option value="05" ${homeNum.political_status=="05"? 'selected':''}>其他</option>
		      </select>
		    </div>
		  </div>
		 <div class="layui-form-item">
		    <label class="layui-form-label" style="width: 112px;">工作单位</label>
		    <div class="layui-input-block">
		      <input type="text" name="job_org" value="${homeNum.job_org }" class="layui-input">
		    </div>
  		 </div>
		 <div class="layui-form-item">
		    <label class="layui-form-label" style="width: 112px;">职位</label>
		    <div class="layui-input-block">
		      <input type="text" name="job_duty" value="${homeNum.job_duty }" class="layui-input">
		    </div>
  		 </div>
		 <div class="layui-form-item" style="text-align: center;">
		     <a href="javascript:homeNum_save()" class="layui-btn">保存</a>
    		 <a href="javascript:homeNum_back();" class="layui-btn layui-btn-normal">返回</a>
  		 </div>
	</form>
</div>
<script>
layui.use('form', function(){
  var form = layui.form();
  form.render();
});

//提交信息
function homeNum_save(){
	var relation = $("input[name='relation']").val();
	var name = $("input[name='name']").val();
	var age = $("input[name='age']").val();
	var political_status = $("#saveHomeNum_political_status").val();
	var job_org = $("input[name='job_org']").val();
	var job_duty = $("input[name='job_duty']").val();
	if(relation == null || relation.length == 0){
		$.messager.alert('提示','请填写称谓信息！'); 
		return false;
	}
	if(name == null || name.length == 0){
		$.messager.alert('提示','请填写家庭成员姓名！'); 
		return false;
	}
	if(age == null || age.length == 0){
		$.messager.alert('提示','请填写家庭成员年龄！'); 
		return false;
	}
	if(political_status == null || political_status.length == 0){
		$.messager.alert('提示','请选择家庭成员的政治面貌！'); 
		return false;
	}
	$.ajax({
		type:"post",
		url:"${ctx }/personInfoController/saveHomeNumInfo.action",
		data:$("#saveReward_form").serialize(),
		dataType:"json",
		success:function(msg){
			if(msg.result=='success'){
            	$.messager.alert('提示','操作成功！');
            	//刷新当前的tab页，关闭dialog
            	parent.window.refreshTab("${ctx}/personInfoController/toHomeNumInfo.action");
            	$('#homeInfo_save').dialog("close");
            }else{
            	$.messager.alert('提示','操作失败，请联系管理员！'); 
            }
		}
	})
}

//关闭dialog
function homeNum_back(){
	$.messager.confirm('确认','您确认想要退出当前操作吗？',function(r){    
	    if (r){    
	    	$('#homeInfo_save').dialog("close");
	    }    
	});
}
</script>