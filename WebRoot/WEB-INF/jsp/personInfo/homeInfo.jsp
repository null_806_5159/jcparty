<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<div style="padding: 20px 30px;">
	<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
	  <legend>家庭主要成员信息</legend>
	</fieldset>
	<table class="layui-table">
	
	  <thead>
	    <tr>
	      <th style="text-align: center;width: 10%">称谓</th>
	      <th style="text-align: center;width: 10%">姓名</th>
	      <th style="text-align: center;width: 10%">年龄</th>
	      <th style="text-align: center;width: 20%">政治面貌</th>
	      <th style="text-align: center;width: 25%">工作单位</th>
	      <th style="text-align: center;width: 10%">职位</th>
	      <th style="text-align: center;width: 15%">操作</th>
	    </tr> 
	  </thead>
	  <tbody>
	  <c:if test="${(homeNums)!= null && fn:length(homeNums) > 0}">
	  	<c:forEach var="homeNum"   items="${ homeNums}">
	  		<tr>
		      <td>${homeNum.relation }</td>
		      <td>${homeNum.name}</td>
		      <td>${homeNum.age }</td>
		      <td>
		      <c:if test="${homeNum.political_status == '01' }">
					 	 中共党员
			  </c:if>
		      <c:if test="${homeNum.political_status == '02' }">
					 	中共预备党员
			  </c:if>
		      <c:if test="${homeNum.political_status == '03' }">
					 	 共青团员
			  </c:if>
		      <c:if test="${homeNum.political_status == '04' }">
					 	 群众
			  </c:if>
		      <c:if test="${homeNum.political_status == '05' }">
					 	 其他
			  </c:if>
		      </td>
		      <td>${homeNum.job_org }</td>
		      <td>${homeNum.job_duty }</td>
		      <td style="text-align: center;">
		      	<a href="javascript:homeNum_delete('${homeNum.id }');" style="cursor: pointer;">[删除]</a>　　
		      	<a href="javascript:homeNum_edit('${homeNum.id }');" style="cursor: pointer;">[修改]</a>
		      </td>
		    </tr>
	  	</c:forEach>
	  </c:if>
	  <c:if test="${(homeNums)== null || fn:length(homeNums) == 0}">
	  	<tr>
	  		<td colspan="7" style="text-align: center;">当前用户没有相关记录！</td>
	  	</tr>
	  </c:if>
	  </tbody>
	</table>
	<div id="homeInfo_save" style="display: none;"></div>  
	<div style="margin-top: 20px;text-align: center;">
	     <a href="javascript:homeInfo_save();" class="layui-btn">添加</a>
	</div>
</div>
<script type="text/javascript">
	//跳转到编辑家庭主要成员的页面
	function homeInfo_save(){
		$('#homeInfo_save').dialog({    
		    title: "新增家庭主要成员信息",    
		    width: 450,    
		    height: 530,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/personInfoController/toSaveHomeNumUI.action',    
		    modal: true   
		});    
	}
	
	//删除
	function homeNum_delete(id){
		$.messager.confirm('确认','您确认想要删除记录吗？',function(r){    
		    if (r){    
		        $.ajax({
		        	type:"post",
		        	url:"${ctx}/personInfoController/deleteHomeNum.action",
		        	data:{id:id},
		        	dataType:"json",
		        	success:function(msg){
		        		if(msg.result=='success'){
		                	$.messager.alert('提示','操作成功！');
		                	refreshTab("${ctx}/personInfoController/toHomeNumInfo.action");
		                }else{
		                	$.messager.alert('提示','操作失败，请联系管理员！'); 
		                }
		        	}
		        })   
		    }    
		});  
	}
	
	//修改
	function homeNum_edit(id){
		$('#homeInfo_save').dialog({    
		    title: "新增家庭主要成员信息",    
		    width: 450,    
		    height: 530,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/personInfoController/toSaveHomeNumUI.action?id='+id,    
		    modal: true   
		});
	}
</script>