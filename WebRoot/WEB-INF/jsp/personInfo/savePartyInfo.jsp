<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
<!--

//-->
</script>
<div style="padding: 50px 150px;">
<div style="text-align: center;">
<span style="font-size: 38px;font-weight: 1px;">党内信息</span>
</div>
<hr/>
<fieldset class="layui-elem-field">
  <legend><i class="fa fa-leaf"></i></legend>
  <div class="layui-field-box" style="padding-left:150px;background: #fafafa;">
	    <form id="savePartyInfo_form" class="layui-form layui-form-pane" action="">
	      <input type="hidden" name="id" value="${partyInfo.id }">
		  <div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label" style="width: 112px;" title="进入党支部时间">进入党支部时间</label>
		      <div class="layui-input-block">
		        <input type="text" name="branch_time" value="${partyInfo.branch_time }" autocomplete="off" class="layui-input" onclick="layui.laydate({elem: this})">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label" style="width: 112px;">入党日期</label>
		      <div class="layui-input-inline">
		        <input type="text" name="party_time" value="${partyInfo.party_time }" autocomplete="off" class="layui-input" onclick="layui.laydate({elem: this})">
		      </div>
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label" style="width: 112px;">转正日期</label>
		      <div class="layui-input-block">
		        <input type="text" name="regular_time" value="${partyInfo.regular_time }" autocomplete="off" class="layui-input" onclick="layui.laydate({elem: this})">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label" style="width: 112px;">所在党支部</label>
		      <div class="layui-input-inline">
		        <input type="text" name="branch" value="${partyInfo.branch }" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		  </div>
		   <div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label" style="width: 112px;">入党介绍人</label>
		      <div class="layui-input-block">
		        <input type="text" name="referencess" value="${partyInfo.referencess }" autocomplete="off" class="layui-input" />
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label" style="width: 112px;" title="入党所在支部">入党所在支部</label>
		      <div class="layui-input-inline">
		        <input type="text" name="party_branch" value="${partyInfo.party_branch }" autocomplete="off" class="layui-input" />
		      </div>
		    </div>
		  </div>
		    <div class="layui-form-item">
			    <div class="layui-inline">
			      <label class="layui-form-label" style="width: 112px;" title="转正时所在党支部">转正时所在党支部</label>
			      <div class="layui-input-block">
			        <input type="text" name="regular_branch" value="${partyInfo.regular_branch }" autocomplete="off" class="layui-input" />
			      </div>
			    </div>
			    <div class="layui-inline">
			      <label class="layui-form-label" style="width: 112px;" title="入党所在支部">党内职务</label>
			      <div class="layui-input-inline">
			        <input type="text" name="party_duty" value="${partyInfo.party_duty }" autocomplete="off" class="layui-input" />
			      </div>
			    </div>
		  </div>
		    <div class="layui-form-item">
			    <div class="layui-inline">
			      <label class="layui-form-label" style="width: 112px;" title="注明省市单位">分配去向</label>
			      <div class="layui-input-block">
			        <input type="text" name="to_where" value="${partyInfo.to_where }" autocomplete="off" class="layui-input" />
			      </div>
			    </div>
			    <div class="layui-inline">
			      <label class="layui-form-label" style="width: 112px;" title="党员组织关系接收部门">党员组织关系接收部门</label>
			      <div class="layui-input-inline">
			        <input type="text" name="accept_dept" value="${partyInfo.accept_dept }" autocomplete="off" class="layui-input" />
			      </div>
			    </div>
		  </div>
		    <div class="layui-form-item">
			    <label class="layui-form-label" style="width: 112px;" title="组织关系所在单位">组织关系所在单位</label>
			    <div class="layui-input-block">
			      <input type="text" name="party_org" value="${partyInfo.party_org }" autocomplete="off" placeholder="请输入标题" class="layui-input" style="width: 82%;">
			    </div>
			</div>
  		  <div class="layui-form-item">
		   	<a href="javascript:partyInfo_save();" class="layui-btn">保存</a>
		   	<c:if test="${partyInfo.id } == null">
		   		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		   	</c:if>
		  </div>
		</form>
  </div>
</fieldset>
</div>
<script>
layui.use(['form', 'laydate'], function(){
  var form = layui.form()
  ,laydate = layui.laydate;
  form.render();
});

//提交信息
function partyInfo_save(){
	$.ajax({
		type:"post",
		url:"${ctx }/personInfoController/savePartyInfo.action",
		data:$("#savePartyInfo_form").serialize(),
		dataType:"json",
		success:function(msg){
			if(msg.result=='success'){
            	$.messager.alert('提示','操作成功！');
            	localtionTab('党内信息完善','${ctx}/personInfoController/toPartyInfo.action','党内信息');
            }else{
            	$.messager.alert('提示','操作失败，请联系管理员！'); 
            }
		}
	})
}
</script>