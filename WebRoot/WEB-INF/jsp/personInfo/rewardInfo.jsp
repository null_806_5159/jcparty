<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
  <legend>个人奖惩情况</legend>
</fieldset>
<table class="layui-table">

  <thead>
    <tr>
      <th style="text-align: center;width: 20%">时间</th>
      <th style="text-align: center;width: 40%">获得奖项</th>
      <th style="text-align: center;width: 25%">颁奖单位</th>
      <th style="text-align: center;width: 15%">操作</th>
    </tr> 
  </thead>
  <tbody>
  <c:if test="${(rewards)!= null && fn:length(rewards) > 0}">
  	<c:forEach var="ward"   items="${ rewards}">
  		<tr>
	      <td>${ward.reward_time_start }至${ward.reward_time_end }</td>
	      <td>${ward.reward}</td>
	      <td>${ward.reward_org }</td>
	      <td style="text-align: center;">
	      	<a href="javascript:rewardInfo_delete('${ward.id }');" style="cursor: pointer;">[删除]</a>　　
	      	<a href="javascript:rewardInfo_edit('${ward.id }');" style="cursor: pointer;">[修改]</a>
	      </td>
	    </tr>
  	</c:forEach>
  </c:if>
  <c:if test="${(rewards)== null || fn:length(rewards) == 0}">
  	<tr>
  		<td colspan="4" style="text-align: center;">当前用户没有相关获奖记录！</td>
  	</tr>
  </c:if>
  </tbody>
</table>
<div id="rewardInfo_save" style="display: none;"></div>  
<div style="margin-top: 20px;text-align: center;">
     <a href="javascript:reward_save();" class="layui-btn">添加</a>
</div>
<script type="text/javascript">
	//跳转到个人奖惩的页面
	function reward_save(){
		$('#rewardInfo_save').dialog({    
		    title: "新增奖惩信息",    
		    width: 800,    
		    height: 450,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/personInfoController/toSaveRewardUI.action',    
		    modal: true   
		});    
	}
	
	//删除
	function rewardInfo_delete(id){
		$.messager.confirm('确认','您确认想要删除记录吗？',function(r){    
		    if (r){    
		        $.ajax({
		        	type:"post",
		        	url:"${ctx}/personInfoController/deleteReward.action",
		        	data:{id:id},
		        	dataType:"json",
		        	success:function(msg){
		        		if(msg.result=='success'){
		                	$.messager.alert('提示','操作成功！');
		                	refreshTab("${ctx}/personInfoController/toRewardInfo.action");
		                }else{
		                	$.messager.alert('提示','操作失败，请联系管理员！'); 
		                }
		        	}
		        })   
		    }    
		});  
	}
	
	//修改
	function rewardInfo_edit(id){
		$('#rewardInfo_save').dialog({    
		    title: "修改奖惩信息",    
		    width: 800,    
		    height: 450,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/personInfoController/toSaveRewardUI.action?id='+id,    
		    modal: true   
		});  
	}
</script>