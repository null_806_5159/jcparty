<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		var parentId = "${permission.parentId}";
		var func_lever = "${permission.func_lever}";
		if(func_lever != '1'){
			$("#parentMenu").show();
			$('#savePermission_pmenu').combobox({    
			    url:'${ctx}/permissionController/getPermission.action?lever='+(func_lever-1),    
			    valueField:'id',    
			    textField:'name',
			    onLoadSuccess:function(){
			    	if(parentId != null && parentId.length>0){
						$('#savePermission_pmenu').combobox("setValue",parentId);
					}
			    }
			});
		}
	})
</script>
<div style="padding: 20px 30px;">
	<form id="savePermission_form" class="layui-form layui-form-pane" action="">
		<input type="hidden" name="id" value="${permission.id }">
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">名称</label>
	    <div class="layui-input-block">
	      <input type="text" name="func_nm" value="${permission.func_nm }" autocomplete="off" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">图标</label>
	    <div class="layui-input-block">
	      <input type="text" name="icon" value="${permission.icon }" autocomplete="off" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">菜单类型</label>
	    <div class="layui-input-block">
	      <select id="savePermission_type" lay-filter="lever" name="func_lever">
	        <option value="">==请选择==</option>
	        <option value="1" ${permission.func_lever=="1"? 'selected':''}>一级菜单</option>
	        <option value="2" ${permission.func_lever=="2"? 'selected':''}>二级菜单</option>
	        <option value="3" ${permission.func_lever=="3"? 'selected':''}>页面按钮</option>
	      </select>
	    </div>
	  </div>
	  <div id="parentMenu" class="layui-form-item" style="display: none;">
	    <label class="layui-form-label" style="width: 112px;">上级菜单</label>
	    <div class="layui-input-block">
	      	 <input type="text" id="savePermission_pmenu" name="parentId" style="width: 263px;height: 38px;"/>
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">顺序</label>
	    <div class="layui-input-block">
	      <input type="text" name="func_order" value="${permission.func_order }" autocomplete="off" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">访问地址</label>
	    <div class="layui-input-block">
	      <input type="text" name="url" value="${permission.url }" autocomplete="off" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	     <label class="layui-form-label" style="width: 112px;">权限描述</label>
	    <div class="layui-input-block">
	      <textarea placeholder="请输入内容" value="${permission.func_desc }" name="func_desc" class="layui-textarea"></textarea>
	    </div>
	  </div>
	  <div class="layui-form-item" style="text-align: center;margin-top: 40px;">
	    <a href="javascript:savePermisssion_save();" class="layui-btn">保存</a>
    	<a onclick="savePermisssion_back()" class="layui-btn layui-btn-normal">取消</a>
	  </div>
  </form>
</div>
<script type="text/javascript">
	layui.use('form', function(){
	  	var form = layui.form();
	  	form.render();
	  	form.on("select(lever)", function(data){
		  	switch(data.value)
			{
	    		case '1':
	    			$("#parentMenu").hide();
	    			break;
	    		case '2':
	    			$("#parentMenu").show();
	    			getPMenu('1');
	    			break;
	    		case '3':
	    			$("#parentMenu").show();
	    			getPMenu('2');
	    			break;
			}
	  	});
	});
	
	//保存提交
	function savePermisssion_save(){
		$.ajax({
		type:"post",
		url:"${ctx}/permissionController/savePermission.action",
		data:$("#savePermission_form").serialize(),
		dataType:"json",
		success:function(msg){
			if(msg.result=='success'){
            	$.messager.alert('提示','操作成功！');
            	window.parent.$('#permissionList_permissionTree').treegrid("reload");
            	$('#permission_save').dialog("close");   
            }else{
            	$.messager.alert('提示','操作失败，请联系管理员！'); 
            }
		}
	})
	}
	
	//返回
	function savePermisssion_back(){
		$.messager.confirm('确认','您确认想要退出当前编辑吗？',function(r){    
		    if (r){    
		    	$('#permission_save').dialog("close");
		    }    
		}); 
	}
	
	//获取所创建的父级菜单
	function getPMenu(lever){
		$('#savePermission_pmenu').combobox({    
		    url:'${ctx}/permissionController/getPermission.action?lever='+lever,    
		    valueField:'id',    
		    textField:'name'   
		});
	}
</script>