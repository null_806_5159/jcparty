<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color: #000;}
	.datagrid-row {  
	  height: 30px;  
	  text-align:center;  
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#permissionList_permissionTree').treegrid({ 
			fitColumns:true,
			url:"${ctx}/permissionController/getPermissionTree.action",
		    idField:'id',
		    height:480,
		    treeField:'text',
		    onLoadSuccess: function () {
		    	$('#permissionList_permissionTree').treegrid('collapseAll')
		    },
		    columns:[[       
		        {field:'text',title:'权限名称',width:60},    
		        {field:'icon',title:'图标',width:'5%',align:'center',
		        	formatter: function(value,row,index){
		        		return "<i class='fa "+value+"'></i>";
		        	}
		        },    
		        {field:'func_lever',title:'类型',width:'10%',
		        	formatter: function(value,row,index){
		        		var type = "";
		        		switch(value)
		        		{
			        		case '1':type = "一级菜单";break;
			        		case '2':type = "二级菜单";break;
			        		case '3':type = "页面按钮";break;
		        		}
		        		return type;
		        	}
		        },    
		        {field:'creatornm',title:'创建人',width:'8%'},    
		        {field:'createtime',title:'创建时间',width:'15%',
		        	formatter: function(value,row,index){
		        		if(value != null && value.length>0){
		        			return value.substring(0,value.length-2);
		        		}else{
		        			return value;
		        		}
		        	}
		        },    
		        {field:'func_order',title:'菜单顺序',width:'5%'}, 
		        {field:'url',title:'访问地址',width:60},  
		        {field:'operate',title:'操作',width:'20%',align:'center',
		        	formatter: function(value,row,index){
		        			var html = "<div>";
		        			if(row.func_lever != '1'){
		        				html += "<a href=\"javascript:permission_delete('"+row.id+"')\">[删除]</a>　　<a href=\"javascript:permission_edit('"+row.id+"')\">[编辑]</a></div>";
		        			}else{
		        				html += "<a href=\"javascript:permission_edit('"+row.id+"')\">[编辑]</a></div>";
		        			}
							return html;
					}
		        }    
		    ]]    
		}); 
	})
	
	//删除
	function permission_delete(id){
		$.messager.confirm('确认','您确认想要删除记录吗？',function(r){    
		    if (r){    
		    	$.ajax({
		        	type:"post",
		        	url:"${ctx}/permissionController/deletePermission.action",
		        	data:{id:id},
		        	dataType:"json",
		        	success:function(msg){
		        		if(msg.result=='success'){
		                	$.messager.alert('提示','操作成功！');
		                	$('#permissionList_permissionTree').treegrid("reload");
		                }else{
		                	$.messager.alert('提示','操作失败，请联系管理员！'); 
		                }
		        	}
		        })     
		    }    
		}); 
	}
	
	//修改
	function permission_edit(id){
		$('#permission_save').dialog({    
		    title: '添加权限',    
		    width: 450,    
		    height: 600,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/permissionController/toSavePermissionUI.action?id='+id,    
		    modal: true   
		}); 
	}
	
	//添加
	function permission_add(){
		$('#permission_save').dialog({    
		    title: '添加权限',    
		    width: 450,    
		    height: 600,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/permissionController/toSavePermissionUI.action',    
		    modal: true   
		}); 
	}
</script>
<div>
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>权限列表</legend>
	</fieldset>
	<div style="padding-left: 90%;margin-bottom: 10px;">
		 <button onclick="permission_add()" class="layui-btn layui-btn-small">添加</button>
	</div>
	<table id="permissionList_permissionTree" style="height:400px"></table> 
</div>
<div id="permission_save" style="display: none;"></div>