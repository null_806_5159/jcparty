<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		$("#saveActivity_form_dept").combobox({
			url:'${ctx}/departmentController/getDeptCombobox.action',    
		    valueField:'id',    
		    textField:'name'   
		})
		validDate('saveActivity_form_validtime');
		
		var deptId = "${activity.dept }";
		if(deptId.length>0){
			$("#saveActivity_form_dept").combobox("setValue",deptId);
		}
	})
	
</script>
<div style="padding-top: 20px; padding-left: 20px;">
	<form id="saveActivity_form" class="layui-form" action="" method="post">
		<input type="hidden" id="saveActivity_form_id" name="id" value="${activity.id }" />
		<div class="layui-form-item">
			<label class="layui-form-label">活动名称</label>
			<div class="layui-input-inline">
				<input type="text" id="saveActivity_form_name" name="name" value="${activity.name }"
					lay-verify="required" placeholder="请输入" autocomplete="off"
					class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">活动地点</label>
			<div class="layui-input-inline">
				<input type="text" id="saveActivity_form_place" name="place" value="${activity.place }" 
					lay-verify="required" placeholder="请输入" autocomplete="off"
					class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">有效时间</label>
			<div class="layui-input-inline">
				<input type="text" id="saveActivity_form_validtime" name="validtime" value="${activity.validtime }" 
					lay-verify="required" placeholder="请输入" autocomplete="off"
					class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">适用系部</label>
			<div class="layui-input-inline">
				<input type="text" id="saveActivity_form_dept" name="dept"
					style="height: 38px; width: 190px;" />
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">活动描述</label>
			<div class="layui-input-inline">
				<textarea placeholder="请输入内容" id="saveActivity_form_remark"
					style="width: 250px;" name="remark" lay-verify="required"
					class="layui-textarea">${activity.remark }</textarea>
			</div>
		</div>
		<div class="layui-form-item" style="padding-left: 150px;">
			<a href="javascript:saveActivityForm();" class="layui-btn layui-btn-small">保存</a> 
			<a href="javascript:saveActivity_back();" class="layui-btn layui-btn-primary layui-btn-small">返回</a>
		</div>
	</form>
</div>
<script type="text/javascript">
	layui.use('form', function(){
	  var form = layui.form();
	  form.render();
	});
	
	//保存活动
	function saveActivityForm(){
		var id = $("#saveActivity_form_id").val();
		var name = $("#saveActivity_form_name").val();
		var place = $("#saveActivity_form_place").val();
		var validtime = $("#saveActivity_form_validtime").val();
		var dept = $("#saveActivity_form_dept").combobox("getValue");
		var remark = $("#saveActivity_form_remark").val();
		if(name.length==0 || name == null){
			$.messager.alert('提示','活动名称尚未填写！'); 
			return false;
		}
		if(place.length==0 || place == null){
			$.messager.alert('提示','活动地点尚未填写！'); 
			return false;
		}
		if(validtime.length==0 || validtime == null){
			$.messager.alert('提示','活动有效时间尚未选择！'); 
			return false;
		}
		if(dept.length==0 || dept == null){
			$.messager.alert('提示','活动适用系部尚未选择！'); 
			return false;
		}
		if(remark.length==0 || remark == null){
			$.messager.alert('提示','请填写活动的相关描述！'); 
			return false;
		}
		$.ajax({
			type:"post",
			url:"${ctx }/activityController/saveActivity.action",
			data:$("#saveActivity_form").serialize(),
			dataType:"json",
			success:function(msg){
				if(msg.result=='success'){
	            	$.messager.alert('提示','操作成功！');
	            	reloadActivitylist();
	            	$('#activityList_addActivity').dialog("close");
	            }else{
	            	$.messager.alert('提示','操作失败，请联系管理员！'); 
	            }
			}
		})
	}
	
	//返回
	function saveActivity_back(){
		$.messager.confirm('确认','您确认想要退出操作吗？',function(r){    
		    if (r){    
		    	$('#activityList_addActivity').dialog("close");
		    }    
		});  
	}
</script>