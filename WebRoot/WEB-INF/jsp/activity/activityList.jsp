<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color:#000;}
</style>
<script type="text/javascript">
	$(function() {
		$('#activityList').datagrid({  
	   		fitColumns:true,
	   		url:"${ctx}/activityController/getActivityList.action",
	   		height:440,
	   		singleSelect:true,
	   		idField:'id',
	   		rownumbers:true,
    		pagination:true,
		    columns:[[   
		        {field:'name',title:'活动名称',width:150,align:'center'},  
		        {field:'createtime',title:'创建时间',width:150,align:'center'},  
		        {field:'creatorNm',title:'创建人',width:150,align:'center'},  
		        {field:'place',title:'地点',width:150,align:'center'},  
		        {field:'deptNm',title:'适用系部',width:150,align:'center'},
		        {field:'state',title:'状态',width:150,align:'center',
		        	formatter: function (value) {
	                	if(value == '0'){
	                		return "<font style='color:red;'>失效</font>";
	                	}else if(value == '2' || value == '3'){
	                		return "<font style='color:#F7BF3A;'>待上线</font>";
	                	}else if(value == '1'){
	                		return "<font style='color:green;'>已上线</font>";
	                	}
	                }
		        },
		        {field:'remark',title:'详情',width:150,align:'center', 
		        	formatter: function (value) {
	                	return "<span title='" + value + "'>" + value + "</span>";
	                }
		        },
		        {field:'operate',title:'操作',width:150,
		        	formatter: function(value,row,index){
		        			var html = "<div class=\"tableOpt\">"
								if(row.state == '1'){
									html += "<a href=\"javascript:activityList_down('"+row.id+"')\">[下线]</a>";
								}else if(row.state == '2' || row.state == '3'){
									html += "<a href=\"javascript:activityList_up('"+row.id+"')\">[上线]</a>";
									html += "<a href=\"javascript:activityList_edit('"+row.id+"')\">[修改]</a>";
								}
			        			html += "<a title='出勤统计' href=\"javascript:activityList_attendance('"+row.id+"')\">[统计]</a>";
			        			html += "</div>";
							return html;
					}
		        }  
	    	]] 
		});  
		//时间区间
		initDate('activityList_startDate','activityList_endDate');
		var start = {  
		        elem: "#activityList_startDate", //选择ID为START的input  
		        format: 'YYYY-MM-DD', //自动生成的时间格式  
		        istime: true, //必须填入时间  
		        istoday: false,  //是否是当天  
		        choose: function(datas){  
		              end.min = datas; //开始日选好后，重置结束日的最小日期  
		              end.start = datas; //将结束日的初始值设定为开始日  
		        }  
			};  
		 	var end = {  
		     	elem: "#activityList_endDate",  
		    	format: 'YYYY-MM-DD',   
		    	istime: true,  
		     	istoday: false,  
			 	choose: function(datas){  
		        	start.max = datas; //结束日选好后，重置开始日的最大日期  
		   		}  
		 	};  
		   	laydate(start);  
		    laydate(end); 
		
		
		$("#activityList_dept").combobox({
			url:'${ctx}/departmentController/getDeptCombobox.action',    
		    valueField:'id',    
		    textField:'name'   
		})
		
		
	});
</script>
	<div style="background-color: #fafafa;">
	<form id="activityList_queryForm" class="layui-form" action="" method="post">
		<div style="height: 10px;"></div>
		<div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label">主题</label>
		      <div class="layui-input-inline">
		        <input type="text" id="activityList_name" name="name" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">适用系部</label>
		      <div class="layui-input-inline">
		        <input type="text" id="activityList_dept" name="dept" style="height: 38px;width: 190px;">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">时间区间</label>
		      <div class="layui-input-inline">
		        <input type="text" id="activityList_startDate" name="startDate" style="cursor: pointer;" class="layui-input">
		      </div>
		      <div class="layui-form-mid">-</div>
		      <div class="layui-input-inline">
		        <input type="text" id="activityList_endDate" name="endDate" style="cursor: pointer;" class="layui-input">
		      </div>
		    </div>
		  </div>
		  <div class="layui-form-item">
		  	<div class="layui-inline" style="float: right;">
		  		<a href="javascript:activityList_query();" class="layui-btn layui-btn-primary layui-btn-small"><i class="fa fa-search" style="margin-right: 10px;"></i>查　　询</a>
		  		<a href="javascript:activityList_reset();" class="layui-btn layui-btn-small"><i class="fa fa-refresh" style="margin-right: 10px;"></i>重　　置</a>
			    <a href="javascript:activityList_add();" class="layui-btn layui-btn-normal layui-btn-small"><i class="fa fa-plus-circle" style="margin-right: 10px;"></i>新　　建</a>
			    <a href="javascript:activityList_getQrcode();" class="layui-btn layui-btn-warm layui-btn-small"><i class="fa fa-qrcode" style="margin-right: 10px;"></i>二维码</a>
		    </div>
		  </div>	
		  </form>		
		<table id="activityList"></table>
	</div>
	<div id="activityList_qrcode" style="display: none;"></div>
	<div id="activityList_addActivity" style="display: none;"></div>
<script>
	layui.use('form', function(){
	  var form = layui.form();
	  form.render();
	});

	//刷新列表
	function reloadActivitylist(){
		$('#activityList').datagrid('load',{  
			name : $('#activityList_name').val(), //标题
			dept : $("#activityList_dept").combobox("getValue"),//适用系部
			startDate : $('#activityList_startDate').val(),//开始时间
			endDate : $('#activityList_endDate').val()//结束时间
		});
	}

	//活动查询
	function activityList_query(){
		reloadActivitylist();
	}

	//重置
	function activityList_reset(){
		$('#activityList_name').val(""), //标题
		$("#activityList_dept").combobox("setValue",""),//适用系部
		$('#activityList_startDate').val(""),//开始时间
		$('#activityList_endDate').val("")//结束时间
		reloadActivitylist();
	} 

	//添加活动
	function activityList_add(){
		$('#activityList_addActivity').dialog({    
		     title: '新增活动',    
		     width: 500,    
		     height: 500,    
		     closed: false,    
		     cache: false, 
		     href:"${ctx}/activityController/toSaveUI.action",
		     modal: true   
		});
	}

//下载二维码
function activityList_getQrcode(id){
	var rows = $('#activityList').datagrid('getSelected');
	if(!rows){
		$.messager.alert('提示','尚未选择要操作的行！'); 
	}else{
		if(rows.state == '1'){
			var state = rows.state;
			var ticket = rows.ticket;
			if(state == '0'){
				$.messager.alert('提示','您选择的活动已经下线！'); 
			}else{
				var url = "${ctx}/activityController/toQrcode.action?ticket="+ticket; 
				$('#activityList_qrcode').dialog({    
				     title: '活动二维码',    
				     width: 500,    
				     height: 500,    
				     closed: false,    
				     cache: false, 
				     href: url,    
				     modal: true   
				});
			}
		}else{
			$.messager.alert('提示','该活动未上线！'); 
		}
		
	}
}

//上线活动
function activityList_up(id){
	$.messager.confirm('确认','您确认想要上线该活动吗？',function(r){    
	    if (r){ 
	    	$.ajax({
	    		type:"post",
	    		url:"${ctx}/activityController/upActivity.action",
	    		data:{id:id},
	    		dataType:"json",
	    		success:function(msg){
	    			if(msg.result=='success'){
                    	$.messager.alert('提示','操作成功！');
                    	reloadActivitylist();
                    }else{
                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
                    }
	    		}
	    	})
	    }    
	});
}

//修改活动
function activityList_edit(id){
	$('#activityList_addActivity').dialog({    
	     title: '修改活动',    
	     width: 500,    
	     height: 500,    
	     closed: false,    
	     cache: false, 
	     href:"${ctx}/activityController/toSaveUI.action?id="+id,
	     modal: true   
	});
}

//活动下线
function activityList_down(id){
	$.messager.confirm('确认','您确认想要下线该活动吗？',function(r){    
	    if (r){    
	       $.ajax({
	    	   type:"post",
	    	   url:"${ctx}/activityController/downActivity.action",
	    	   data:{id:id},
	    	   dataType:"json",
	    	   success:function(msg){
	    		   if(msg.result=='success'){
                   	$.messager.alert('提示','操作成功！');
                   	reloadActivitylist();
                   }else{
                   	$.messager.alert('提示','操作失败，请联系管理员！'); 
                   }
	    	   }
	       }) 
	    }    
	}); 
}
</script>