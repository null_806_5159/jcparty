<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>金城党建信息管理</title>
<link href="${ctx}/resources/style/login/ty_back_login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

</script> 
</head>
<body>
<div class="login_index">
	<div id="alertMessage"></div>
  	<div id="successLogin"></div>
  	<div class="text_success">
   		<img src="${ctx}/resources/plug-in/login/images/loader_green.gif" alt="Please wait" />
   		<span style="font-size:16px;font-weight:bolder;color:#000000">登录成功!系统加载中....</span>
	</div>
	<div class="logo_login">
		<!-- logo图片 -->
		<a href="#"><img src="${ctx}/resources/images/login/login_logo.png"></a>
	</div>
	<form name="formLogin" id="formLogin" action="${ctx}/manage.action" check="${ctx}/checkuser.action" method="post" >
	<div class="login_middle">
		<div class="login_middle_inner">
			<div class="middle_left">
			</div>
			<div class="middle_right">
				<div class="login_form">
					<div class="form_group">
						<div class="input_line">
							<font>用户名：</font>
							<input type="text" style="color: RGB(192, 192, 192);" class="form_input" name="usercode" id="usercode" placeholder="请输入用户名" value="">
						</div>
						<div class="input_line" style="margin-bottom:0">
							<font>密&nbsp;&nbsp;码：</font>
							<input type="password" style="color: RGB(192, 192, 192);" class="form_input" name="password" id="password" placeholder="请输入密码" value="">
							<div id="msgError" style="color:red;position: absolute;margin-top: 76px;"></div>
						</div>
					</div>
					<div class="btn_line">
						<div class="btn_line_inner">
							<input class="sumit_btn"  id="but_login" name="登 录" type="button" value="登 &nbsp;&nbsp;录" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    </form>
    
    <div class="p_bottom" style="color: #000;">
    <em>© <font>2017-</font>南京航空航天大学金城学院-信息工程系</em>
    </div>
    
</div>

<div id="msg" align="right" style="color: red;font-size: 20px;"></div>
<script type="text/javascript" src="${ctx}/resources/plug-in/jquery/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="${ctx}/resources/plug-in/jquery/jquery.cookie.js"></script>
  <script type="text/javascript" src="${ctx}/resources/plug-in/login/js/jquery-jrumble.js"></script>
  <script type="text/javascript" src="${ctx}/resources/plug-in/login/js/jquery.tipsy.js"></script>
  <script type="text/javascript" src="${ctx}/resources/plug-in/login/js/login.js"></script> 
</body>
</html>
