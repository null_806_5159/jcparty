<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	UE.delEditor("newsDetail_content");//在页面每次初始化的时候，先删除掉以前的编辑器，再次进行初始化,否则会出现渲染失败的情景
	var ue = UE.getEditor('newsDetail_content', {
	    toolbars: [],
	    autoHeightEnabled: true,
	    autoFloatEnabled: true,
	    wordCount:false,
	    elementPathEnabled:false,
	    zIndex :900
	});
	ue.addListener('ready', function () {
        ue.setDisabled();
    });
</script>
<div style="background: #fafafa;">
	<div class="blog-body" style="margin-left: 15%; height: 100%; margin-top: 0px;">
		<div class="blog-container">
			<div class="blog-main" style="text-align: center;">
				<div class="blog-main-left">
					<div class="article-detail shadow">
						<div>
							<img src="/upload${news.path }" width="400px" height="300px">
						</div>
						<div class="article-detail-title">${news.title }</div>
						<div class="article-detail-info">
							 <span>发布人：${news.creatorNm }</span><font style="margin: 0px 10px;">|</font><span>编辑时间：${fn:substring(news.createtime, 0, 19)}</span>
						</div>
						<!-- 文章内容 -->
						<textarea id="newsDetail_content" readonly style="width: 100%;">${news.content }</textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
