<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color: #000;}
</style>
<script type="text/javascript">
	$(function(){
		$('#newsList_newsTable').datagrid({  
	   		fitColumns:true,
	   		url:"${ctx}/newsController/getNewsList.action",
	   		height:420,
	   		idField:'id', 
    		pagination:true,
    		singleSelect:true,
    		rownumbers:true,
		    columns:[[  
		        {field:'id',title:'主键',hidden:true},  
		        {field:'title',title:'标题',width:250,
		        	formatter: function(value,row,index){
						if(value.length>0){
							return "<span title='" + value + "'>" + value + "</span>";	
						}else{
							return "";
						}
					}
		        },  
		        {field:'creatorNm',title:'创建人',width:150},  
		        {field:'createtime',title:'创建时间',width:200,
		        	formatter: function(value,row,index){
						if(value.length>0){
							return value.substring(0,19);
						}else{
							return "";
						}
					}
		        },    
		        {field:'reviewer',title:'审批人',width:100},  
		        {field:'reviewetime',title:'审批时间',width:150,
		        	formatter: function(value,row,index){
						if(typeof(value)!="undefined"){
							return value.substring(0,19);
						}else{
							return "";
						}
					}
		        },   
		        {field:'review_result',title:'审批结果',width:100},  
		        {field:'deptNm',title:'适用系部',width:100},  
		        {field:'state',title:'状态',width:70,
		        	formatter: function(value,row,index){
						if(value == '1'){
							return "<font style='color:#green'>已上线</font>";
						}else if(value == '0'){
							return "<font style='color:red;'>无效</font>";
						}else if(value == '2'){
							return "<font style='color:#F7BF3A;'>待上线</font>";
						}
					}
		        },
		        {field:'operate',title:'操作',width:250,align:'center',
		        	formatter: function(value,row,index){
						var html = "<div>";	
						html+="<a href='javascript:showNewsDetails(\""+row.id+"\")'style='cursor: pointer;margin-left: 10px'>[详情]</a>";
						if(row.state == '1'){
							html+="<a href='javascript:downNews(\""+row.id+"\")' style='cursor: pointer;margin-left: 10px;'>[下线]</a>";
						}else{
							html+="<a href='javascript:editNews(\""+row.id+"\")' style='cursor: pointer;margin-left: 10px;'>[修改]</a>";
							html+="<a href='javascript:upNews(\""+row.id+"\")' style='cursor: pointer;margin-left: 10px;'>[上线]</a>";
						}
						html +="</div>";	  
					    return html;
					}
		        }  
	    	]] 
		}); 
		
		//加载文章类型
		$('#newsList_text_type').combobox({    
		    url:'${ctx}/newsController/getNewsType.action',    
		    valueField:'id',    
		    textField:'name'   
		}); 
		//试用系部
		$('#newsList_text_dept').combobox({    
		    url:'${ctx}/departmentController/getDeptCombobox.action',    
		    valueField:'id',    
		    textField:'name',
		    editable:false
		});

	    initDate('newsList_startDate','newsList_endDate')
	})
	
	/*新增文章*/
	function addnews(){
		localtionTab('新闻管理','${ctx}/newsController/toSaveUI.action','新增文章');
	}
</script>
<form class="layui-form" style="margin-top: 2%;margin-left: 4%;" action="">
<div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">文章标题</label>
      <div class="layui-input-inline">
        <input type="tel" name="phone" lay-verify="phone" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-inline">
      <label class="layui-form-label">发布日期</label>
      <div class="layui-input-inline">
        <input type="text" id="newsList_startDate" name="startDate" placeholder="点击" readonly="readonly" autocomplete="off" class="layui-input">
      </div>
      <div class="layui-form-mid">-</div>
      <div class="layui-input-inline">
        <input type="text" id="newsList_endDate" name="endDate" placeholder="点击" readonly="readonly" autocomplete="off" class="layui-input">
      </div>
    </div>
    
</div>
<div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">文章分类</label>
	    <div class="layui-input-inline">
	      <input id="newsList_text_type" name="type" style="height: 38px;width: 190px;"/>
	    </div>
    </div>
    <div class="layui-inline">
      <label class="layui-form-label">适用系部</label>
	    <div class="layui-input-inline">
	      <input id="newsList_text_dept" name="type" style="height: 38px;width: 190px;"/>
	    </div>
	    
    </div>
    <div class="layui-inline">
      	<a class="layui-btn layui-btn-samll">查询</a>
    	<a class="layui-btn layui-btn-samll">重置</a>
    	<a href="javascript:addnews()" class="layui-btn layui-btn-samll">新增</a>
    	<a class="layui-btn layui-btn-samll">审核</a>
    </div>
</div>
</form>
<div>
	<table id="newsList_newsTable"></table>
</div>
<script>
layui.use('form', function(){
  var form = layui.form();
  form.render();
});
//查看文章详情
function showNewsDetails(id){
	addTab("文章详情","${ctx}/newsController/showNewsDetails.action?id="+id);
}

//文章材料下线
function downNews(id){
	$.messager.confirm('确认','您确认想要下线该材料吗？',function(r){    
	    if (r){    
	    	setNewsState(id,"0");
	    }    
	});
}

//文章材料上线
function upNews(id){
	$.messager.confirm('确认','您确认想要上线该材料吗？',function(r){    
	    if (r){    
	    	setNewsState(id,"1");
	    }    
	});
}

function setNewsState(id,type){
	 $.ajax({
  	   type:"post",
  	   url:"${ctx}/newsController/updateNewsState.action",
  	   data:{id:id,type:type},
  	   dataType:"json",
  	   success:function(msg){
  		   if(msg.result=='success'){
             	$.messager.alert('提示','操作成功！');
             	$('#newsList_newsTable').datagrid('reload');
             }else{
             	$.messager.alert('提示','操作失败，请联系管理员！'); 
             }
  	   },
  	   error:function(){
  		   $.messager.alert('提示','系统异常，请联系管理员！');    
  	   }
     })   
}
</script>