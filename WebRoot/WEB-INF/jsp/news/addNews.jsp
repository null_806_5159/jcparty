<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	//实例化编辑器
	UE.delEditor("addNews_text_content");//在页面每次初始化的时候，先删除掉以前的编辑器，再次进行初始化,否则会出现渲染失败的情景
	var ue = UE.getEditor('addNews_text_content', {
	    toolbars: [],
	    autoHeightEnabled: true,
	    autoFloatEnabled: true,
	    elementPathEnabled:false,
	    zIndex :900
	});
	
	function showContent(){
		addTab("详情","${ctx}/newsController/toContent.action");
	}
	
	$(function(){
		//加载文章类型
		$('#addNews_text_type').combobox({    
		    url:'${ctx}/newsController/getNewsType.action',    
		    valueField:'id',    
		    textField:'name',
		    editable:false   
		});
		
		//试用系部
		$('#addNews_text_dept').combobox({    
		    url:'${ctx}/departmentController/getDeptCombobox.action',    
		    valueField:'id',    
		    textField:'name',
		    editable:false
		});
		
		
	})
	
	function addNewsType(){	
		$('#addNews_addNews').dialog({    
		    title: '添加专题',    
		    width: 400,    
		    height: 200,    
		    closed: false,    
		    cache: false,     
		    modal: true   
		});
	}
	
	function addType(){
		var name = $("#addNews_typeNm").val();
		$.ajax({
			type:"post",
			url:"${ctx}/newsController/addType.action",
			data:{name:name},
			dataType:"json",
			success:function(data){
				if(data.result == 'success'){
					$('#addNews_text_type').combobox("reload",{
						onLoadSuccess:function(){
							$('#addNews_addNews').dialog("close");
						}
				    }); 
				}else{
					$.messager.alert('提示','操作失败，请联系管理员！');
				}
			}
		})
	}
	
	/*返回到文章列表*/
	function goBack(){
		$.messager.confirm('确认','您确认想要离开编辑页面吗？',function(r){    
		    if (r){    
		    	localtionTab('新增文章','${ctx}/newsController/toIndex.action','新闻管理'); 
		    }    
		}); 
	}
</script>
<div style="background-color: #fdfdfd;padding-top: 2%;">
<form class="layui-form" action="">
	<div class="layui-form-item">
	    <label class="layui-form-label">文章标题</label>
	    <div class="layui-input-block">
	      <input type="text" id="addNews_newsTitle" name="title" style="width: 50%;" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
	    </div>
  	</div>
	<div class="layui-form-item">
	    <label class="layui-form-label">文章插图</label>
	    <div class="layui-input-block">
	    	<input type="file" id="addNews_picture" name="file" class="layui-upload-file">
	    	<input type="hidden" id="addNews_picture_addr" name="path" value=""/>
	    </div>
  	</div>
  	<div class="layui-form-item">
	     <label class="layui-form-label">专题分类</label>
    	<div class="layui-input-block">
	       <input id="addNews_text_type" name="type" style="height: 38px;width: 190px;"/>
	       <span style="margin-left: 15px;"><a onclick="addNewsType()" style="cursor: pointer;">[添加]</a></span>
    	</div>
   </div>
  	<div class="layui-form-item">
	     <label class="layui-form-label">适用系部</label>
    	<div class="layui-input-block">
	     	<input id="addNews_text_dept" name="dept" style="height: 38px;width: 190px;"/>
    	</div>
   </div>
    <div class="layui-form-item layui-form-text">
	    <label class="layui-form-label">文章内容</label>
	    <div class="layui-input-block">
	      <textarea id="addNews_text_content" name="content" style="width: 95%;height: 500px;"  placeholder="请输入内容" ></textarea>
	    </div>
 	</div>
 	<div class="layui-form-item">
	    <div class="layui-input-block">
	     <a href="javascript:saveNews();" class="layui-btn layui-btn-normal"><i class="fa fa-tags" style="margin-right: 10px;"></i>保存</a>
	     <a href="javascript:goBack()" class="layui-btn"><i class="fa fa-mail-reply" style="margin-right: 10px;"></i>返回</a>
	    </div>
  	</div>
</form>
<div id="addNews_addNews" style="display: none;">
	  <div class="layui-form-item" style="margin-top: 20px;">
	    <label class="layui-form-label">专题名称</label>
	    <div class="layui-input-block">
	      <input type="text" id="addNews_typeNm" name="name" style="width: 150px;" autocomplete="off" placeholder="请输入专题" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <div class="layui-input-block">
	     <button class="layui-btn layui-btn-radius" onclick="addType()"><i class="fa fa-tags" style="margin-right: 10px;"></i>保存</button>
	    </div>
  	</div>
</div>
</div>
<script>
layui.use(['form','upload'], function(){
  var form = layui.form();
  form.render();
  layui.upload({
	    url: '${ctx}/newsController/saveIamg.action'
	    ,elem: '#addNews_picture' //指定原始元素，默认直接查找class="layui-upload-file"
	    ,method: 'post' //上传接口的http类型
	    ,success: function(res){
	      $("#addNews_picture_addr").val(res.data.src);//返回上传图片存储的路径
	    }
  });
});

/**
 * 保存新闻
 */
function saveNews(){
	var content = UE.getEditor('addNews_text_content').getContent();
	var title = $("#addNews_newsTitle").val();
	var path = $("#addNews_picture_addr").val();
	var type = $("#addNews_text_type").combobox("getValue");
	var dept = $("#addNews_text_dept").combobox("getValue");
	$.messager.confirm('确认','您确认想要保存该文章吗？',function(r){    
	    if (r){    
	        $.ajax({
	        	type:"post",
	        	url:"${ctx}/newsController/saveNews.action",
	        	data:{
	        		content:content,
	        		title:title,
	        		path:path,
	        		type:type,
	        		dept:dept
	        	},
	        	dataType:"json",
	        	success:function(msg){
	        		if(msg.result=='success'){
                    	$.messager.alert('','操作成功！','info',function(){
                    		localtionTab('新增文章','${ctx}/newsController/toIndex.action','新闻管理')
                    	});
                    	
                    }else{
                    	$.messager.alert('提示','操作失败，请联系管理员！'); 
                    }
	        	}
	        })  
	    }    
	}); 
}
</script>