<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	/* var maxtime = 1*30 //半个小时，按秒计算，自己调整!  
	function CountDown(){  
		if(maxtime>=0){  
		minutes = Math.floor(maxtime/30);  
		seconds = Math.floor(maxtime%30);  
		msg = "距离结束还有"+minutes+"分"+seconds+"秒";  
		document.all["timer"].innerHTML=msg;  
		if(maxtime == 5*30) 
			alert('注意，还有5分钟!');  
			--maxtime;  
		}  
		else{  
			clearInterval(timer);  
			alert("时间到，结束!");  
		}  
	}  
	 var timer= setInterval("CountDown()",1000);   */
</script>
<div style="padding: 10px 30px;">
	<blockquote class="layui-elem-quote">
		<font style="font-size: 30px;">${test.title }</font>
		<!-- <span id="timer" style="color:red;font-size: 20px;float: right;"></span> -->
	</blockquote>
	<form class="layui-form" action="">
	<input id="test_count" value="${fn:length(questionList) }" type="hidden"/>
	<input id="test_id" value="${test.id }" type="hidden"/>
	<c:if test="${(questionList)!= null && fn:length(questionList) > 0}">
  			<c:forEach var="question" items="${ questionList}" varStatus="status">
		<fieldset class="layui-elem-field">
			<legend>第${status.index + 1 }题</legend>
			<div class="layui-field-box">
				<div>
					<span style="font-size: 24px;">${question.title }</span>
				</div>
				<div class="layui-input-block" style="margin-left:30px;">
					<input type="radio" name="Q${status.index + 1 }" value="A" title="${question.question_1 }">
					<br/>
					<input type="radio" name="Q${status.index + 1 }" value="B" title="${question.question_2 }">
					<br/>
					<input type="radio" name="Q${status.index + 1 }" value="C" title="${question.question_3 }">
					<c:if test="${(question.question_4)!= null && fn:length(question.question_4) > 0}">
						<br/>
						<input type="radio" name="Q${status.index + 1 }" value="D" title="${question.question_4 }">
					</c:if>
				</div>
			</div>
		</fieldset>
		</c:forEach>
  		</c:if>
		

		<a class="layui-btn" href="javascript:test_saveResult();">提交试卷</a>
		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
	</form>
</div>
<script type="text/javascript">
layui.use('form', function() {
	var form = layui.form();
	form.render();
});

//提交试卷
function test_saveResult(){
	var count = $("#test_count").val();
	var results = "";
	var answers = "";
	var score = 0;
	<c:forEach var="question" items="${ questionList}" varStatus="status">
	       answers += "${question.answer}"+",";
	</c:forEach>
	for (var i = 0; i < count; i++) {
		if(i != count-1){
			results += $("input[name='Q"+(i+1)+"']:checked").val() + ",";
		}else{
			results += $("input[name='Q"+(i+1)+"']:checked").val();
		}
	}
	var resultArray = results.split(",");
	var answerArray = answers.split(",");
	for (var i = 0; i < resultArray.length; i++) {
		if(resultArray[i] == answerArray[i]){
			score += 1; 
		}
	}
	
	var testId = $("#test_id").val();
	$.ajax({
		type:"post",
		url:"${ctx}/testController/subScore.action",
		data:{
			score:score,
			testId:testId
		},
		dataType:"json",
		success:function(msg){
			if(msg.result=='success'){ 
    			$.messager.alert('提示','操作成功,本次得分'+score+"!"); 
    			refreshTab("${ctx}/testController/toTestRecord.action");
            }else{
            	$.messager.alert('提示','操作失败，请联系管理员！'); 
            }
		},
		error:function(){
			$.messager.alert('提示','系统异常，请联系管理员！');    
		}
	})
}
</script>