<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color: #000;}
</style>
<script type="text/javascript">
	$(function() {
		$('#testManage_testList').datagrid({  
	   		url:"${ctx}/testController/getTestList.action",
	   		height:440,
	   		idField:'id', 
    		pagination:true,
    		singleSelect:true,
    		rownumbers:true,
		    columns:[[   
		        {field:'title',title:'试卷标题',width:'15%',align:'center',},  
		        {field:'releasetime',title:'发布日期',width:'10%',align:'center',},  
		        {field:'invalidtime',title:'失效时间',width:'10%',align:'center',},  
		        {field:'state',title:'状态',width:'5%',align:'center',
		        	formatter: function(value,row,index){
		        		switch(value){
			        		case '1':
			        			return "<font style=\"color:#F7BF3A\">待发布</font>";
			        			break;
			        		case '2':
			        			return "<font style=\"color:green;\">待发布</font>";
			        			break;
			        		case '3':
			        			return "<font style=\"color:red;\">已失效</font>";
			        			break;
		        		}
					}
		        },
		        {field:'deptNm',title:'使用院系',width:'10%',align:'center'},
		        {field:'test_desc',title:'描述',width:'25%',align:'center'},
		        {field:'operate',title:'操作',width:'25%',align:'center',
		        	formatter: function(value,row,index){
		        		return "<span><a href=\"javascript:testManage_issue('"+row.id+"')\" style='cursor: pointer;'>[发布]</a> "
	        		      +"<a href=\"javascript:testManage_down('"+row.id+"')\" style='cursor: pointer;'>[失效]</a>"
	        		      +" <a href=\"javascript:testManage_details('"+row.id+"')\" style='cursor: pointer;'>[试卷详情]</a> "
	        		      +"<a href=\"javascript:testManage_showResult('"+row.id+"')\" style='cursor: pointer;'>[结果]</a></span>";
					}
		        }  
	    	]] 
		});  
	});
</script>
<div style="background-color: #fafafa;">
	<form class="layui-form" action="" method="post">
	<div style="height: 20px;"></div>
		<div class="layui-form-item" style="height: 60px;">
		    <div class="layui-inline">
		      <label class="layui-form-label">测试标题</label>
		      <div class="layui-input-inline">
		        <input type="text" id="roleList_roleNm" name="roleNm" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">状态</label>
		      <div class="layui-input-inline">
		        <select name="interest">
			        <option value="">==请选择==</option>
			        <option value="0">失效</option>
			        <option value="1">有效</option>
			        <option value="2">待上线</option>
			      </select>
		      </div>
		    </div>
		    <div class="layui-inline">
			 	 <a href="javascript:testManage_query();" class="layui-btn layui-btn-primary layui-btn-radius layui-btn-small"><i class="fa fa-search" style="margin-right: 10px;"></i>查询</a>
			  	 <a href="javascript:testManage_add();" class="layui-btn layui-btn-radius layui-btn-small"><i class="fa fa-plus-circle" style="margin-right: 10px;"></i>新增</a>
				 <a href="javascript:testManage_reset();" class="layui-btn layui-btn-normal layui-btn-radius layui-btn-small"><i class="fa fa-refresh" style="margin-right: 10px;"></i>重置</a>
				 <a href="javascript:testManage_edit();" class="layui-btn layui-btn-danger layui-btn-radius layui-btn-small"><i class="fa fa-edit" style="margin-right: 10px;"></i>修改</a>
		 	</div>
		 </div>
	</form>
	<table id="testManage_testList"></table>
	</div>
<div id="testManage_saveDiv" style="display: none;"></div>
<script>
	layui.use('form', function(){
	  var form = layui.form();
	  form.render();
	});
	
  	//新增投票
  	function testManage_add(){
	 	localtionTab('测试管理','${ctx}/testController/toSaveTestUI.action',"编辑测试")
  	}
  	
    //发布试卷
  	function testManage_issue(id){
  		testManage_setTestState(id,"2"); 
  	}
  	
  	//失效 3
  	function testManage_down(id){
  		$.messager.confirm('确认','您确认想要失效当前试卷吗？',function(r){    
  		    if (r){    
  		    	testManage_setTestState(id,"3"); 
  		    }    
  		}); 
  	}
  	
  	function testManage_setTestState(id,type){
  		 $.ajax({
  	  	   type:"post",
  	  	   url:"${ctx}/testController/updateTestState.action",
  	  	   data:{id:id,type:type},
  	  	   dataType:"json",
  	  	   success:function(msg){
  	  		   if(msg.result=='success'){
  	             	$.messager.alert('提示','操作成功！');
  	             	$('#testManage_testList').datagrid('reload');
  	             }else{
  	             	$.messager.alert('提示','操作失败，请联系管理员！'); 
  	             }
  	  	   },
  	  	   error:function(){
  	  		   $.messager.alert('提示','系统异常，请联系管理员！');    
  	  	   }
  	     }) 
  	}
  	
  	//查看详情
  	function voteManage_details(id){
  		alert(id);
  	}
  	
  	//投票结果
  	function voteManage_showResult(id){
  		alert(id);
  	}
  	
  	
</script>