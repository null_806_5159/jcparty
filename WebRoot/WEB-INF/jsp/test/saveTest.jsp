<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	$(function(){
		$("#vote_dept").combobox({
			url:'${ctx}/departmentController/getDeptCombobox.action',    
		    valueField:'id',    
		    textField:'name'   
		})
		
		var start = {  
	        elem: "#saveVote_invalidtime", //选择ID为START的input  
	        format: 'YYYY-MM-DD', //自动生成的时间格式  
	        max: laydate.now(), //最大日期  
	        istime: true, //必须填入时间  
	        istoday: false
		}; 
		laydate(start);  
	})
</script>
<div style="padding: 20px 30px;background: #fafafa;">
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>编辑试题</legend>
	</fieldset>
	<form class="layui-form layui-form-pane" action="">
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">测试标题</label>
	    <div class="layui-input-block">
	      <input type="text" name="title" autocomplete="off" style="width: 54%;" placeholder="请输入标题" class="layui-input">
	    </div>
	  </div>
	   <div class="layui-form-item">
	    <div class="layui-inline">
	      <label class="layui-form-label" style="width: 112px;">适用系部</label>
	      <div class="layui-input-inline">
	        <input type="text" id="vote_dept" name="dept" style="height: 38px;width: 220px;"/>
	      </div>
	    </div>
	    <div class="layui-inline" style="margin-left: 20px;">
	      <label class="layui-form-label" style="width: 112px;">失效时间</label>
	      <div class="layui-input-inline">
	        <input type="text" id="saveVote_invalidtime" name="invalidtime" class="layui-input">
	      </div>
	    </div>
	  </div>
	  <div class="layui-form-item layui-form-text">
	    <label class="layui-form-label">描述</label>
	    <div class="layui-input-block">
	       <textarea placeholder="请输入内容" name="test_desc" class="layui-textarea"></textarea>
	    </div>
	  </div>
	  <fieldset class="layui-elem-field">
		  <legend>习题集 <a href="javascript:saveTest_addTopic();" class="layui-btn layui-btn-small">添加题目</a></legend>
		  <div class="layui-field-box" id="saveTest_topics">
				<!-- <fieldset class="layui-elem-field" style="background: #fff;">
					<div class="layui-field-box">
						<div>
							<span style="font-size: 24px;">题目：Java对象流的输出类是()</span>
						</div>
						<div class="layui-input-block" style="margin-left: 20px;">
							<blockquote class="layui-elem-quote layui-quote-nm" style="width: 50%;">
							  <font style="font-size: 14px;font-weight: 2px;margin-right: 10px;">A</font>中国
							</blockquote>
							<blockquote class="layui-elem-quote layui-quote-nm" style="width: 50%;">
							  <font style="font-size: 14px;font-weight: 2px;margin-right: 10px;">B</font>中国
							</blockquote>
							<blockquote class="layui-elem-quote layui-quote-nm" style="width: 50%;">
							  <font style="font-size: 14px;font-weight: 2px;margin-right: 10px;">C</font>中国
							</blockquote>
							<blockquote class="layui-elem-quote layui-quote-nm" style="width: 50%;">
							  <font style="font-size: 14px;font-weight: 2px;margin-right: 10px;">D</font>中国
							</blockquote>
							<blockquote class="layui-elem-quote layui-quote-nm" style="width: 20%;">
							  <font style="font-size: 14px;font-weight: 2px;margin-right: 10px;">正确答案</font>A
							</blockquote>
						</div>
					</div>
				</fieldset> -->
			</div>
	  </fieldset>
	  <div class="layui-form-item">
	    <a class="layui-btn layui-btn-small" href="javascript:saveTest_save();">保存</a>
	    <a class="layui-btn layui-btn-small" href="javascript:saveTest_back();">返回</a>
	  </div>
	</form>
</div>
<div id="addTopic_div" style="display: none;padding: 20px 30px;"></div>
<script type="text/javascript">
	layui.use('form', function() {
		var form = layui.form();
		form.render();
	});
	//添加候选人
	function saveTest_addTopic(){
		$('#addTopic_div').dialog({    
		    title: '添加试题',    
		    width: 600,    
		    height:550,    
		    closed: false,
		    href:"${ctx}/testController/toTopicUI.action",
		    cache: false,       
		    modal: true   
		});
	}
	
	//插入所编辑的题目信息
	function saveTest_setTopic(topicNm,qA,qB,qC,qD,answer){
		var html = "<fieldset class=\"layui-elem-field\" style=\"background: #fff;\"><div class=\"layui-field-box\">";
		    html +="<div><span style=\"font-size: 24px;\">题目："+topicNm+"</span></div>";
			  html +="<div class=\"layui-input-block\" style=\"margin-left: 20px;\">";
	          html +="<blockquote class=\"layui-elem-quote layui-quote-nm\" style=\"width: 50%;\">";
	          html +="<font style=\"font-size: 14px;font-weight: 2px;margin-right: 10px;\">A</font>"+qA+"";
	          html +="</blockquote><blockquote class=\"layui-elem-quote layui-quote-nm\" style=\"width: 50%;\">";
	          html +="<font style=\"font-size: 14px;font-weight: 2px;margin-right: 10px;\">B</font>"+qB+"";
	          html +="</blockquote><blockquote class=\"layui-elem-quote layui-quote-nm\" style=\"width: 50%;\">";
	          html +="<font style=\"font-size: 14px;font-weight: 2px;margin-right: 10px;\">C</font>"+qC+"";
	          if(qD.length>0){
	        	  html +="</blockquote><blockquote class=\"layui-elem-quote layui-quote-nm\" style=\"width: 50%;\">";
		          html +="<font style=\"font-size: 14px;font-weight: 2px;margin-right: 10px;\">D</font>"+qD+"";
	          }
	          html +="</blockquote><blockquote class=\"layui-elem-quote layui-quote-nm\" style=\"width: 20%;\">";
	          html +="<font style=\"font-size: 14px;font-weight: 2px;margin-right: 10px;\">正确答案</font>"+answer+"";
	          html +="</blockquote></div></div></fieldset>";;
		$("#saveTest_topics").append(html);
	}
	
	//删除已选择的候选人
	function saveVote_deleteCandidate(id){
		$.messager.confirm('确认','您确认删除该候选人吗？',function(r){    
		    if (r){    
		    	$("#"+id).remove(); 
		    }    
		}); 	
	}
	
	//保存投票
	function saveVote_save(){
		var title = $('input[name="title"').val();
		var invalidtime = $('input[name="invalidtime"').val();
		var vote_desc = $('textarea[name="vote_desc"').val();
		var dept = $("#vote_dept").combobox("getValue");
		var candidateIdArray = $('input[name="candidateId"').map(function(){ return $(this).val(); }).get(); 
		var contentArray = $('textarea[name="content"').map(function(){ return $(this).val(); }).get(); 
		var candidateIds = "";
		var contents = "";
		if(title == null || title.length==0){
			$.messager.alert('提示','请填写本次投票主题');
			return false;
		}
		
		if(invalidtime == null || invalidtime.length==0){
			$.messager.alert('提示','请选择投票失效时间！');
			return false;
		}
		
		if(dept == null || dept.length==0){
			$.messager.alert('提示','请选择适用系部！');
			return false;
		}
		for (var i = 0; i < candidateIdArray.length; i++) {
			if(i != (candidateIdArray.length-1)){
				candidateIds += candidateIdArray[i] + ";";
				contents += contentArray[i]+";";
			}else{
				candidateIds += candidateIdArray[i];
				contents += contentArray[i];
			}
		}
		if(candidateIds == null || candidateIds.length==0){
			$.messager.alert('提示','请添加候选人信息！');
			return false;
		}
		$.ajax({
			type:"post",
			url:"${ctx}/voteController/saveVote.action",
			data:{
				title:title,
				invalidtime:invalidtime,
				vote_desc:vote_desc,
				dept:dept,
				candidateIds:candidateIds,
				contents:contents
			},
			dataType:"json",
			success:function(msg){
				if(msg.result=='success'){
					$.messager.alert('','操作成功','info',function(){
						localtionTab('编辑投票','${ctx}/voteController/toVoteManage.action','投票管理');
					}); 
					
                }else{
                	$.messager.alert('提示','操作失败，请联系管理员！'); 
                }
			}
			
		})
	}
</script>