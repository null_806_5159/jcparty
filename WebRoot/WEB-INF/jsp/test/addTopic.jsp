<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<form class="layui-form layui-form-pane" action="">
	<div class="layui-form-item layui-form-text">
		<label class="layui-form-label">题目</label>
		<div class="layui-input-block">
			<textarea placeholder="请输入题目" name="title" class="layui-textarea"></textarea>
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label" style="width: 112px;">选项A</label>
		<div class="layui-input-block">
			<input type="text" name="qa" autocomplete="off"
				placeholder="请输入选项A" class="layui-input">
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label" style="width: 112px;">选项B</label>
		<div class="layui-input-block">
			<input type="text" name="qb" autocomplete="off"
				placeholder="请输入选项B" class="layui-input">
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label" style="width: 112px;">选项C</label>
		<div class="layui-input-block">
			<input type="text" name="qc" autocomplete="off"
				placeholder="请输入选项C" class="layui-input">
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label" style="width: 112px;">选项D</label>
		<div class="layui-input-block">
			<input type="text" name="qd" autocomplete="off"
				placeholder="请输入选项D" class="layui-input">
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label" style="width: 112px;">正确答案</label>
		<div class="layui-input-block">
			<input type="text" name="answer" autocomplete="off"
				placeholder="请输入正确答案" class="layui-input">
		</div>
	</div>
	<div class="layui-form-item" style="text-align: center;">
		<a class="layui-btn layui-btn-small" href="javascript:addTopic_save();">保存</a>
		<a class="layui-btn layui-btn-small">返回</a>
	</div>
</form>

<script type="text/javascript">
	//保存试题
	function addTopic_save(){
		var title = $("textarea[name='title']").val();
		var qa = $("input[name='qa']").val();
		var qb = $("input[name='qb']").val();
		var qc = $("input[name='qc']").val();
		var qd = $("input[name='qd']").val();
		var answer = $("input[name='answer']").val();
		if(title == null || title.length==0){
			$.messager.alert('提示','请输入题目！');
			return false;
		}
		if(qa == null || qa.length==0){
			$.messager.alert('提示','请输入选项A！');
			return false;
		}
		if(qb == null || qb.length==0){
			$.messager.alert('提示','请输入选项B！');
			return false;
		}
		if(qc == null || qc.length==0){
			$.messager.alert('提示','请输入选项C！');
			return false;
		}
		if(answer == null || answer.length==0){
			$.messager.alert('提示','请输入正确的选项！');
			return false;
		}
		window.parent.saveTest_setTopic(title,qa,qb,qc,qd,answer);
		$('#addTopic_div').dialog("close");
	}
</script>