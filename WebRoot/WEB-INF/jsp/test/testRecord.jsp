<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color:#000;}
</style>
<script type="text/javascript">
	/* $(function(){
		$("#mytest_testList").datagrid({
		   		fitColumns:true,
		   		//url:"${ctx}/userController/getUserInfoList.action",
		   		height:440,
		   		singleSelect:true,
		   		idField:'id',
		   		rownumbers:true,
	    		pagination:true,
			    columns:[[   
			        {field:'dept',title:'试卷名称',width:'20%',align:'center'},  
			        {field:'username',title:'有效时间',width:'30%',align:'center'},  
			        {field:'usercode',title:'得分',width:'10%',align:'center'},  
			        {field:'major',title:'及格分数',width:'10%',align:'center'},  
			        {field:'classNm',title:'创建人',width:'10%',align:'center'},
			        {field:'operate',title:'操作',width:'20%',align:'center',
			        	formatter: function(value,row,index){
								return "<div class=\"tableOpt\">"
									   +"<i title=\"删除\" onclick=\"deleteUser('"+row.id+"')\" class=\"fa fa-times\"></i>"
									   +"<i title=\"修改\" onclick=\"editUser('"+row.id+"')\" class=\"fa fa-edit\"></i>"
									   +"<i title=\"查看详细信息\" onclick=\"viewDetails('"+row.id+"')\" class=\"fa fa-eye\"></i>"
									   +"<i title=\"初始化密码\" onclick=\"initPasswd('"+row.id+"')\" class=\"fa fa-undo\"></i>"
									   +"</div>";
						}
			        }  
		    	]] 
			});
	}) */
</script>
<div style="padding: 20px 50px;background: #fafafa;">
	<fieldset class="layui-elem-field layui-field-title">
	  <legend>最近的考试</legend>
	</fieldset>  
	 
	<table class="layui-table" lay-skin="line">
	  <colgroup>
	    <col width="150">
	    <col width="250">
	    <col width="100">
	    <col>
	  </colgroup>
	  <thead>
	    <tr>
	      <th>试卷名称</th>
	      <th>试卷有效期</th>
	      <th>试题数</th>
	      <th>出题人</th>
	      <th>操作</th>
	    </tr> 
	  </thead>
	  <tbody>
	   <c:if test="${(testList)!= null && fn:length(testList) > 0}">
  				<c:forEach var="test"   items="${ testList}">
	    <tr>
	      <td>${test.title }</td>
	      <td>${fn:substring(test.releasetime, 0, 10)}~${fn:substring(test.invalidtime, 0, 10)}</td>
	      <td>${test.questionCount }</td>
	      <td>${test.creator }</td>
	      <td><a href="javascript:mytest_startTest('${test.id }');" style="cursor: pointer;">[开始考试]</a></td>
	    </tr>
	    </c:forEach>
  			</c:if>
	  </tbody>
	</table> 
	<!-- <fieldset class="layui-elem-field layui-field-title">
	  <legend>考试记录</legend>
	</fieldset>   
	<div style="background: #fafafa;">
	<form class="layui-form" action="">
		<div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label">试卷名称</label>
		      <div class="layui-input-inline">
		        <input type="tel" name="phone" lay-verify="phone" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">是否及格</label>
		      <div class="layui-input-inline">
		        <select name="interest" lay-filter="aihao">
			        <option value="">==请选择==</option>
			        <option value="0">未及格</option>
			        <option value="1">及格</option>
			     </select>
		      </div>
		    </div>
		    <div class="layui-inline">
		      <a class="layui-btn layui-btn-small">查询</a>
      		  <a class="layui-btn layui-btn-primary layui-btn-small">重置</a>
		    </div>
		 </div>
		 </form>
	</div>
	<table id="mytest_testList"></table> -->
</div>
<script type="text/javascript">
	layui.use('form', function(){
	  var form = layui.form();
	  form.render();
	});
	
	//跳转到考试页面
	function mytest_startTest(id){
		localtionTab('我的测试','${ctx}/testController/toTest.action?id='+id,'开始测试')
	}
</script>