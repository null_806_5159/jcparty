<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<style>
	a{color: #000;}
</style>
<div style="padding: 20px 30px;background: #fafafa;height: 100%;">
	<fieldset class="layui-elem-field layui-field-title">
		<legend>待我审批事项</legend>
	</fieldset>
	<div class="layui-form">
		<table class="layui-table">
			<colgroup>
				<col width="25%">
				<col width="25%">
				<col width="25%">
				<col width="25%">
				<col>
			</colgroup>
			<thead>
				<tr>
					<th>申请标题</th>
					<th>申请人</th>
					<th>申请时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
			<c:if test="${(applyInfoList)!= null && fn:length(applyInfoList) > 0}">
				<c:forEach var="apply"   items="${applyInfoList}">
					<tr>
				      <td>${apply.title }</td>
				      <td>${apply.applicantNm }</td>
				      <td>${apply.applydate }</td>
				      <td style="text-align: center;">
				      	<a href="javascript:myapprove_approve('${apply.id }');" style="cursor: pointer;">[审核]</a>
				      </td>
				    </tr>
				</c:forEach>
			</c:if>
			<c:if test="${(applyInfoList)== null || fn:length(applyInfoList) == 0}">
			  	<tr>
			  		<td colspan="4" style="text-align: center;">您当前没有需要审批的事项！</td>
			  	</tr>
			 </c:if>
			</tbody>
		</table>
	</div>
</div>
<div id="myapprove_detail" style="display: none;">Dialog Content.</div> 
<script type="text/javascript">
	function myapprove_approve(id){
		$('#myapprove_detail').dialog({    
		    title: '申请审核',    
		    width: 800,    
		    height: 600,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/applicationController/toApproveDetail.action?id='+id,    
		    modal: true   
		});    
	}
</script>