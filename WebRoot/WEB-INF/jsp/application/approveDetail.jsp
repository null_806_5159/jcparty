<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<div style="padding: 20px 30px;background: #fefefe;">
	<form class="layui-form layui-form-pane" action="">
	  <div class="layui-form-item">
	    <label class="layui-form-label" style="width: 112px;">申请标题</label>
	    <div class="layui-input-block">
	      <input type="text" value="${approve.title }" readonly class="layui-input">
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <div class="layui-inline">
	      <label class="layui-form-label" style="width: 112px;">申请人</label>
	      <div class="layui-input-block">
	        <input type="text" value="${approve.applicant }" readonly class="layui-input"/>
	      </div>
	    </div>
	    <div class="layui-inline">
	      <label class="layui-form-label" style="width: 112px;">申请时间</label>
	      <div class="layui-input-inline">
	        <input type="text" value="${approve.applydate }" readonly class="layui-input"/>
	      </div>
	    </div>
	  </div>
	  <div class="layui-form-item">
	    <div class="layui-inline">
	      <label class="layui-form-label" style="width: 112px;">所属专业</label>
	      <div class="layui-input-block">
	        <input type="text" readonly class="layui-input"/>
	      </div>
	    </div>
	    <div class="layui-inline">
	      <label class="layui-form-label" style="width: 112px;">角色</label>
	      <div class="layui-input-inline">
	        <input type="text" readonly class="layui-input"/>
	      </div>
	    </div>
	  </div>
	  <div class="layui-form-item layui-form-text">
	    <label class="layui-form-label">申请详情</label>
	    <div class="layui-input-block" style="border:1px solid #Fff;">
	      ${approve.content }
	    </div>
	  </div>
	  <hr>
	  <div class="layui-form-item">
	    <label class="layui-form-label">审批状态</label>
	    <div class="layui-input-block">
	      <input type="radio" name="state" value="2" title="通过">
	      <input type="radio" name="state" value="3" title="不通过">
	    </div>
	  </div>
	  <div class="layui-form-item layui-form-text">
	    <label class="layui-form-label">审批理由</label>
	    <div class="layui-input-block">
	      <textarea placeholder="请输入内容" name="result" class="layui-textarea"></textarea>
	    </div>
	  </div>
	  <div class="layui-form-item" style="text-align: center;">
	      <a class="layui-btn" href="javascript:approveDetail_sub();">提交</a>
	      <a class="layui-btn layui-btn-primary" href="javascript:approveDetail_back();">重置</a>
	  </div> 
  	</form>
</div>
<script type="text/javascript">
	layui.use('form', function(){
		  var form = layui.form();
		  form.render();
	});
	
	//提交审批结果
	function approveDetail_sub(){
		var id = "${approve.id }";
		var state = $("input[name='state']:checked").val();
		var result = $("textarea[name='result']").val();
		$.ajax({
			type:"post",
			url:"${ctx}/applicationController/approve.action",
			data:{
				id:id,
				state:state,
				result:result
			},
			dataType:"json",
			success:function(msg){
				if(msg.result=='success'){
	            	$.messager.alert('提示','操作成功！');
	            	//刷新当前的tab页，关闭dialog
	            	parent.window.refreshTab("${ctx}/personInfoController/toRewardInfo.action");
	            	$('#myapprove_detail').dialog("close");
	            }else{
	            	$.messager.alert('提示','操作失败，请联系管理员！'); 
	            }
			}
		})
	}
	
	//退出返回
	function approveDetail_back(){
		$.messager.confirm('确认','您确认退出操作吗？',function(r){    
		    if (r){    
		    	$('#myapprove_detail').dialog("close");
		    }    
		});
	}
</script>