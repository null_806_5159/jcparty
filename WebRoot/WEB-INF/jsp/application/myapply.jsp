<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<link rel="stylesheet" href="${ctx}/resources/style/applyFor.css">
<script type="text/javascript">
	$(function(){
		initDate("apply_startDate","apply_endDate");
	})
</script>
<style>
	a{color: #000;}
</style>
<div id="myapply_applyResult"
	style="background: #f1f1f1; padding: 20px 30px;">
	<div class="process_top" style="margin-left: 7%;">
		<div class="process_top_round">
			<div class="process_top_round_font">
				<font>我的申请</font>
			</div>
		</div>
		<div class="process_index">
			<div class="process_index_line" style="margin-bottom: 0;">
				<span class="process_btn_bg"> <em>1</em> <font>发起申请</font>
				</span> <span class="arrows_1"></span> <span class="process_btn_bg">
					<em>2</em> <font>审批中</font>
				</span> <span class="arrows_1"></span> <span class="process_btn_bg_2">
					<em>3</em> <font>已审批</font>
				</span>
			</div>
		</div>
	</div>
	
</div>
<div style="width: 100%;">
	<table id="myapply_applyList"></table>
</div>
<div id="apply_tools" style="padding-top: 15px;">
		<form id="myapply_query_form" class="layui-form"
			action="${ctx }/applicationController/getApplyList.action">
			<div class="layui-form-item">
			    <div class="layui-inline" style="margin-right: 0;">
			      <label class="layui-form-label">申请标题</label>
			      <div class="layui-input-inline">
			        <input type="text" name="title" style="width: 150px;" class="layui-input">
			      </div>
			    </div>
			    <div class="layui-inline">
			      <label class="layui-form-label">受理状态</label>
			      <div class="layui-input-inline">
			        <select name="state">
				        <option value=""></option>
				        <option value="1">受理中</option>
				        <option value="2">通过</option>
				        <option value="3">不通过</option>
				    </select>
			      </div>
			    </div>
			    <div class="layui-inline">
			      <label class="layui-form-label">时间区间</label>
			      <div class="layui-input-inline" style="width: 150px;">
			        <input type="text" id="apply_startDate" name="startDate" autocomplete="off" class="layui-input">
			      </div>
			      <div class="layui-form-mid">-</div>
			      <div class="layui-input-inline" style="width: 150px;">
			        <input type="text" id="apply_endDate" name="endDate" autocomplete="off" class="layui-input">
			      </div>
			    </div>
			    <div class="layui-inline">
					<a href="javascript:myapply_query();" class="layui-btn  layui-btn-small">查询</a> 
					<a href="javascript:myapply_add();" class="layui-btn  layui-btn-small">新增</a>
				</div>
			  </div>
		</form>
	</div>
	<div id="myapply_addDiv" style="display: none;"></div>
<script>
	layui.use('form', function() {
		var form = layui.form();
		form.render();
	});

	$(function() {
		$("#myapply_applyList").datagrid({
			fitColumns : true,
			url : "${ctx }/applicationController/getApplyList.action",
			singleSelect : true,
			height : 390,
			idField : 'id',
			toolbar: '#apply_tools',
			rownumbers : true,
			pagination : true,
			columns : [ [ {
				field : 'title',
				title : '标题',
				width : 150,
				align : 'center'
			}, {
				field : 'applydate',
				title : '申请时间',
				width : 150,
				align : 'center'
			}, {
				field : 'acceptorNm',
				title : '受理人',
				width : 150,
				align : 'center'
			}, {
				field : 'result',
				title : '受理结果',
				width : 150,
				align : 'center'
			}, {
				field : 'operate',
				title : '操作',
				width : 150,
				align : 'center',
				formatter : function(value, row, index) {
					return "<span><a style='cursor: pointer;'>[查看流程]</a>　　<a style='cursor: pointer;'>[申请详情]</a></span>";
				}
			} ] ]
		});

		//日期设置
		var start = {
			elem : '#myapply_startDate', //选择ID为START的input  
			format : 'YYYY-MM-DD', //自动生成的时间格式  
			max : laydate.now(), //最大日期  
			istime : true, //必须填入时间  
			istoday : false, //是否是当天  
			choose : function(datas) {
				end.min = datas; //开始日选好后，重置结束日的最小日期  
				end.start = datas; //将结束日的初始值设定为开始日  
			}
		};
		var end = {
			elem : '#myapply_endDate',
			format : 'YYYY-MM-DD',
			max : laydate.now(),
			istime : true,
			istoday : true,
			choose : function(datas) {
				start.max = datas; //结束日选好后，重置开始日的最大日期  
			}
		};
		laydate(start);
		laydate(end);
	})
	
	//查询
	function myapply_query() {
		$('#myapply_applyList').datagrid('load',{  
			title : $("input[name='title']").val(), //申请标题
			startDate : $("input[name='startDate']").val(),//开始日期
			endDate : $("input[name='endDate']").val(),//结束时间
			state : $("select[name='state']").val()//状态
		});
	}
	
	//发起申请
	function myapply_add(){
		$('#myapply_addDiv').dialog({    
		    title: '发起申请',    
		    width: 800,    
		    height: 600,    
		    closed: false,    
		    cache: false,    
		    href: '${ctx}/applicationController/toSaveApplyUI.action',    
		    modal: true   
		}); 
	}
</script>