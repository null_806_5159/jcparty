<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/tag.jsp"%>
<script type="text/javascript">
	//实例化编辑器
	UE.delEditor("saveApply_centent");//在页面每次初始化的时候，先删除掉以前的编辑器，再次进行初始化,否则会出现渲染失败的情景
	var ue = UE.getEditor('saveApply_centent', {
	    toolbars: [[
			'bold', //加粗
			'indent', //首行缩进
			'fontfamily', //字体
	        'fontsize', //字号
	        'paragraph', //段落格式
	        'justifyleft', //居左对齐
	        'justifyright', //居右对齐
	        'justifycenter', //居中对齐
	        'justifyjustify', //两端对齐
	        'forecolor', //字体颜色
	    ]],
	    autoHeightEnabled: true,
	    autoFloatEnabled: true,
	    elementPathEnabled:false,
	    maximumWords:500
	});
</script>
<div style="padding: 20px 20px;">
	<form id="saveApply_form" class="layui-form layui-form-pane" action="">
		<div class="layui-form-item">
			<label class="layui-form-label" style="width: 112px;">申请标题</label>
			<div class="layui-input-block">
				<input type="text" id="saveApply_form_title" name="title" autocomplete="off"
					placeholder="请输入标题" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label" style="width: 112px;">受理人</label>
			<div class="layui-input-block">
				<input type="text" id="saveApply_AcceptorNm" name="acceptorNm" value=""
					placeholder="点击选择" onclick="saveApply_selectAcceptor()" class="layui-input">
				<input type="hidden" id="saveApply_AcceptorId" name="acceptorId" value=""
					placeholder="点击选择" onclick="saveApply_selectAcceptor()" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item layui-form-text">
		    <label class="layui-form-label">申请内容</label>
		    <div class="layui-input-block">
		      <textarea name="content" id="saveApply_centent" style="height: 200px;"></textarea>
		    </div>
		 </div>
		<div class="layui-form-item" style="text-align: center;">
		    <a href="javascript:saveApply_save();" class="layui-btn" >提交</a>
      		<a href="javascript:saveApply_back();" class="layui-btn layui-btn-primary">返回</a>
		 </div>
	</form>
	<div id="selectAcceptor" style="display: none;text-align: center;padding-top: 20px;">
		<input id="selectAcceptor_" style="height: 30px;width: 150px;"/>
	</div>
</div>
<script type="text/javascript">
	layui.use('form',function(){
		  var form = layui.form();
		  form.render();
	});
	
	//选择受理人
	function saveApply_selectAcceptor(){
		$("#selectAcceptor_").combobox({
			valueField: 'id',    
	        textField: 'name',    
	        url: '${ctx }/userController/getCounselorList.action',    
	        onSelect: function(rec){    
	              $("#saveApply_AcceptorNm").val(rec.name); 
	              $("#saveApply_AcceptorId").val(rec.id); 
	              $("#selectAcceptor").dialog("close");
	        }
		})
		$('#selectAcceptor').dialog({    
		    title: '选择受理人',    
		    width: 400,    
		    height: 200,    
		    closed: false,    
		    cache: false,      
		    modal: true   
		});
	}
	
	//保存提交
	function saveApply_save(){
		$.messager.confirm('确认','您确认提交该申请吗？',function(r){    
		    if (r){    
		    	var title = $("#saveApply_form_title").val();
				var acceptorId = $("input[name='acceptorId']").val();
				var content = UE.getEditor('saveApply_centent').getAllHtml();
				if(title == null || title.length==0){
					$.messager.alert('提示','请填写申请标题！');
					return false;
				}
				if(acceptorId == null || acceptorId.length==0){
					$.messager.alert('提示','请选择申请受理人！');
					return false;
				}
				if(content == null || content.length==0){
					$.messager.alert('提示','请填写申请内容！');
					return false;
				}
				$.ajax({
					type:"post",
					url:"${ctx}/applicationController/saveApply.action",
					data:$("#saveApply_form").serialize(),
					dataType:"json",
					success:function(msg){
						if(msg.result=='success'){
			            	$.messager.alert('提示','操作成功！');
			            	window.parent.$('#myapply_applyList').datagrid("reload");
			            	$('#myapply_addDiv').dialog("close");   
			            }else{
			            	$.messager.alert('提示','操作失败，请联系管理员！'); 
			            }
					}
				})
		    }    
		});  
	}
	
	//退出返回
	function saveApply_back(){
		$.messager.confirm('确认','您确认想要退出操作吗？',function(r){    
		    if (r){    
		    	$('#myapply_addDiv').dialog("close");  
		    }    
		}); 
	}
</script>