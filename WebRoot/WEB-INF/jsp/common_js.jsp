<script type="text/javascript" src="${ctx }/resources/plug-in/easyUI/jquery.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/plug-in/easyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/plug-in/easyUI/locale/easyui-lang-zh_CN.js"></script>

<script type="text/javascript" src="${ctx }/resources/script/common.js"></script>

<script type="text/javascript" src="${ctx }/resources/plug-in/echarts-2.2.7/build/dist/echarts.js"></script>

<script type="text/javascript" src="${ctx }/resources/plug-in/layui/layui.js"></script>



<!-- Ueditor import files -->
<script type="text/javascript" src="${ctx}/resources/plug-in/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/resources/plug-in/ueditor/ueditor.all.js"></script>

<script type="text/javascript" src="${ctx}/resources/plug-in/laydate-master/laydate.dev.js"></script>

<script src="${ctx }/resources/script/slide.js"></script>
<script src="${ctx }/resources/script/prettify.js"></script>




