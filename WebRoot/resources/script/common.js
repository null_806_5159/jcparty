/**
 * @author neil
 * 
 */
/**
 * 初始化一个时间区间
 * @param startId 开始时间input
 * @param endId  结束时间input
 */
function initDate(startId,endId){
	//日期设置
    var start = {  
        elem: "#"+startId, //选择ID为START的input  
        format: 'YYYY-MM-DD', //自动生成的时间格式  
        max: laydate.now(), //最大日期  
        istime: true, //必须填入时间  
         istoday: false,  //是否是当天  
         choose: function(datas){  
              end.min = datas; //开始日选好后，重置结束日的最小日期  
              end.start = datas; //将结束日的初始值设定为开始日  
         }  
	};  
 	var end = {  
     	elem: "#"+endId,  
    	format: 'YYYY-MM-DD',   
     	max: laydate.now(),  
    	istime: true,  
     	istoday: false,  
	 	choose: function(datas){  
        	start.max = datas; //结束日选好后，重置开始日的最大日期  
   		}  
 	};  
   	laydate(start);  
    laydate(end); 
}

function validDate(id){
	var validDate = {  
	     	elem: "#"+id,  
	    	format: 'YYYY-MM-DD hh:mm:ss',   
	     	min: laydate.now(),  
	    	istime: true,  
	     	istoday: false
	};  
	laydate(validDate);  
}