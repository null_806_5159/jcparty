package edu.nuaa.party.web.ports;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import edu.nuaa.party.web.service.PersonInfoService;
import edu.nuaa.party.web.service.SysService;
import edu.nuaa.party.web.vo.PortsResultVo;
import edu.nuaa.party.web.vo.RewardInfo;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.SysUser;
@Controller
@RequestMapping("/portsLoginController")
public class PortsLoginController {

	private Logger logger = Logger.getLogger(this.getClass());
	private Gson gson = new Gson();
	
	@Autowired
	private PersonInfoService personInfoService;
	@Autowired
	private SysService sysService;
	
	@RequestMapping("/feedback")
	@ResponseBody
	public String feedback(@RequestParam String usercode, @RequestParam String password) throws Exception{
		SysUser user = this.sysService.checkUser(usercode,password);
		PortsResultVo<RewardInfo> result = new PortsResultVo<RewardInfo>();
		if(user != null){
			result.setError_code(1);
			result.setReason("请求成功");
			List<RewardInfo> rewards = this.personInfoService.getRewardListByUserId("33e0951d-1616-11e7-b1cc-00163e1a7a5d");
			result.setData(rewards);
		}else{
			result.setError_code(0);
			result.setReason("用户名或密码错误！");
			result.setData(new ArrayList<RewardInfo>());
			
		}
		return gson.toJson(result);
	 }
}
