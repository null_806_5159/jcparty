package edu.nuaa.party.web.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.nuaa.party.web.service.PersonInfoService;
import edu.nuaa.party.web.utils.StringUtils;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.BaseInfoShowVo;
import edu.nuaa.party.web.vo.BaseInfoVo;
import edu.nuaa.party.web.vo.HomeNumVo;
import edu.nuaa.party.web.vo.PartyInfo;
import edu.nuaa.party.web.vo.PartyInfoShowVo;
import edu.nuaa.party.web.vo.RewardInfo;
import edu.nuaa.party.web.vo.SysConstant;
/**
 * 个人档案管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/personInfoController")
public class PersonInfoController {

	@Autowired
	private PersonInfoService personInfoService;
	
	/**
	 * 跳转到个人基本信息页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toBaseInfo")
	public String toBaseInfo(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		BaseInfoShowVo baseInfo = this.personInfoService.getUserBaseInfoByUserId(activeUser.getUserId());
		
		request.setAttribute("baseInfo", baseInfo);
		
		return "personInfo/baseInfo";
	}
	
	/**
	 * 跳转到党内信息页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toPartyInfo")
	public String toPartyInfo(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		PartyInfoShowVo partyInfo = this.personInfoService.getPartyInfoByUserId(activeUser.getUserId());
		
		request.setAttribute("partyInfo", partyInfo);
		
		return "personInfo/partyInfo";
	}
	
	/**
	 * 跳转到奖惩情况页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toRewardInfo")
	public String toRewardInfo(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		List<RewardInfo> rewards = this.personInfoService.getRewardListByUserId(activeUser.getUserId());
		request.setAttribute("rewards", rewards);
		return "personInfo/rewardInfo";
	}
	
	/**
	 * 跳转到保存基本信息页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toSaveUI")
	public String toSaveUI(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		BaseInfoShowVo baseInfo = this.personInfoService.getUserBaseInfoByUserId(activeUser.getUserId());
		request.setAttribute("baseInfo", baseInfo);
		return "personInfo/saveBaseInfo";
	}
	
	/**
	 * 基本信息保存
	 * @param baseInfo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveBaseInfo")
	@ResponseBody
	public String saveBaseInfo(BaseInfoVo baseInfo,HttpServletRequest request) throws Exception{
		String result = SysConstant.OPERATE_RESULT_SUCCESS;
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		baseInfo.setUserid(activeUser.getUserId());
		if(baseInfo != null){
			String id = baseInfo.getId();
			if(id != null && id.length()>0){//修改操作
				result = this.personInfoService.editBaseInfo(baseInfo);
			}else{//新增操作
				result = this.personInfoService.addBaseInfo(baseInfo);
			}
		}
		return result;
	}
	
	//跳转到编辑党内信息页面
	@RequestMapping("/toSavePartyUI")
	public String toSavePartyUI(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		PartyInfoShowVo partyInfo = this.personInfoService.getPartyInfoByUserId(activeUser.getUserId());
		request.setAttribute("partyInfo", partyInfo);
		return "personInfo/savePartyInfo";
	}
	
	//保存党内信息
	@RequestMapping("/savePartyInfo")
	@ResponseBody
	public String savePartyInfo(PartyInfo partyInfo,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		partyInfo.setUserid(activeUser.getUserId());
		String result = this.personInfoService.savePartyInfo(partyInfo);
		return result;
	}
	
	//跳转到编辑奖惩信息页面
	@RequestMapping("/toSaveRewardUI")
	public String toSaveRewardUI(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id")==null?"":request.getParameter("id");
		if(id != null && id.length()>0){
			RewardInfo rewardInfo = this.personInfoService.getRewardInfoById(id);
			request.setAttribute("rewardInfo", rewardInfo);
		}
		return "personInfo/saveRewardInfo";
	}
	
	//保存获奖信息
	@RequestMapping("/saveRewardInfo")
	@ResponseBody
	public String saveRewardInfo(RewardInfo rewardInfo,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		rewardInfo.setUserid(activeUser.getUserId());
		String result = this.personInfoService.saveRewardInfo(rewardInfo);
		return result;
	}
	
	//删除获奖信息
	@RequestMapping("/deleteReward")
	@ResponseBody
	public String deleteReward(String id) throws Exception{
		String result = this.personInfoService.deleteReward(id);
		return result;
	}
	
	/**
	 * 跳转到家庭主要成员列表页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toHomeNumInfo")
	public String toHomeNumInfo(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		List<HomeNumVo> homeNums = this.personInfoService.getHomeNumListByUserId(activeUser.getUserId());
		request.setAttribute("homeNums", homeNums);
		return "personInfo/homeInfo";
	}
	
	//跳转到编辑家庭主要成员信息页面
	@RequestMapping("/toSaveHomeNumUI")
	public String toSaveHomeNumUI(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id")==null?"":request.getParameter("id");
		if(id != null && id.length()>0){
			HomeNumVo homeNum = this.personInfoService.getHomeNumInfoById(id);
			request.setAttribute("homeNum", homeNum);
		}
		return "personInfo/saveHomeInfo";
	}
	
	//保存家庭成员信息
	@RequestMapping("/saveHomeNumInfo")
	@ResponseBody
	public String saveHomeNumInfo(HomeNumVo homeNum,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		homeNum.setUserid(activeUser.getUserId());
		String result = this.personInfoService.saveHomeNumInfo(homeNum);
		return result;
	}
	
	//删除获奖信息
	@RequestMapping("/deleteHomeNum")
	@ResponseBody
	public String deleteHomeNum(String id) throws Exception{
		String result = this.personInfoService.deleteHomeNum(id);
		return result;
	}
	
	/**
	 * 头像上传
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/avatarUpload")
	@ResponseBody
	public String avatarUpload(@RequestParam MultipartFile file,HttpServletRequest request) throws Exception{
		//处理上传的文件
		SimpleDateFormat sdf = new SimpleDateFormat("/yyyy/MM/dd/");
		//1、得到保存路径的绝对地址
		/*String basePath = request.getSession().getServletContext().getRealPath("")
				+ "_upload/news_upload".replace("\\", "/");*/
		String basePath = SysConstant.SERVER_UPLOAD_ADDR;//服务器上的
		//String basePath = SysConstant.LOCAL_UPLOAD_ADDR;//本地的
		String datePath = sdf.format(new Date());
		String fileName = StringUtils.getPKByUUID() + "." +file.getOriginalFilename().split("\\.")[1];
		File dir = new File(basePath+datePath);
		if(!dir.exists()){
			dir.mkdirs();
		}
		String path = basePath + datePath + fileName;//上传文件最终路径
		File destFile = new File(path);		
		//移动文件
		file.transferTo(destFile);
		//返回上传状态
		return "{\"code\": 0,\"msg\": \"\",\"data\": {\"src\": \""+sdf.format(new Date())+ fileName+"\"}}";
	}
}
