package edu.nuaa.party.web.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import edu.nuaa.party.web.service.NewsService;
import edu.nuaa.party.web.utils.StringUtils;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.IndexNewsVo;
import edu.nuaa.party.web.vo.NewsInfo;
import edu.nuaa.party.web.vo.NewsTypeVo;
import edu.nuaa.party.web.vo.NewsVo;
import edu.nuaa.party.web.vo.QueryNews;
import edu.nuaa.party.web.vo.SysConstant;
/**
 * 文章资料
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/newsController")
public class NewsController {

	@Autowired
	private NewsService newsService;
	
	private Gson gson = new Gson();
	
	@RequestMapping("/toIndex")
	public String toIndex() throws Exception{
		
		return "news/newsList";
	}
	
	@RequestMapping("/toSaveUI")
	public String toSaveUI() throws Exception{
		
		return "news/addNews";
	}
	/**
	 * 获取文章专题
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getNewsType")
	@ResponseBody
	public String getNewsType() throws Exception{
		String typeComboboxStr = newsService.getNewsType();
		return typeComboboxStr;
	}
	
	/**
	 * 添加文章专题
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/addType")
	@ResponseBody
	public String addType(NewsTypeVo newsType,HttpServletRequest request) throws Exception{
		//获取当前的用户
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		newsType.setCreator(activeUser.getUserId());
		newsType.setCreator("admin");
		String resultStr = newsService.addType(newsType);
		return resultStr;
	}
	
	/**
	 * 添加文章插图
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveIamg")
	@ResponseBody
	public String saveIamg(@RequestParam MultipartFile file,HttpServletRequest request) throws Exception{
		//处理上传的文件
		SimpleDateFormat sdf = new SimpleDateFormat("/yyyy/MM/dd/");
		//1、得到保存路径的绝对地址
		/*String basePath = request.getSession().getServletContext().getRealPath("")
				+ "_upload/news_upload".replace("\\", "/");*/
		String basePath = SysConstant.SERVER_UPLOAD_ADDR;//服务器上的
		//String basePath = SysConstant.LOCAL_UPLOAD_ADDR;//本地的
		String datePath = sdf.format(new Date());
		String fileName = StringUtils.getPKByUUID() + "." +file.getOriginalFilename().split("\\.")[1];
		File dir = new File(basePath+datePath);
		if(!dir.exists()){
			dir.mkdirs();
		}
		String path = basePath + datePath + fileName;//上传文件最终路径
		File destFile = new File(path);		
		//移动文件
		file.transferTo(destFile);
		//返回上传状态
		return "{\"code\": 0,\"msg\": \"\",\"data\": {\"src\": \""+sdf.format(new Date())+ fileName+"\"}}";
	}
	
	/**
	 * 保存文章
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveNews")
	@ResponseBody
	public String saveNews(NewsVo vo,HttpServletRequest request) throws Exception{
		String id = vo.getId();
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		if(id != null && id.length()>0){
			vo.setUpdator(activeUser.getUserId());
		}else{
			vo.setCreator(activeUser.getUserId());
		}
		String resultStr = newsService.saveNews(vo);
		return resultStr;
	}
	
	/**
	 * 获取文章列表
	 * @param queryNews
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getNewsList")
	@ResponseBody
	public String getNewsList(QueryNews queryNews) throws Exception{
		String newsListStr = newsService.getNewsList(queryNews);
		return newsListStr;
	}
	
	/**
	 * 文章展示
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/showNewsDetails")
	public String showNewsDetails(String id,HttpServletRequest request) throws Exception{
		NewsInfo news = newsService.getNewsInfo(id);
		request.setAttribute("news", news);
		return "news/newsDetail";
	}
	
	/**
	 * 首页根据文章专题获取文章列表
	 * @param typeId 文章标题id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getNewsListBytype")
	@ResponseBody
	public String getNewsListBytype(String typeId) throws Exception{
		List<IndexNewsVo> news = this.newsService.getIndexNewsList(typeId);
		String result = "[]";
		if(news.size()>0){
			result = gson.toJson(news);
		}
		return result;
	}
	
	//首页查看文章详情
	@RequestMapping("/showIndexNewsDetails")
	public String showIndexNewsDetails(String id,HttpServletRequest request) throws Exception{
		NewsInfo news = newsService.getNewsInfo(id);
		request.setAttribute("news", news);
		return "index/newsDetails";
	}
	
	/**
	 * 文章材料上线、下线
	 * @param id 材料id
	 * @param type 操作标识    0下线   1上线
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/updateNewsState")
	@ResponseBody
	public String updateNewsState(String id,String type) throws Exception{
		String result = this.newsService.updateNewsState(id,type);
		return result;
	}
}
