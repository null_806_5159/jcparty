package edu.nuaa.party.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.PermissionService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.PermissionVo;

/**
 * 权限管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/permissionController")
public class PermissionController {

	@Autowired
	private PermissionService permissionService;
	
	/**
	 * 角色授权树展示
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getPermissionTree")
	@ResponseBody
	public String getPermissionTree() throws Exception{
		String treeStr = this.permissionService.getPermissionTree().replaceAll("func_nm", "text");
		return treeStr;
	}
	
	//跳转到权限管理页面
	@RequestMapping("/toList")
	public String toList() throws Exception{
		
		return "permission/permissionList";
	}
	
	//跳转到编辑权限页面
	@RequestMapping("/toSavePermissionUI")
	public String toSavePermissionUI(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id")==null?"":request.getParameter("id");
		if(id != null && id.length()>0){
			PermissionVo permission = this.permissionService.getPermissionById(id);
			request.setAttribute("permission", permission);
		}
		return "permission/savePermission";
	}
	/**
	 * 根据权限等级获取下拉列表
	 * @param lever
	 * @return
	 */
	@RequestMapping("/getPermission")
	@ResponseBody
	public String getPermission(String lever) throws Exception{
		String result = this.permissionService.getPermissionByLever(lever);
		return result;
	}
	
	//保存权限信息
	@RequestMapping("/savePermission")
	@ResponseBody
	public String savePermission(PermissionVo permission,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		String id = permission.getId();
		String result = "";
		if(id != null && id.length()>0){
			permission.setUpdator(activeUser.getUserId());
			result = this.permissionService.editPermission(permission);
		}else{
			permission.setCreator(activeUser.getUserId());
			result = this.permissionService.addPermission(permission);
		}
		return result;
	}
	
	//保存权限信息
	@RequestMapping("/deletePermission")
	@ResponseBody
	public String deletePermission(String id) throws Exception{
		String result = this.permissionService.deletePermission(id);
		return result;
	}
	/**
	 * 根据角色id回显权限数据
	 * @param roleId 角色id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getRolePeermission")
	@ResponseBody
	public String getRolePeermission(String id) throws Exception{
		String result = this.permissionService.getRolePeermission(id);
		return result;
	}
}
