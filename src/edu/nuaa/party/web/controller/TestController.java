package edu.nuaa.party.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.TestService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.QueryTest;
import edu.nuaa.party.web.vo.TestQuestionVo;
import edu.nuaa.party.web.vo.TestVo;
/**
 * 测试管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/testController")
public class TestController {

	@Autowired
	private TestService testService;
	
	
	//跳转到用户测试记录页面
	@RequestMapping("/toTestRecord")
	public String toTestRecord(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		List<TestVo> testList = this.testService.getWaitTestList(activeUser.getDeptId(),activeUser.getUserId());
		request.setAttribute("testList", testList);
		return "test/testRecord";
	}
	
	//跳转到用户测试记录页面
	@RequestMapping("/toTest")
	public String toTest(String id,HttpServletRequest request) throws Exception{
		List<TestQuestionVo> questionList = this.testService.getQuestionListByTestId(id);
		TestVo test = this.testService.getTestById(id);
		request.setAttribute("questionList", questionList);
		request.setAttribute("test", test);
		return "test/test";
	}
	
	//跳转到测试管理页面
	@RequestMapping("/toTestManage")
	public String toTestManage() throws Exception{
		
		return "test/testManage";
	}
	
	//跳转到编辑测试页面
	@RequestMapping("/toSaveTestUI")
	public String toSaveTestUI() throws Exception{
		return "test/saveTest";
	}
	
	//跳转到添加试题页面
	@RequestMapping("/toTopicUI")
	public String toTopicUI() throws Exception{
		
		return "test/addTopic";
	}
	
	//获取试题列表信息
	@RequestMapping("/getTestList")
	@ResponseBody
	public String getTestList(QueryTest queryTest) throws Exception{
		
		String result = this.testService.getTestList(queryTest);
		return result;
	}
	
	//试卷发布与失效
	@RequestMapping("/updateTestState")
	@ResponseBody
	public String updateTestState(String id,String type) throws Exception{
		String result = this.testService.updateTestState(id,type);
		return result;
	}
	
	//试卷发布与失效
	@RequestMapping("/subScore")
	@ResponseBody
	public String subScore(String score,String testId,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		String result = this.testService.saveScore(score,testId,activeUser.getUserId());
		return result;
	}
}
