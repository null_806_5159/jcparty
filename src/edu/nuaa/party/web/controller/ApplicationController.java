package edu.nuaa.party.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.ApplicationService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.ApplyInfo;
import edu.nuaa.party.web.vo.ApproveInfo;
import edu.nuaa.party.web.vo.QueryApply;

/**
 * 申请审批
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/applicationController")
public class ApplicationController {

	@Autowired
	private ApplicationService applicationService;
	
	//跳转到我的申请页面
	@RequestMapping("/toIndex")
	public String toIndex() throws Exception{
		
		return "application/myapply";
	}
	
	/**
	 * 查询我的申请的列表
	 * @param applyCondition
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getApplyList")
	@ResponseBody
	public String getApplyList(QueryApply applyCondition) throws Exception{
		String result = this.applicationService.getApplyList(applyCondition);
		return result;
	}
	
	//跳转到待我审批
	@RequestMapping("/toApprove")
	public String toApprove(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		List<ApplyInfo> applyInfoList = this.applicationService.getApproveList(activeUser.getUserId());
		request.setAttribute("applyInfoList", applyInfoList);
		return "application/myapprove";
	}
	
	//跳转到发起申请页面
	@RequestMapping("/toSaveApplyUI")
	public String toSaveApplyUI() throws Exception{
			
		return "application/saveApply";
	}
	
	//保存申请内容
	@RequestMapping("/saveApply")
	@ResponseBody
	public String saveApply(ApplyInfo apply,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		apply.setApplicant(activeUser.getUserId());
		String result = this.applicationService.saveApply(apply);
		return result;
	}
	
	//跳转到申请审核页面
	@RequestMapping("/toApproveDetail")
	public String toApproveDetail(String id,HttpServletRequest request) throws Exception{
		ApproveInfo approve = this.applicationService.getApplyDetailById(id);
		request.setAttribute("approve", approve);
		return "application/approveDetail";
	}
	
	//提交审批结果
	@RequestMapping("/approve")
	@ResponseBody
	public String approve(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id");
		String state = request.getParameter("state");
		String result = request.getParameter("result");
		String jsonStr = this.applicationService.approve(id,state,result);
		return jsonStr;
	}
}
