package edu.nuaa.party.web.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

import edu.nuaa.party.web.service.ThinkService;
import edu.nuaa.party.web.utils.StringUtils;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.QueryThink;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.ThoughtreportVo;
/**
 * 思想汇报的上传
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/thinkController")
public class ThinkController {

	@Autowired
	private ThinkService thinkService;
	
	@RequestMapping("/toIndex")
	public String toIndex() throws Exception{
		
		return "thinkUpload/uploadIndex";
	}
	
	//上传图片
	@RequestMapping("/uploadImag")
	@ResponseBody
    public String handleRequest(@RequestParam(value="fileList",required=false) MultipartFile[] file,HttpServletRequest request,HttpServletResponse response) throws Exception {

      System.out.println(file.length);
    //处理上传的文件
    		SimpleDateFormat sdf = new SimpleDateFormat("/yyyy/MM/dd/");
    		//1、得到保存路径的绝对地址
    		String basePath = SysConstant.SERVER_UPLOAD_ADDR;//上传路径
    		String datePath = sdf.format(new Date());
    		String fileNames = "";
    		for(int i = 0;i<file.length;i++){
    			String fileName = StringUtils.getPKByUUID() + "." +file[i].getOriginalFilename().split("\\.")[1];
        		File dir = new File(basePath+datePath);
        		if(!dir.exists()){
        			dir.mkdirs();
        		}
        		String path = basePath + datePath + fileName;//上传文件最终路径
        		File destFile = new File(path);		
        		//移动文件
        		file[i].transferTo(destFile);
        		if(i != file.length - 1){
        			fileNames += sdf.format(new Date())+ fileName +";";
        		}else{
        			fileNames += sdf.format(new Date())+ fileName ;
        		}
    		}
    		
    		//返回上传状态
    		return "{\"code\": 0,\"msg\": \"\",\"data\": {\"src\": \""+fileNames+"\"}}";
	}
	
	//上传图片
	@RequestMapping("/saveThink")
	@ResponseBody
	public String saveThink(ThoughtreportVo think,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		think.setCreator(activeUser.getUserId());
		String result = this.thinkService.saveThink(think);
		return result;
	}
	
	//查看思想汇报上传记录
	@RequestMapping("/toHistoryUI")
	public String toHistoryUI() throws Exception{
		return "thinkUpload/historyList";
	}
	
	//上传图片
	@RequestMapping("/getThinkUploadList")
	@ResponseBody
	public String getThinkUploadList(QueryThink queryThink,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		queryThink.setCreator(activeUser.getUserId());
		String result = this.thinkService.getThinkUploadList(queryThink);
		return result;
	}
	
	//上传图片
	@RequestMapping("/toShowFullUI")
	public String toShowFullUI(String id,HttpServletRequest request) throws Exception{
		ThoughtreportVo think = this.thinkService.getThinkById(id);
		request.setAttribute("think", think);
		return "thinkUpload/thinkUploadDetail";
	}
}
