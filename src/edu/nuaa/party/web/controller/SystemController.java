package edu.nuaa.party.web.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.SysService;

@Controller
@RequestMapping("/systemController")
public class SystemController {

	@Autowired
	private SysService sysService;
	
	//获取省列表
	@RequestMapping("/getProvince")
	@ResponseBody
	public String getProvince() throws Exception{
		String result = this.sysService.getProvince();
		return result;
	}
	
	//获取地市列表
	@RequestMapping("/getCity")
	@ResponseBody
	public String getCity(String pcode) throws Exception{
		String result = this.sysService.getCity(pcode);
		return result;
	}
	
	//获取区县列表
	@RequestMapping("/getCounty")
	@ResponseBody
	public String getCounty(String pcode) throws Exception{
		String result = this.sysService.getCounty(pcode);
		return result;
	}
	
	//文档下载
	@RequestMapping("/downloadFile")
	public void downloadFile(String name,HttpServletRequest request,HttpServletResponse response) throws Exception{
		    response.setContentType("text/html;charset=UTF-8");   
	        BufferedInputStream in = null;  
	        BufferedOutputStream out = null;  
	        request.setCharacterEncoding("UTF-8");  
	        String filePath = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/file/";
	        String fileName = name + ".doc";
	        String name_ = URLEncoder.encode("党建系统操作指南.doc", "UTF-8"); 
	        try {  
	            File f = new File(filePath + fileName);  
	            response.setContentType("application/msword");  
	            response.setCharacterEncoding("UTF-8");  
	            response.setHeader("Content-Disposition", "attachment; filename="+name_);  
	            response.setHeader("Content-Length",String.valueOf(f.length()));  
	            in = new BufferedInputStream(new FileInputStream(f));  
	            out = new BufferedOutputStream(response.getOutputStream());  
	            byte[] data = new byte[1024];  
	            int len = 0;  
	            while (-1 != (len=in.read(data, 0, data.length))) {  
	                out.write(data, 0, len);  
	            }  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        } finally {  
	            if (in != null) {  
	                in.close();  
	            }  
	            if (out != null) {  
	                out.close();  
	            }  
	        }  
	}
}
