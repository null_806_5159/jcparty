package edu.nuaa.party.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import edu.nuaa.party.web.service.NewsService;
import edu.nuaa.party.web.service.SysService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.AjaxJson;
import edu.nuaa.party.web.vo.IndexNewsVo;
import edu.nuaa.party.web.vo.NewsTypeVo;
/**
 * 登录
 * @author Administrator
 *
 */
@Controller
public class LoginController {

	@Autowired
	private SysService sysService;
	
	@Autowired
	private NewsService newsService; 
	
	private Gson gson = new Gson();
	
	//系统首页
	@RequestMapping("/index")
	public String index(Model model,HttpServletRequest request)throws Exception{
		//int page = request.getParameter("page")==null?0:Integer.parseInt(request.getParameter("page"));
		//int offset = (10-1)*rows;
		String newType = request.getParameter("newType")==null?"":request.getParameter("newType");
		List<IndexNewsVo> news = this.newsService.getIndexNewsList(newType);
		List<NewsTypeVo> newsTypeList = this.newsService.getNewsTypeList();
		request.setAttribute("newsList", news);
		request.setAttribute("newsTypeList", newsTypeList);
		return "index/index";
	}
		
	//系统管理后台首页
	@RequestMapping("/manage")
	public String manage(Model model,HttpServletRequest request)throws Exception{

		return "/index";
	}
	
	@RequestMapping("/login")
	public String toLogin() throws Exception{
		return "login";
	}
	
	/**
	 * 用户认证
	 * @param user
	 * @param req
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/checkuser")
	@ResponseBody
	public String checkuser(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		AjaxJson j = new AjaxJson();
		String usercode = request.getParameter("usercode");
		String password = request.getParameter("password");
		if(usercode == null || usercode.trim().equals("")){
			j.setMsg("用户名或密码不可为空！");
			j.setSuccess(false);
			return gson.toJson(j);
		}
		if(password == null || password.toString().trim().equals("")){
			j.setMsg("用户名或密码不可为空！");
			j.setSuccess(false);
			return gson.toJson(j);
		}
		ActiveUser activeUser = this.sysService.authenticad(usercode, password);
		if(activeUser == null){
			j.setMsg("用户名或密码不正确！");
			j.setSuccess(false);
			return gson.toJson(j);
		}
		session.setAttribute("activeUser", activeUser);
		//return "redirect:/manage.action";
		return gson.toJson(j);
	}
	
	//退出
	@RequestMapping("/logout")
	public String logout(HttpSession httpSession) throws Exception{
		//清空session
		httpSession.invalidate();
			
		return "redirect:/login.action";
	}
}
