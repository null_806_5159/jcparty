package edu.nuaa.party.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.DepartmentServie;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.DeptmentVo;
/**
 * 系部管理
 * @author neil
 *
 */
@Controller
@RequestMapping("/departmentController")
public class DepartmentController {

	@Autowired
	private DepartmentServie departmentService;
	
	@RequestMapping("/toList")
	public String toList() throws Exception{
		
		return "department/deptList";
	}
	
	/**
	 * 获取部门树形的列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getDeptList")
	@ResponseBody
	public String getDeptList() throws Exception{
		String deptTreeStr = departmentService.getDeptList();
		return deptTreeStr;
	}
	
	/**
	 * 获取form表单的部门列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getDeptCombotree")
	@ResponseBody
	public String getDeptCombotree() throws Exception{
		String deptTreeStr = departmentService.getDeptList().replaceAll("name", "text");
		return deptTreeStr;
	}
	
	/**
	 * 获取系部的下拉框
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getDeptCombobox")
	@ResponseBody
	public String getDeptComboBox() throws Exception{
		String deptBoxStr = departmentService.getDeptComboBox();
		return deptBoxStr;
	}
	
	/**
	 * 根据Id删除系部专业
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/deleteDept")
	@ResponseBody
	public String deleteDept(String id) throws Exception{
		String resultStr = departmentService.deleteDept(id);
		return resultStr;
	}
	
	/**
	 * 获取部门管理的树形datatree
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getDeptTreeGrid")
	@ResponseBody
	public String getDeptTreeGrid() throws Exception{
		String resultStr = this.departmentService.getDeptTreeGrid();
		return resultStr;
	}
	
	//跳转到保存部门页面
	@RequestMapping("/toSaveDeptUI")
	public String toSaveDeptUI(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id");
		if(id != null && id.length()>0){
			DeptmentVo dept = this.departmentService.getDeptById(id);
			request.setAttribute("dept", dept);
		}
		return "department/saveDept";
	}
	
	/**
	 * 根据lever获取部门combobox
	 * @param lever
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getDeptByLever")
	@ResponseBody
	public String getDeptByLever(String lever) throws Exception{
		String result = this.departmentService.getDeptByLever(lever);
		return result;
	}
	
	/**
	 * 保存部门
	 * @param lever
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveDept")
	@ResponseBody
	public String saveDept(DeptmentVo dept,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		String result = this.departmentService.saveDept(dept,activeUser.getUserId());
		return result;
	}
	
	/**
	 * 获取可选负责人列表
	 * @param lever
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getDeptMa")
	@ResponseBody
	public String getDeptMa() throws Exception{
		String result = this.departmentService.getDeptMa();
		return result;
	}
	
	/**
	 * 设置部门管理员
	 * @param maId 管理员id
	 * @param id 部门id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/setDeptManager")
	@ResponseBody
	public String setDeptManager(String maId,String id) throws Exception{
		String result = this.departmentService.setDeptManager(maId,id);
		return result;
	}
}
