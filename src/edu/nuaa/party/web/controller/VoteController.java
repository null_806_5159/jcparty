package edu.nuaa.party.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import edu.nuaa.party.web.service.VoteService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.QueryVote;
import edu.nuaa.party.web.vo.SaveVoteVo;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.VoteOptionVo;
import edu.nuaa.party.web.vo.VoteVo;
/**
 * 投票管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/voteController")
public class VoteController {

	@Autowired
	private VoteService voteService;
	private Gson gson = new Gson();
	
	//跳转到待我投票页面
	@RequestMapping("/toMyVote")
	public String toMyVote(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		
		List<VoteVo> voteList = this.voteService.getWaitVoteList(activeUser.getUserId(),activeUser.getDeptId());
		request.setAttribute("voteList", voteList);
		return "vote/myVote";
	}
	
	//跳转到投票页面
	@RequestMapping("/toVote")
	public String toVote(String id,HttpServletRequest request) throws Exception{
		
		List<VoteOptionVo> optionList = this.voteService.getVoteOptionList(id);
		request.setAttribute("optionList", optionList);
		request.setAttribute("voteId", id);
		request.setAttribute("type", "1");
		return "vote/vote";
	}
	
	/**
	 * 获取投票列表
	 * @param queryVote
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getVoteList")
	@ResponseBody
	public String getVoteList(QueryVote queryVote,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		queryVote.setUserid(activeUser.getUserId());
		List<VoteVo> voteList = this.voteService.getVoteList(queryVote);
		int total = this.voteService.getVoteListCount(queryVote);
		DataGridVo<VoteVo> datagrid = new DataGridVo<VoteVo>();
		datagrid.setTotal(total);
		datagrid.setRows(voteList);
		String jsonStr = SysConstant.NULL_DATAGRID_RESULT;
		if(voteList.size()> 0 && total >0){
			jsonStr = gson.toJson(datagrid);
		}
		return jsonStr;
	}
	
	//跳转到投票管理页面
	@RequestMapping("/toVoteManage")
	public String toVoteManage() throws Exception{
		
		return "vote/voteManage";
	}
	
	//跳转到编辑投票页面
	@RequestMapping("/toSaveVoteUI")
	public String toSaveVote() throws Exception{
		
		return "vote/saveVote";
	}
	
	//获取投票候选人列表
	@RequestMapping("/getVoteOption")
	@ResponseBody
	public String getVoteOption(HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		String result = this.voteService.getVoteOption(activeUser.getUserId());
		return result;
	}
	
	//添加候选人页面
	@RequestMapping("/toAddCandidateUI")
	public String toAddCandidateUI() throws Exception{
		return "vote/addCandidate";
	}
	
	//保存投票信息
	@RequestMapping("/saveVote")
	@ResponseBody
	public String saveVote(HttpServletRequest request,SaveVoteVo vote) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		vote.setCreator(activeUser.getUserId());
		String result = this.voteService.saveVote(vote);
		return result;
	}
	
	//投票的发布与失效
	@RequestMapping("/updateVoteState")
	@ResponseBody
	public String updateVoteState(String id,String type) throws Exception{
		String result = this.voteService.updateVoteState(id,type);
		return result;
	}
	
	//保存投票记录
	@RequestMapping("/saveVoteResult")
	@ResponseBody
	public String saveVoteResult(String chk_valueIds,String voteId,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		
		String result = this.voteService.saveVoteResult(chk_valueIds,voteId,activeUser.getUserId());
		return result;
	}
	
	//保存弃权投票记录
	@RequestMapping("/abstained")
	@ResponseBody
	public String abstained(String id,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		String result = this.voteService.abstained(id,activeUser.getUserId());
		return result;
	}

}
