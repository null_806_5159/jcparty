package edu.nuaa.party.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.RoleService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.QueryRole;
import edu.nuaa.party.web.vo.RoleInfo;
/**
 * 角色
 * @author 聂卫星
 *
 */
@Controller
@RequestMapping("/roleController")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	/**
	 * 获取表单中的角色下拉列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getRoleList")
	@ResponseBody
	public String getRoleList() throws Exception{
		String roleJson = roleService.getRoleList();
		return roleJson;
	}
	
	/**
	 * 跳转到角色列表页面
	 * @param queryRole
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toList")
	public String toList() throws Exception{
		return "role/roleList";
	}
	
	/**
	 * 获取角色列表
	 * @param queryRole
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getRoleDataGrid")
	@ResponseBody
	public String getRoleDataGrid(QueryRole queryRole) throws Exception{
		String roleJson = roleService.getRoleDataGrid(queryRole);
		return roleJson;
	}
	
	/**
	 * 保存角色信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveRole")
	@ResponseBody
	public String saveRole(RoleInfo editRoleVo,HttpServletRequest request) throws Exception{
		//获取当前的用户
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		editRoleVo.setCreator(activeUser.getUserId());
		String resultJson = roleService.saveRole(editRoleVo);
		return resultJson;
	}
	/**
	 * 根据角色Id获取到角色信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getRoleInfoById")
	@ResponseBody
	public String getRoleInfoById(String id) throws Exception{
		String roleJson = roleService.getRoleInfoById(id);
		return roleJson;
	}
	/**
	 * 根据角色Id删除
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/deleteRoleById")
	@ResponseBody
	public String deleteRoleById(String id) throws Exception{
		String resultStr = roleService.deleteRoleById(id);
		return resultStr;
	}
	
	/**
	 * 角色授权
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/authToRole")
	@ResponseBody
	public String authToRole(String funcIds,String roleId) throws Exception{
		String resultStr = roleService.authToRole(funcIds,roleId);
		return resultStr;
	}
}
