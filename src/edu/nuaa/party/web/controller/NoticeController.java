package edu.nuaa.party.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.NoticeService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.QueryNotice;
import edu.nuaa.party.web.vo.SysNotice;

/**
 * 系统公告
 * @author Administrator
 *
 */
@RequestMapping("/noticeController")
@Controller
public class NoticeController {

	@Autowired
	private NoticeService noticeService;
	/**
	 * 跳转到通知公告首页
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toIndex")
	public String toIndex() throws Exception{
		return "notice/noticeList";
	}
	
	/**
	 * 获取系统通知的列表
	 * @param queryVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getNoticeList")
	@ResponseBody
	public String getNoticeList(QueryNotice queryVo) throws Exception{
		String noticeList = noticeService.getNoticeList(queryVo);
		return noticeList;
	}
	
	/**
	 * 保存通知公告
	 * @param notice
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveNotice")
	@ResponseBody
	public String saveNotice(SysNotice notice,HttpServletRequest request) throws Exception{
		//获取当前的用户
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		notice.setCreator(activeUser.getUserId());
		String resultStr = noticeService.saveNotice(notice);
		return resultStr;
	}
	
	/**
	 * 根据id查询相关的公告
	 * @param notice
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getNoticeById")
	public String getNoticeById(String id,HttpServletRequest request) throws Exception{
		SysNotice notice = noticeService.getNoticeById(id);
		request.setAttribute("notice", notice);
		return "notice/noticeDetail";
		
	}
	
	//跳转到通知保存页面
	@RequestMapping("/toSaveUI")
	public String toSaveUI(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id")==null?"":request.getParameter("id");
		if(id != null && id.length()>0){
			SysNotice notice = this.noticeService.getNoticeById(id);
			request.setAttribute("notice", notice);
		}
		return "notice/saveNotice";
	}
	
	//跳转到通知审核页面
	@RequestMapping("/toCheckUI")
	public String toCheckUI(String id,HttpServletRequest request) throws Exception{
		SysNotice notice = this.noticeService.getNoticeById(id);
		request.setAttribute("notice", notice);
		return "notice/reviewNotice";
	}
	
	//通知审核发布
	@RequestMapping("/checkNotice")
	@ResponseBody
	public String checkNotice(SysNotice notice,HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		notice.setReviewer(activeUser.getUserId());
		String result = this.noticeService.checkNotice(notice);
		return result;
	}
	
	//通知审核发布
	@RequestMapping("/downNotice")
	@ResponseBody
	public String downNotice(String id) throws Exception{
		String result = this.noticeService.downNoticeById(id);
		return result;
	}
}
