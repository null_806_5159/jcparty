package edu.nuaa.party.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.ActivityService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.ActivityVo;
import edu.nuaa.party.web.vo.QueryActivity;
import edu.nuaa.party.weixin.service.AccessTokenService;
import edu.nuaa.party.weixin.service.QrcodeService;
import edu.nuaa.party.weixin.vo.QrcodeVo;

/**
 * 活动管理
 * @author neil
 *
 */
@Controller
@RequestMapping("/activityController")
public class ActivityController {

	@Autowired
	private ActivityService activityService;
	@Autowired
	private QrcodeService qrcodeService;
	@Autowired
	private AccessTokenService accessTokenService;
	
	//跳转到活动管理页面
	@RequestMapping("/toIndex")
	public String toIndex() throws Exception{
		
		return "activity/activityList";
	}
	
	//获取活动列表
	@RequestMapping("/getActivityList")
	@ResponseBody
	public String getActivityList(QueryActivity query) throws Exception{
		String activityList = this.activityService.getActivityList(query);
		return activityList;
	}
	
	//活动上线
	@RequestMapping("/upActivity")
	@ResponseBody
	public String upActivity(String id) throws Exception{
		String token = this.accessTokenService.getAccessToken();
		String result = "";
		QrcodeVo qrcode = this.qrcodeService.createQrcode(id,token);
		if(qrcode != null){
			result = this.activityService.upActivity(id,qrcode);
		}
		return result;
	}
	
	//显示二维码
	@RequestMapping("/toQrcode")
	public String toQrcode(String ticket,HttpServletRequest request) throws Exception{
		request.setAttribute("ticket", ticket);
		return "activity/qrcode";
	}
	
	//获取活动列表
	@RequestMapping("/saveActivity")
	@ResponseBody
	public String saveActivity(ActivityVo activity,HttpServletRequest request) throws Exception{
		String id = activity.getId();
		String result = "";
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		if(id != null && id.length()>0){//用户的id存在，为修改操作
			activity.setUpdator(activeUser.getUserId());
			result = this.activityService.editActivity(activity);
		}else{
			activity.setCreator(activeUser.getUserId());
			result = this.activityService.addActivity(activity);
		}
		return result;
	}
	
	//跳转到保存活动页面
	@RequestMapping("/toSaveUI")
	public String toSaveUI(HttpServletRequest request) throws Exception{
		String id = request.getParameter("id")==null?"":request.getParameter("id");
		if(id != null && id.length()>0){
			ActivityVo activity = this.activityService.getActivityById(id);
			request.setAttribute("activity", activity);
		}
		return "activity/saveActivity";
	}
	
	//跳转到保存活动页面
	@RequestMapping("/downActivity")
	@ResponseBody
	public String downActivity(String id) throws Exception{
		String result = this.activityService.downActivity(id);
		return result;
	}
}
