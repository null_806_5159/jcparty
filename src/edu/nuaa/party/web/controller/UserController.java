package edu.nuaa.party.web.controller;

import java.io.File;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.nuaa.party.web.service.UserService;
import edu.nuaa.party.web.utils.ExportExcelUtil;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.QueryUser;
import edu.nuaa.party.web.vo.SysUser;
import edu.nuaa.party.web.vo.UserDetailVo;
import edu.nuaa.party.web.vo.UserInfo;
/**
 * 用户管理
 * @author 聂卫星
 *
 */
@Controller
@RequestMapping("/userController")
public class UserController {

	@Autowired
	private UserService userService;
	/**
	 * 跳转到用户列表页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toList")
	public String toList() throws Exception{
		
		return "user/userList";
	}
	
	/**
	 * 获取用户列表
	 * @param queryUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getUserInfoList")
	@ResponseBody
	public String getUserInfoList(QueryUser queryUser) throws Exception{
		String usersJsonStr = userService.getUserInfoList(queryUser);
		return usersJsonStr;
	}
	
	/**
	 * 导出用户清单
	 * @param queryUser
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportUserList")
	@ResponseBody
	public String exportUserList(QueryUser queryUser,HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String filePath = request.getSession().getServletContext().getRealPath("/")
				+ "WEB-INF/template/3a6b1d2c-063a-11e7-a4b8-28d244e1fc55.xls".replace("\\", "/");
		OutputStream os = null;  
		Workbook wb = null;    //工作薄
		
		try {
			//模拟数据库取值
			List<UserInfo> userInfos = userService.getExportUserInfo(queryUser);
			//导出Excel文件数据
			ExportExcelUtil util = new ExportExcelUtil();
			File file =new File(filePath);
			String sheetName="Sheet1";  
			wb = util.writeNewExcel(file, sheetName,userInfos); 
			
			String fileName="用户清单.xls";
		    response.setContentType("application/vnd.ms-excel");
		    response.setHeader("Content-disposition", "attachment;filename="+ URLEncoder.encode(fileName, "utf-8"));
		    os = response.getOutputStream();
			wb.write(os);  
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			os.flush();
			os.close();
			//wb.close();
		} 
		return null;
	}
	
	/**
	 * 保存用户
	 * @param sysUser 用户信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveUser")
	@ResponseBody
	public String saveUser(SysUser sysUser,HttpServletRequest request) throws Exception{
		String id = sysUser.getId();
		HttpSession session = request.getSession();
		ActiveUser activeUser = (ActiveUser) session.getAttribute("activeUser");
		if(id != null && id .length()>0){
			sysUser.setUpdator(activeUser.getUserId());//这里获取到当前的用户Id
		}else{
			sysUser.setCreator(activeUser.getUserId());//这里获取到当前的用户id
		}
		String resultStr = userService.saveUser(sysUser);
		return resultStr;
	}
	
	/**
	 * 根据用户的id删除当前用户
	 * @param id 删除用户的iD
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/deleteUserByPK")
	@ResponseBody
	public String deleteUserByPK(String id) throws Exception{
		String resultStr = userService.deleteUserByPK(id);
		return resultStr;
	}
	
	/**
	 * 初始化密码
	 * @param id 用户Id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/initPasswd")
	@ResponseBody
	public String initPasswd(String id) throws Exception{
		String resultStr = userService.initPasswd(id);
		return resultStr;
	}
	
	/**
	 * 根据Id获取用户的信息
	 * @param id 用户Id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getUserById")
	@ResponseBody
	public String getUserById(String id) throws Exception{
		String resultStr = userService.getUserById(id);
		return resultStr;
	}
	
	/**
	 * 跳转到用户详细信息页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toDetails")
	public String toDetails(String id,HttpServletRequest request) throws Exception{
		UserDetailVo userDetailVo = this.userService.getUserDetailsByUserId(id);
		request.setAttribute("userDetailVo", userDetailVo);
		return "user/userDetails";
	}
	
	//获取辅导员列表
	@RequestMapping("/getCounselorList")
	@ResponseBody
	public String getCounselorList() throws Exception{
		String result = this.userService.getCounselorList();
		return result;
	}
	
	//修改密码
	@RequestMapping("/editPasswd")
	@ResponseBody
	public String editPasswd(@RequestParam(value = "usercode") String usercode,@RequestParam(value = "newpassword") String newpassword) throws Exception{
		String result = this.userService.editPasswd(usercode,newpassword);
		return result;
	}
	
	//跳转到修改密码页面
	@RequestMapping("/toEditPasswd")
	public String toEditPasswd() throws Exception{
		return "user/editPasswd";
	}
}
