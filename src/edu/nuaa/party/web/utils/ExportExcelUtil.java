package edu.nuaa.party.web.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import edu.nuaa.party.web.vo.UserInfo;

public class ExportExcelUtil {  
	
	public  Workbook writeNewExcel(File file,String sheetName,List<UserInfo> lis) throws Exception{  
	    Workbook wb = null;  
	    Row row = null;   
	    Cell cell = null;  
	      
	    FileInputStream fis = new FileInputStream(file);  
	    wb = new ImportExcelUtil().getWorkbook(fis, file.getName());    //获取工作薄  
	    Sheet sheet = wb.getSheet(sheetName);  
	      
	    //循环插入数据  
	    int lastRow = sheet.getLastRowNum()+1;    //插入数据的数据ROW  
	    CellStyle cs = setSimpleCellStyle(wb);    //Excel单元格样式  
	    for (int i =0; i < lis.size(); i++) {  
	        row = sheet.createRow(lastRow+i); //创建新的ROW，用于数据插入  
	          
	        //按项目实际需求，在该处将对象数据插入到Excel中  
	        UserInfo vo  = lis.get(i);  
	        if(null==vo){  
	            break;  
	        }  
	        //Cell赋值开始  
	        cell = row.createCell(0);  
	        cell.setCellValue(vo.getUsername());  
	        cell.setCellStyle(cs);  
	          
	        cell = row.createCell(1);  
	        cell.setCellValue(vo.getUsercode());  
	        cell.setCellStyle(cs); 
	        
	        cell = row.createCell(2);  
	        cell.setCellValue(vo.getMajor());  
	        cell.setCellStyle(cs);  
	        
	        cell = row.createCell(3);  
	        cell.setCellValue(vo.getClassNm());  
	        cell.setCellStyle(cs); 
	        
	        cell = row.createCell(4);  
	        cell.setCellValue(vo.getRoleName());  
	        cell.setCellStyle(cs);  
	    }  
	    return wb;  
	}  
	  
	/** 
	 * 描述：设置简单的Cell样式 
	 * @return 
	 */  
	public  CellStyle setSimpleCellStyle(Workbook wb){  
	    CellStyle cs = wb.createCellStyle();  
	      
	    cs.setBorderBottom(CellStyle.BORDER_THIN); //下边框  
	    cs.setBorderLeft(CellStyle.BORDER_THIN);//左边框  
	    cs.setBorderTop(CellStyle.BORDER_THIN);//上边框  
	    cs.setBorderRight(CellStyle.BORDER_THIN);//右边框  
	
	    cs.setAlignment(CellStyle.ALIGN_CENTER); // 居中  
	      
	    return cs;  
	} 
}

