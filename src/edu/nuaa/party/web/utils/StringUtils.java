package edu.nuaa.party.web.utils;

import java.util.UUID;

/**
 * 常用的字符串工具类
 * @author Administrator
 *
 */
public class StringUtils {

	/**
	 * 生成UUID作为常用的主键Id
	 * @return
	 */
	public static String getPKByUUID(){
		String uuidStr = UUID.randomUUID().toString();
		return uuidStr;
	}
}
