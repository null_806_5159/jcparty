package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.BaseRole;
import edu.nuaa.party.web.vo.PerminssionRoleVo;
import edu.nuaa.party.web.vo.QueryRole;
import edu.nuaa.party.web.vo.RoleInfo;

public interface RoleMapper {

	List<BaseRole> getRoleList() throws Exception;

	/*获取角色列表*/
	List<RoleInfo> getDataList(QueryRole queryRole) throws Exception;
	int getDataCount(QueryRole queryRole) throws Exception;

	RoleInfo getRoleInfoById(String id) throws Exception;

	int editRole(RoleInfo editRoleVo) throws Exception;

	int saveRole(RoleInfo editRoleVo) throws Exception;

	int deleteRoleById(String id) throws Exception;

	int deleteFuncRoleByRoleId(String roleId) throws Exception;

	int saveFuncRole(List<PerminssionRoleVo> perminssionRoleVos) throws Exception;

}
