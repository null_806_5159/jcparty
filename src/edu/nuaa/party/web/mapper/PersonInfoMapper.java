package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.BaseInfoShowVo;
import edu.nuaa.party.web.vo.BaseInfoVo;
import edu.nuaa.party.web.vo.HomeNumVo;
import edu.nuaa.party.web.vo.PartyInfo;
import edu.nuaa.party.web.vo.PartyInfoShowVo;
import edu.nuaa.party.web.vo.RewardInfo;

public interface PersonInfoMapper {

	int editBaseInfo(BaseInfoVo baseInfo) throws Exception;

	int addBaseInfo(BaseInfoVo baseInfo) throws Exception;

	BaseInfoShowVo getUserBaseInfoByUserId(String userId) throws Exception;

	int editPartyInfo(PartyInfo partyInfo) throws Exception;

	int addPartyInfo(PartyInfo partyInfo) throws Exception;

	PartyInfoShowVo getPartyInfoByUserId(String userId) throws Exception;

	int editReward(RewardInfo rewardInfo) throws Exception;

	int addReward(RewardInfo rewardInfo) throws Exception;

	int deleteReward(String id) throws Exception;

	RewardInfo getRewardInfoById(String id) throws Exception;

	List<RewardInfo> getRewardListByUserId(String userId) throws Exception;

	List<HomeNumVo> getHomeNumListByUserId(String userId) throws Exception;

	HomeNumVo getHomeNumInfoById(String id) throws Exception;

	int editHomeNumInfo(HomeNumVo homeNum) throws Exception;

	int addHomeNumInfo(HomeNumVo homeNum) throws Exception;

	int deleteHomeNum(String id) throws Exception;

}
