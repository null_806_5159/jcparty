package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.QueryNotice;
import edu.nuaa.party.web.vo.SysNotice;

public interface NoticeMapper {

	List<SysNotice> getNoticeRows(QueryNotice queryVo) throws Exception;
	int getNoticeTotal(QueryNotice queryVo) throws Exception;
	
	/*修改公告*/
	int editNoticeById(SysNotice notice) throws Exception;
	
	/*新增公告*/
	int addNotice(SysNotice notice) throws Exception;
	
	/*根据Id获取公告信息*/
	SysNotice getNoticeById(String id) throws Exception;
	
	int checkNotice(SysNotice notice) throws Exception;
	
	int downNoticeById(String id) throws Exception;

}
