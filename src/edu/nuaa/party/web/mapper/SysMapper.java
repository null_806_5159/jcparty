package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.MenuVo;

public interface SysMapper {

	List<BaseComboboxVo> getProvince() throws Exception;

	List<BaseComboboxVo> getCity(String pcode) throws Exception;

	List<BaseComboboxVo> getCounty(String pcode) throws Exception;

	List<MenuVo> getMenuList(String usercode, String func_lever) throws Exception;

}
