package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.BaseMajorVo;
import edu.nuaa.party.web.vo.DeptmentVo;

public interface DeptMapper {

	List<BaseComboboxVo> getDeptList() throws Exception;

	List<BaseMajorVo> getDeptInfoList() throws Exception;

	int deleteDept(String id) throws Exception;

	List<BaseComboboxVo> getDeptComboBox() throws Exception;

	List<BaseComboboxVo> getDeptByLever(String lever) throws Exception;

	int editDept(DeptmentVo dept) throws Exception;

	int addDept(DeptmentVo dept) throws Exception;

	DeptmentVo getDeptById(String id) throws Exception;

	List<BaseComboboxVo> getDeptMa() throws Exception;

	int setDeptManager(String maId, String id) throws Exception;

}
