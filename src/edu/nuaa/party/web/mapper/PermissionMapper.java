package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.PermissionVo;

public interface PermissionMapper {

	List<PermissionVo> getPermissionList() throws Exception;

	List<BaseComboboxVo> getPermissionByLever(String lever) throws Exception;

	int addPermission(PermissionVo permission) throws Exception;

	int editPermission(PermissionVo permission) throws Exception;

	PermissionVo getPermissionById(String id) throws Exception;

	int deletePermission(String id) throws Exception;

	List<String> getRolePeermission(String roleId) throws Exception;

}
