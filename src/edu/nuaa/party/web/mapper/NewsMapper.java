package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.IndexNewsVo;
import edu.nuaa.party.web.vo.NewsInfo;
import edu.nuaa.party.web.vo.NewsTypeVo;
import edu.nuaa.party.web.vo.NewsVo;
import edu.nuaa.party.web.vo.QueryNews;

public interface NewsMapper {

	List<BaseComboboxVo> getNewsType() throws Exception;

	int addType(NewsTypeVo newsType) throws Exception;

	int addNews(NewsVo vo) throws Exception;

	//获取文章列表和数量
	List<NewsInfo> getNewsList(QueryNews queryNews) throws Exception;
	int getNewsCount(QueryNews queryNews) throws Exception;

	NewsInfo getNewsInfo(String id) throws Exception;

	List<IndexNewsVo> getIndexNewsList(String newType) throws Exception;

	List<NewsTypeVo> getNewsTypeList() throws Exception;

	int updateNewsState(String id, String type) throws Exception;

	

	
}
