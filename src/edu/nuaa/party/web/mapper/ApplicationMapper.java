package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.ApplyInfo;
import edu.nuaa.party.web.vo.ApproveInfo;
import edu.nuaa.party.web.vo.QueryApply;

public interface ApplicationMapper {

	//获取申请的列表
	List<ApplyInfo> getApplyList(QueryApply applyCondition) throws Exception;
	int getApplyCount(QueryApply applyCondition) throws Exception;
	
	int saveApply(ApplyInfo apply) throws Exception;
	
	List<ApplyInfo> getApproveList(String acceptor) throws Exception;
	
	ApproveInfo getApplyDetailById(String id) throws Exception;
	
	int saveApprove(String id, String state, String result) throws Exception;

}
