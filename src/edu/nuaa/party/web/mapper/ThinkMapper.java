package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.QueryThink;
import edu.nuaa.party.web.vo.ThoughtreportVo;

public interface ThinkMapper {

	int saveThink(ThoughtreportVo think) throws Exception;

	List<ThoughtreportVo> getThinkUploadList(QueryThink queryThink) throws Exception;
	int getThinkUploadListCount(QueryThink queryThink) throws Exception;

	ThoughtreportVo getThinkById(String id) throws Exception;

}
