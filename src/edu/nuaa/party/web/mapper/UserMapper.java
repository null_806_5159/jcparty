package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.QueryUser;
import edu.nuaa.party.web.vo.SysUser;
import edu.nuaa.party.web.vo.UserInfo;

public interface UserMapper {

	List<UserInfo> getUserInfoList(QueryUser queryUser) throws Exception;
	int getUserInfoCount(QueryUser queryUser) throws Exception;
	
	/*导出用户清单*/
	List<UserInfo> getExportUserInfo(QueryUser queryUser) throws Exception;
	
	/*保存用户*/
	int addUser(SysUser sysUser) throws Exception;
	
	/*逻辑删除用户*/
	int deleteUserByPK(String id) throws Exception;
	
	/*初始化密码*/
	int initPasswd(String id) throws Exception;
	
	/*根据用户id获取用户信息*/
	SysUser getUserById(String id) throws Exception;
	
	/*更新用户信息*/
	int updateUser(SysUser sysUser) throws Exception;
	
	//根据登录名、密码查询用户信息
	SysUser checkUser(String usercode,String password) throws Exception;
	
	
	List<BaseComboboxVo> getCounselorList() throws Exception;
	
	int editPasswd(String usercode, String newpassword) throws Exception;
	
	String getRoleNmByUserId(String id) throws Exception;
}
