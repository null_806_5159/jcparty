package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.QueryVote;
import edu.nuaa.party.web.vo.SaveVoteVo;
import edu.nuaa.party.web.vo.VoteOptionVo;
import edu.nuaa.party.web.vo.VoteVo;

public interface VoteMapper {

	//获取投票列表
	List<VoteVo> getVoteList(QueryVote queryVote) throws Exception;
	int getVoteListCount(QueryVote queryVote) throws Exception;
	
	List<BaseComboboxVo> getVoteOption(String userId) throws Exception;
	
	//添加投票信息
	int addVoteOption(List<VoteOptionVo> voteOptionList) throws Exception;
	int addVote(SaveVoteVo vote) throws Exception;
	
	int updateVoteState(String id, String type) throws Exception;
	
	
	List<VoteVo> getWaitVoteList(String userId,String deptId) throws Exception;
	
	List<VoteOptionVo> getVoteOptionList(String id) throws Exception;
	
	int addVoteUser(String voteId, String userId) throws Exception;
	
	int addVoteCount(String chk_valueIds) throws Exception;
	
	int addAbstainedVote(String id, String userId) throws Exception;

}
