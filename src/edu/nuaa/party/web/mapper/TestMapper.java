package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.QueryTest;
import edu.nuaa.party.web.vo.TestQuestionVo;
import edu.nuaa.party.web.vo.TestVo;

public interface TestMapper {

	//获取试卷列表
	List<TestVo> getTestList(QueryTest queryTest) throws Exception;
	int getTestListCount(QueryTest queryTest) throws Exception;
	
	
	int updateTestState(String id, String type) throws Exception;
	
	List<TestVo> getWaitTestList(String deptId,String userId) throws Exception;
	
	List<TestQuestionVo> getQuestionListByTestId(String id) throws Exception;
	
	TestVo getTestById(String id) throws Exception;
	
	int saveScore(String score, String testId, String userId) throws Exception;

	
}
