package edu.nuaa.party.web.mapper;

import java.util.List;

import edu.nuaa.party.web.vo.ActivityInfo;
import edu.nuaa.party.web.vo.ActivityVo;
import edu.nuaa.party.web.vo.QueryActivity;

public interface ActivityMapper {

	//获取活动列表
	List<ActivityInfo> getActivityList(QueryActivity query) throws Exception;
	int getActivityCount(QueryActivity query) throws Exception;
	
	int upActivity(String id, String ticket, String url) throws Exception;
	
	int addActivity(ActivityVo activity) throws Exception;
	
	int editActivity(ActivityVo activity) throws Exception;
	
	ActivityVo getActivityById(String id) throws Exception;
	
	int downActivity(String id) throws Exception;
	
}
