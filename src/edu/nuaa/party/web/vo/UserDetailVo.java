package edu.nuaa.party.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 查看用户详细信息
 * @author Administrator
 *
 */
public class UserDetailVo {

	private SysUser sysUser;//系统用户信息
	private List<RewardInfo> rewardList = new ArrayList<RewardInfo>(); //获奖情况
	private List<HomeNumVo> homeNumList = new ArrayList<HomeNumVo>();//家庭成员
	private BaseInfoShowVo baseInfoShowVo;//基本信息
	private String roleNm;//角色名称
	private PartyInfoShowVo partyInfo;//党内关系
	public SysUser getSysUser() {
		return sysUser;
	}
	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}
	public List<RewardInfo> getRewardList() {
		return rewardList;
	}
	public void setRewardList(List<RewardInfo> rewardList) {
		this.rewardList = rewardList;
	}
	public List<HomeNumVo> getHomeNumList() {
		return homeNumList;
	}
	public void setHomeNumList(List<HomeNumVo> homeNumList) {
		this.homeNumList = homeNumList;
	}
	public BaseInfoShowVo getBaseInfoShowVo() {
		return baseInfoShowVo;
	}
	public void setBaseInfoShowVo(BaseInfoShowVo baseInfoShowVo) {
		this.baseInfoShowVo = baseInfoShowVo;
	}
	public PartyInfoShowVo getPartyInfo() {
		return partyInfo;
	}
	public void setPartyInfo(PartyInfoShowVo partyInfo) {
		this.partyInfo = partyInfo;
	}
	public String getRoleNm() {
		return roleNm;
	}
	public void setRoleNm(String roleNm) {
		this.roleNm = roleNm;
	}
}
