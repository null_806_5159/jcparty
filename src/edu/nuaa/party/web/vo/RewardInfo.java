package edu.nuaa.party.web.vo;
/**
 * 个人奖惩
 * @author Administrator
 *
 */
public class RewardInfo {

	private String id;//主键
	private String reward_time_start;//获奖开始时间
	private String reward_time_end;//获奖结束时间
	private String reward;//奖项
	private String reward_org;//授奖单位
	private String userid;//用户id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReward_time_start() {
		return reward_time_start;
	}
	public void setReward_time_start(String reward_time_start) {
		this.reward_time_start = reward_time_start;
	}
	public String getReward_time_end() {
		return reward_time_end;
	}
	public void setReward_time_end(String reward_time_end) {
		this.reward_time_end = reward_time_end;
	}
	public String getReward() {
		return reward;
	}
	public void setReward(String reward) {
		this.reward = reward;
	}
	public String getReward_org() {
		return reward_org;
	}
	public void setReward_org(String reward_org) {
		this.reward_org = reward_org;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
}
