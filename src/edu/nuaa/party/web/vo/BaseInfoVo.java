package edu.nuaa.party.web.vo;
/**
 * 用户的基本信息
 * @author Administrator
 *
 */
public class BaseInfoVo {

	private String id;//主键
	private String path;//头像路径
	private String id_card;//身份证号
	private String phoneNum;//手机号
	private String qq;//QQ号
	private String weixin;//微信号
	private String sex;//性别：1男2女
	private String national;//民族
	private String native_place;//籍贯
	private String classNum;//班级
	private String class_duty;//班级职务
	private String diploma;//学历:1为大专2为本科3硕士研究生4博士研究生
	private String degrees;//学位；1为学士2为硕士3为博士
	private String university;//毕业院校
	private String domicile_1;//户籍所在地:省编号
	private String current_place_1;//现居住地:省编号
	private String job_time;//工作时间
	private String job_org;//工作单位
	private String job_duty;//工作职务
	private String userid;//用户id
	private String domicile_2;//户籍所在地:市编号
	private String domicile_3;//户籍所在地:区县编号
	private String current_place_2;//现居住地:市编号
	private String current_place_3;//现居住地:区县编号
	private String current_place_4;//现居住地:剩余详细地址
	private String marital_status;//婚姻状态：1单身2已婚3离异
	private String birthday;//出生日期
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getId_card() {
		return id_card;
	}
	public void setId_card(String id_card) {
		this.id_card = id_card;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getWeixin() {
		return weixin;
	}
	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getNational() {
		return national;
	}
	public void setNational(String national) {
		this.national = national;
	}
	public String getNative_place() {
		return native_place;
	}
	public void setNative_place(String native_place) {
		this.native_place = native_place;
	}
	public String getClassNum() {
		return classNum;
	}
	public void setClassNum(String classNum) {
		this.classNum = classNum;
	}
	public String getClass_duty() {
		return class_duty;
	}
	public void setClass_duty(String class_duty) {
		this.class_duty = class_duty;
	}
	public String getDiploma() {
		return diploma;
	}
	public void setDiploma(String diploma) {
		this.diploma = diploma;
	}
	public String getDegrees() {
		return degrees;
	}
	public void setDegrees(String degrees) {
		this.degrees = degrees;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	public String getDomicile_1() {
		return domicile_1;
	}
	public void setDomicile_1(String domicile_1) {
		this.domicile_1 = domicile_1;
	}
	public String getCurrent_place_1() {
		return current_place_1;
	}
	public void setCurrent_place_1(String current_place_1) {
		this.current_place_1 = current_place_1;
	}
	public String getJob_time() {
		return job_time;
	}
	public void setJob_time(String job_time) {
		this.job_time = job_time;
	}
	public String getJob_org() {
		return job_org;
	}
	public void setJob_org(String job_org) {
		this.job_org = job_org;
	}
	public String getJob_duty() {
		return job_duty;
	}
	public void setJob_duty(String job_duty) {
		this.job_duty = job_duty;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getDomicile_2() {
		return domicile_2;
	}
	public void setDomicile_2(String domicile_2) {
		this.domicile_2 = domicile_2;
	}
	public String getDomicile_3() {
		return domicile_3;
	}
	public void setDomicile_3(String domicile_3) {
		this.domicile_3 = domicile_3;
	}
	public String getCurrent_place_2() {
		return current_place_2;
	}
	public void setCurrent_place_2(String current_place_2) {
		this.current_place_2 = current_place_2;
	}
	public String getCurrent_place_3() {
		return current_place_3;
	}
	public void setCurrent_place_3(String current_place_3) {
		this.current_place_3 = current_place_3;
	}
	public String getCurrent_place_4() {
		return current_place_4;
	}
	public void setCurrent_place_4(String current_place_4) {
		this.current_place_4 = current_place_4;
	}
	public String getMarital_status() {
		return marital_status;
	}
	public void setMarital_status(String marital_status) {
		this.marital_status = marital_status;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
}
