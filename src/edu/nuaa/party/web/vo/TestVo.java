package edu.nuaa.party.web.vo;

/**
 * 试卷信息
 * @author Administrator
 *
 */
public class TestVo {

	private String id;//主键
	private String createtime;//创建时间
	private String creator;//创建人
	private String releasetime;//发布时间
	private String dept;//适用系部
	private String deptNm;//院系名称
	private String test_desc;//试卷描述
	private String state;//状态：1待发布2已发布3已结束
	private String invalidtime;//失效时间
	private String title;//试卷标题
	private String questionCount;//题数
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getReleasetime() {
		return releasetime;
	}
	public void setReleasetime(String releasetime) {
		this.releasetime = releasetime;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getTest_desc() {
		return test_desc;
	}
	public void setTest_desc(String test_desc) {
		this.test_desc = test_desc;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getInvalidtime() {
		return invalidtime;
	}
	public void setInvalidtime(String invalidtime) {
		this.invalidtime = invalidtime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDeptNm() {
		return deptNm;
	}
	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}
	public String getQuestionCount() {
		return questionCount;
	}
	public void setQuestionCount(String questionCount) {
		this.questionCount = questionCount;
	}
}
