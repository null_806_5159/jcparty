package edu.nuaa.party.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限封装类
 * @author Administrator
 *
 */
public class PermissionVo {

	private String id;//主键
	private String func_nm;//权限名称
	private String func_desc;//权限描述
	private String parentId;//父级Id
	private String createtime;//创建时间
	private String creator;//创建人
	private String state;//状态：1有效，0失效
	private String func_lever;//1为一级菜单，2为二级菜单，3为页面url
	private String func_order;//左侧菜单的排列顺序
	private String url;//访问路径
	private String icon;//图标
	private String creatornm;//创建人名称
	private String updator;//更新人
	private String updatetime;//更新时间
	List<PermissionVo> children = new ArrayList<PermissionVo>();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFunc_nm() {
		return func_nm;
	}
	public void setFunc_nm(String func_nm) {
		this.func_nm = func_nm;
	}
	public String getFunc_desc() {
		return func_desc;
	}
	public void setFunc_desc(String func_desc) {
		this.func_desc = func_desc;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getFunc_lever() {
		return func_lever;
	}
	public void setFunc_lever(String func_lever) {
		this.func_lever = func_lever;
	}
	public String getFunc_order() {
		return func_order;
	}
	public void setFunc_order(String func_order) {
		this.func_order = func_order;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public List<PermissionVo> getChildren() {
		return children;
	}
	public void setChildren(List<PermissionVo> children) {
		this.children = children;
	}
	public String getCreatornm() {
		return creatornm;
	}
	public void setCreatornm(String creatornm) {
		this.creatornm = creatornm;
	}
	public String getUpdator() {
		return updator;
	}
	public void setUpdator(String updator) {
		this.updator = updator;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
}
