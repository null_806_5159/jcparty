package edu.nuaa.party.web.vo;
/**
 * 个人基本信息的信息展示
 * @author Administrator
 *
 */
public class BaseInfoShowVo extends BaseInfoVo{

	private String username;
	private String usercode;
	private String province_hj;//户籍所在省
	private String city_hj;//户籍所在城市
	private String county_hj;//户籍区县
	private String province_zj;//居住地所在省
	private String city_zj;//居住地所在城市
	private String county_zj;//居住地区县
	private String dept;//所在系部
	private String major;//所在专业
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsercode() {
		return usercode;
	}
	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}
	public String getProvince_hj() {
		return province_hj;
	}
	public void setProvince_hj(String province_hj) {
		this.province_hj = province_hj;
	}
	public String getCity_hj() {
		return city_hj;
	}
	public void setCity_hj(String city_hj) {
		this.city_hj = city_hj;
	}
	public String getCounty_hj() {
		return county_hj;
	}
	public void setCounty_hj(String county_hj) {
		this.county_hj = county_hj;
	}
	public String getProvince_zj() {
		return province_zj;
	}
	public void setProvince_zj(String province_zj) {
		this.province_zj = province_zj;
	}
	public String getCity_zj() {
		return city_zj;
	}
	public void setCity_zj(String city_zj) {
		this.city_zj = city_zj;
	}
	public String getCounty_zj() {
		return county_zj;
	}
	public void setCounty_zj(String county_zj) {
		this.county_zj = county_zj;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
}
