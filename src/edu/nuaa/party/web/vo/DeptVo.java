package edu.nuaa.party.web.vo;
/**
 * treeGrid的基础类
 * @author neil
 *
 */
public class DeptVo {

	private String id;
	private String name;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
