package edu.nuaa.party.web.vo;
/**
 * 我的申请
 * @author nieweixing
 *
 */
public class ApplyInfo {

	private String id;//主键id
	private String title;//申请标题
	private String applydate;//发出申请时间
	private String acceptorId;//受理人id
	private String acceptorNm;//受理人id
	private String result;//审批描述
	private String state;//状态：1已提交2受理中3通过4未通过
	private String content;//申请内容
	private String applicant;//申请人
	private String applicantNm;//申请人
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApplydate() {
		return applydate;
	}
	public void setApplydate(String applydate) {
		this.applydate = applydate;
	}
	public String getAcceptorId() {
		return acceptorId;
	}
	public void setAcceptorId(String acceptorId) {
		this.acceptorId = acceptorId;
	}
	public String getAcceptorNm() {
		return acceptorNm;
	}
	public void setAcceptorNm(String acceptorNm) {
		this.acceptorNm = acceptorNm;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getApplicant() {
		return applicant;
	}
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getApplicantNm() {
		return applicantNm;
	}
	public void setApplicantNm(String applicantNm) {
		this.applicantNm = applicantNm;
	}
}
