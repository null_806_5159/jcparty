package edu.nuaa.party.web.vo;
/**
 * 用户信息查询
 * @author 聂卫星
 *
 */
public class QueryUser extends BaseQueryVo{

	private String username;//用户姓名
	private String majorId;//系部或者专业
	private String roleId;//角色
	private String stuNum;//学号
	private String state;//是否毕业：0为毕业1为未毕业
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getMajorId() {
		return majorId;
	}
	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getStuNum() {
		return stuNum;
	}
	public void setStuNum(String stuNum) {
		this.stuNum = stuNum;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}
