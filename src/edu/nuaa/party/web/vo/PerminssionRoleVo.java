package edu.nuaa.party.web.vo;

public class PerminssionRoleVo {

	private String roleId;
	private String funcId;

	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getFuncId() {
		return funcId;
	}
	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}
}
