package edu.nuaa.party.web.vo;
/**
 * 左侧菜单实体
 * @author Administrator
 *
 */
public class MenuVo {

	private String id;//菜单id
	private String pid;//父级id
	private String name;//名称
	private String icon;//图标
	private String url;//url
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
