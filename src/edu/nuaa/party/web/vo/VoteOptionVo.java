package edu.nuaa.party.web.vo;
/**
 * 投票候选人的信息
 * @author Administrator
 *
 */
public class VoteOptionVo {

	private String id;
	private String optionId;
	private String remark;
	private String voteId;
	
	private String path;//头像路径
	private String candition;//候选人名称
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOptionId() {
		return optionId;
	}
	public void setOptionId(String optionId) {
		this.optionId = optionId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getVoteId() {
		return voteId;
	}
	public void setVoteId(String voteId) {
		this.voteId = voteId;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getCandition() {
		return candition;
	}
	public void setCandition(String candition) {
		this.candition = candition;
	}
}
