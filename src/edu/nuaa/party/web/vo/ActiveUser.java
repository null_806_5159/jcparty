package edu.nuaa.party.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 存储认证通过后的用户信息
 * @author Administrator
 *
 */
public class ActiveUser {

	private String userId;
	private String usercode;
	private String username;
	private String password;
	private String userIP;
	private String deptId;
	private List<MenuVo> topMenuList = new ArrayList<MenuVo>();
	private List<MenuVo> menuList = new ArrayList<MenuVo>();
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUsercode() {
		return usercode;
	}
	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserIP() {
		return userIP;
	}
	public void setUserIP(String userIP) {
		this.userIP = userIP;
	}
	public List<MenuVo> getTopMenuList() {
		return topMenuList;
	}
	public void setTopMenuList(List<MenuVo> topMenuList) {
		this.topMenuList = topMenuList;
	}
	public List<MenuVo> getMenuList() {
		return menuList;
	}
	public void setMenuList(List<MenuVo> menuList) {
		this.menuList = menuList;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
}
