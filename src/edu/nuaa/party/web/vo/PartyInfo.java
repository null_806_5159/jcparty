package edu.nuaa.party.web.vo;
/**
 * 党内信息
 * @author Administrator
 *
 */
public class PartyInfo {

	private String id;//主键
	private String userid;//用户id
	private String party_time;//入党时间
	private String regular_time;//转正时间
	private String party_branch;//入党所在党支部
	private String regular_branch;//转正所在党支部
	private String party_org;//党组织所在单位
	private String branch;//所在党支部
	private String branch_time;//进入党支部时间
	private String party_duty;//党内职务
	private String referencess;//入党介绍人
	private String to_where;//分配去向
	private String accept_dept;//接受部门
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getParty_time() {
		return party_time;
	}
	public void setParty_time(String party_time) {
		this.party_time = party_time;
	}
	public String getRegular_time() {
		return regular_time;
	}
	public void setRegular_time(String regular_time) {
		this.regular_time = regular_time;
	}
	public String getParty_branch() {
		return party_branch;
	}
	public void setParty_branch(String party_branch) {
		this.party_branch = party_branch;
	}
	public String getRegular_branch() {
		return regular_branch;
	}
	public void setRegular_branch(String regular_branch) {
		this.regular_branch = regular_branch;
	}
	public String getParty_org() {
		return party_org;
	}
	public void setParty_org(String party_org) {
		this.party_org = party_org;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getBranch_time() {
		return branch_time;
	}
	public void setBranch_time(String branch_time) {
		this.branch_time = branch_time;
	}
	public String getParty_duty() {
		return party_duty;
	}
	public void setParty_duty(String party_duty) {
		this.party_duty = party_duty;
	}
	public String getReferencess() {
		return referencess;
	}
	public void setReferencess(String referencess) {
		this.referencess = referencess;
	}
	public String getTo_where() {
		return to_where;
	}
	public void setTo_where(String to_where) {
		this.to_where = to_where;
	}
	public String getAccept_dept() {
		return accept_dept;
	}
	public void setAccept_dept(String accept_dept) {
		this.accept_dept = accept_dept;
	}
}
