package edu.nuaa.party.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * easyUI的dataGrid分装类
 * @author 聂卫星
 *
 * @param <T> dataGrid 信息展示的实体分装类
 */
public class DataGridVo<T> {

	private int total;
	private List<T> rows = new ArrayList<T>();
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
}
