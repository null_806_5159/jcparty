package edu.nuaa.party.web.vo;
/**
 * 家庭主要成员信息
 * @author Administrator
 *
 */
public class HomeNumVo {

	private String id;//主键
	private String userid;//用户id
	private String relation;//称谓
	private String name;//名称
	private String age;//年龄
	private String political_status;//政治面貌：01中共党员02中共预备党员03共青团员04群众05其他
	private String job_org;//工作单位
	private String job_duty;//工作职务
	private String createtime;//创建时间
	private String updatetime;//L修改时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getPolitical_status() {
		return political_status;
	}
	public void setPolitical_status(String political_status) {
		this.political_status = political_status;
	}
	public String getJob_org() {
		return job_org;
	}
	public void setJob_org(String job_org) {
		this.job_org = job_org;
	}
	public String getJob_duty() {
		return job_duty;
	}
	public void setJob_duty(String job_duty) {
		this.job_duty = job_duty;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
}
