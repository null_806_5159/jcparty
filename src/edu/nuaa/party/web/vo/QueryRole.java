package edu.nuaa.party.web.vo;

public class QueryRole extends BaseQueryVo{

	private String roleNm;
	private String state;
	
	public String getRoleNm() {
		return roleNm;
	}
	public void setRoleNm(String roleNm) {
		this.roleNm = roleNm;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
}
