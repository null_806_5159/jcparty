package edu.nuaa.party.web.vo;
/**
 * 活动信息展示
 * @author Administrator
 *
 */
public class ActivityInfo {

	private String id;//主键
	private String name;//标题
	private String place;//地点
	private String createtime;//创建时间
	private String creatorNm;//创建人
	private String dept;//适用系部
	private String deptNm;//系部名称
	private String state;//状态
	private String ticket;//票据
	private String url;//二维码地址
	private String remark;//备注
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getCreatorNm() {
		return creatorNm;
	}
	public void setCreatorNm(String creatorNm) {
		this.creatorNm = creatorNm;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDeptNm() {
		return deptNm;
	}
	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}
}
