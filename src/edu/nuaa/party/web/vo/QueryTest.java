package edu.nuaa.party.web.vo;
/**
 * 试题查询
 * @author Administrator
 *
 */
public class QueryTest extends BaseQueryVo {

	private String title;
	private String state;
	private String userid;
	private String deptId;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
}
