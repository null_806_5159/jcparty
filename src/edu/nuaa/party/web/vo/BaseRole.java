package edu.nuaa.party.web.vo;
/**
 * 基础的角色类
 * @author 聂卫星
 *
 */
public class BaseRole {

	private String id;
	private String name;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
