package edu.nuaa.party.web.vo;
/**
 * 往期投票查询
 * @author Administrator
 *
 */
public class QueryVote extends BaseQueryVo {

	private String title;
	private String state;
	private String userid;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
}
