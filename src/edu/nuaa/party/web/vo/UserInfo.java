package edu.nuaa.party.web.vo;
/**
 * 用户信息列表
 * @author 聂卫星
 *
 */
public class UserInfo extends SysUser{

	private String roleName;
	private String classNm;
	private String major;
	private String dept;

	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getClassNm() {
		return classNm;
	}
	public void setClassNm(String classNm) {
		this.classNm = classNm;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
}
