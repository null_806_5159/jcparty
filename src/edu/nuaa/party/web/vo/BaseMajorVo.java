package edu.nuaa.party.web.vo;

import com.google.gson.annotations.SerializedName;

/**
 * datatree 三级vo 专业
 * @author Administrator
 *
 */
public class BaseMajorVo{
	private String id;//主键
	private String name;//姓名
	private String createtime;//创建时间
	private String managerNm;//负责人员
	private String pid;//父级Id
	private String lever;//部门等级
	private String deptNum;//编号
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getManagerNm() {
		return managerNm;
	}
	public void setManagerNm(String managerNm) {
		this.managerNm = managerNm;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getLever() {
		return lever;
	}
	public void setLever(String lever) {
		this.lever = lever;
	}
	public String getDeptNum() {
		return deptNum;
	}
	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}
}
