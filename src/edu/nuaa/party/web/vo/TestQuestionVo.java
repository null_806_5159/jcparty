package edu.nuaa.party.web.vo;
/**
 * 试题信息
 * @author Administrator
 *
 */
public class TestQuestionVo {

	private String id;//主键ID
	private String title;//题目
	private String answer;//正确答案
	private String question_1;//选项A
	private String question_2;//选项B
	private String question_3;//选项C
	private String question_4;//选项D
	private String createtime;//创建时间
	private String creator;//创建人
	private String test_id;//试题id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getQuestion_1() {
		return question_1;
	}
	public void setQuestion_1(String question_1) {
		this.question_1 = question_1;
	}
	public String getQuestion_2() {
		return question_2;
	}
	public void setQuestion_2(String question_2) {
		this.question_2 = question_2;
	}
	public String getQuestion_3() {
		return question_3;
	}
	public void setQuestion_3(String question_3) {
		this.question_3 = question_3;
	}
	public String getQuestion_4() {
		return question_4;
	}
	public void setQuestion_4(String question_4) {
		this.question_4 = question_4;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getTest_id() {
		return test_id;
	}
	public void setTest_id(String test_id) {
		this.test_id = test_id;
	}
}
