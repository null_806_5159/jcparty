package edu.nuaa.party.web.vo;
/**
 * 党内信息展示
 * @author Administrator
 *
 */
public class PartyInfoShowVo extends PartyInfo{

	private String username;
	private String usercode;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsercode() {
		return usercode;
	}
	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}
}
