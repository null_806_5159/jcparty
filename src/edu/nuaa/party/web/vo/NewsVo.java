package edu.nuaa.party.web.vo;
/**
 * 文章
 * @author Administrator
 *
 */
public class NewsVo {

	private String id;//主键
	private String title;//文章标题
	private String path;//插图路径
	private String type;//文章专题
	private String dept;//适用系部
	private String creator;//创建人
	private String createtime;//创建时间
	private String content;//文章内容
	private String state;//状态0失效1有效2保存待上线
	private String updator;//更新人
	private String updatetime;//更新时间
	private String reviewer;//审核人
	private String reviewetime;//审核时间
	private String review_result;//审核理由
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUpdator() {
		return updator;
	}
	public void setUpdator(String updator) {
		this.updator = updator;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	public String getReviewer() {
		return reviewer;
	}
	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}
	public String getReviewetime() {
		return reviewetime;
	}
	public void setReviewetime(String reviewetime) {
		this.reviewetime = reviewetime;
	}
	public String getReview_result() {
		return review_result;
	}
	public void setReview_result(String review_result) {
		this.review_result = review_result;
	}
}
