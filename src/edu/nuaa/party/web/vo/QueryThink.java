package edu.nuaa.party.web.vo;
/**
 * 思想汇报查询条件
 * @author Administrator
 *
 */
public class QueryThink extends BaseQueryVo {

	private String title;
	private String creator;
	private String startDate;
	private String endDate;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
}
