package edu.nuaa.party.web.vo;

import java.util.ArrayList;
import java.util.List;
/**
 * datatree 二级Vo  系部
 * @author neil
 *
 */
public class BaseDeptVo extends BaseMajorVo {
	
	private List<BaseMajorVo> children = new ArrayList<BaseMajorVo>();

	public List<BaseMajorVo> getChildren() {
		return children;
	}

	public void setChildren(List<BaseMajorVo> children) {
		this.children = children;
	}
}
