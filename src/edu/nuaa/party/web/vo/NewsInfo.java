package edu.nuaa.party.web.vo;

public class NewsInfo extends NewsVo {

	private String creatorNm;
	private String deptNm;
	private String reviewerNm;

	public String getCreatorNm() {
		return creatorNm;
	}

	public void setCreatorNm(String creatorNm) {
		this.creatorNm = creatorNm;
	}

	public String getDeptNm() {
		return deptNm;
	}

	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}

	public String getReviewerNm() {
		return reviewerNm;
	}

	public void setReviewerNm(String reviewerNm) {
		this.reviewerNm = reviewerNm;
	}
}
