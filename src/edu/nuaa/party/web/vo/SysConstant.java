package edu.nuaa.party.web.vo;
/**
 * 系统常亮
 * @author Administrator
 *
 */
public class SysConstant {

	//datagrid返回结果为0
	public static final String NULL_DATAGRID_RESULT = "{\"total\":0,\"rows\":[]}";
	
	//部门等级：1为系部 2为专业
	public static final int DEPT_DEPT_LEVER = 1;
	public static final int DEPT_MAJOR_LEVER = 2;
	
	public static final String OPERATE_RESULT_SUCCESS = "{\"result\":\"success\"}";
	public static final String OPERATE_RESULT_FAILE = "{\"result\":\"faile\"}";
	
	/*用户加密的token*/
	public static final String USER_TOKEN = "jcparty";
	
	//public static final String SERVER_UPLOAD_ADDR = "D:\\apache-tomcat-7.0.77\\upload\\";
	public static final String SERVER_UPLOAD_ADDR = "/home/jcdj/java/tomcat/apache-tomcat-7.0.76/upload";
}
