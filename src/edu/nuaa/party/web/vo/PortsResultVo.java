package edu.nuaa.party.web.vo;

import java.util.ArrayList;
import java.util.List;
/**
 * 接口返回实体类
 * @author Administrator
 *
 * @param <T> 数据信息类
 */
public class PortsResultVo<T> {

	private int error_code;//请求是否成功的标志
	private String reason;//该字段可有可不有
	private List<T> data = new ArrayList<T>();//返回数据
	public int getError_code() {
		return error_code;
	}
	public void setError_code(int error_code) {
		this.error_code = error_code;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
}
