package edu.nuaa.party.web.vo;
/**
 * 首页新闻信息展示
 * @author Administrator
 *
 */
public class IndexNewsVo {

	private String id;//主键
	private String title;//文章标题
	private String path;//插图路径
	private String type;//文章专题
	private String creator;//创建人
	private String createtime;//创建时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
}
