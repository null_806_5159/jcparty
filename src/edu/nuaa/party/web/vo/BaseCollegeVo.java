package edu.nuaa.party.web.vo;

import java.util.ArrayList;
import java.util.List;
/**
 * datatree 一级Vo  学院
 * @author neil
 *
 */
public class BaseCollegeVo extends BaseMajorVo {
	
	private List<BaseDeptVo> children = new ArrayList<BaseDeptVo>();

	public List<BaseDeptVo> getChildren() {
		return children;
	}

	public void setChildren(List<BaseDeptVo> children) {
		this.children = children;
	}
}
