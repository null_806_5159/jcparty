package edu.nuaa.party.web.vo;
/**
 * 思想汇报
 * @author Administrator
 *
 */
public class ThoughtreportVo {

	private String id;//id
	private String title;//标题
	private String path;//图片路径，多图片以;隔开
	private String creator;//创建人Id
	private String creatorNm;//创建人名称
	private String createtime;//创建时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getCreatorNm() {
		return creatorNm;
	}
	public void setCreatorNm(String creatorNm) {
		this.creatorNm = creatorNm;
	}
}
