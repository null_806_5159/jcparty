package edu.nuaa.party.web.vo;
/**
 * 保存投票信息
 * @author Administrator
 *
 */
public class VoteVo {

	private String id;
	private String title;//投票标题
	private String invalidtime;//有效时间
	private String vote_desc;
	private String dept;
	private String candidateIds;
	private String contents;
	private String createtime;
	private String creator;
	private String releasetime;
	private String state;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getInvalidtime() {
		return invalidtime;
	}
	public void setInvalidtime(String invalidtime) {
		this.invalidtime = invalidtime;
	}
	public String getVote_desc() {
		return vote_desc;
	}
	public void setVote_desc(String vote_desc) {
		this.vote_desc = vote_desc;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getCandidateIds() {
		return candidateIds;
	}
	public void setCandidateIds(String candidateIds) {
		this.candidateIds = candidateIds;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getReleasetime() {
		return releasetime;
	}
	public void setReleasetime(String releasetime) {
		this.releasetime = releasetime;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
}
