package edu.nuaa.party.web.vo;
/**
 * 公告通知实体类
 * @author Administrator
 *
 */
public class SysNotice {

	private String id;
	private String title;//标题
	private String content;//内容
	private String creator;//创建人
	private String creatorNm;//创建人名称
	private String createtime;//创建时间
	private String reviewer;//审核人
	private String reviewetime;//审核时间
	private String review_result;//审核结果
	private String state;//状态
	private String fit_dept;//适用系部专业
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getReviewer() {
		return reviewer;
	}
	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}
	public String getReviewetime() {
		return reviewetime;
	}
	public void setReviewetime(String reviewetime) {
		this.reviewetime = reviewetime;
	}
	public String getReview_result() {
		return review_result;
	}
	public void setReview_result(String review_result) {
		this.review_result = review_result;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getFit_dept() {
		return fit_dept;
	}
	public void setFit_dept(String fit_dept) {
		this.fit_dept = fit_dept;
	}
	public String getCreatorNm() {
		return creatorNm;
	}
	public void setCreatorNm(String creatorNm) {
		this.creatorNm = creatorNm;
	}
	
}
