package edu.nuaa.party.web.vo;
/**
 * 审批详情
 * @author Administrator
 *
 */
public class ApproveInfo extends ApplyInfo {

	private String roleNm;
	private String major;
	public String getRoleNm() {
		return roleNm;
	}
	public void setRoleNm(String roleNm) {
		this.roleNm = roleNm;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
}
