package edu.nuaa.party.web.vo;

public class BaseQueryVo {

	private int rows;
	private int page;
	private int offset;
	public int getOffset() {
		offset = (this.page-1)*this.rows;
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
}
