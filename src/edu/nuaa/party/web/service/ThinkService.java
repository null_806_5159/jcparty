package edu.nuaa.party.web.service;

import edu.nuaa.party.web.vo.QueryThink;
import edu.nuaa.party.web.vo.ThoughtreportVo;

public interface ThinkService {

	String saveThink(ThoughtreportVo think) throws Exception;

	String getThinkUploadList(QueryThink queryThink) throws Exception;

	ThoughtreportVo getThinkById(String id) throws Exception;

}
