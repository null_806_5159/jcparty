package edu.nuaa.party.web.service;

import java.util.List;

import edu.nuaa.party.web.vo.QueryUser;
import edu.nuaa.party.web.vo.SysUser;
import edu.nuaa.party.web.vo.UserDetailVo;
import edu.nuaa.party.web.vo.UserInfo;

public interface UserService {

	String getUserInfoList(QueryUser queryUser) throws Exception;

	List<UserInfo> getExportUserInfo(QueryUser queryUser) throws Exception;

	String saveUser(SysUser sysUser) throws Exception;

	String deleteUserByPK(String id) throws Exception;

	String initPasswd(String id) throws Exception;

	String getUserById(String id) throws Exception;

	String getCounselorList() throws Exception;

	String editPasswd(String usercode, String newpassword) throws Exception;

	UserDetailVo getUserDetailsByUserId(String id) throws Exception;

}
