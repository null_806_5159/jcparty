package edu.nuaa.party.web.service;

import java.util.List;

import edu.nuaa.party.web.vo.BaseInfoShowVo;
import edu.nuaa.party.web.vo.BaseInfoVo;
import edu.nuaa.party.web.vo.HomeNumVo;
import edu.nuaa.party.web.vo.PartyInfo;
import edu.nuaa.party.web.vo.PartyInfoShowVo;
import edu.nuaa.party.web.vo.RewardInfo;

public interface PersonInfoService {

	String editBaseInfo(BaseInfoVo baseInfo) throws Exception;

	String addBaseInfo(BaseInfoVo baseInfo) throws Exception;

	BaseInfoShowVo getUserBaseInfoByUserId(String userId) throws Exception;

	String savePartyInfo(PartyInfo partyInfo) throws Exception;

	PartyInfoShowVo getPartyInfoByUserId(String userId) throws Exception;

	String deleteReward(String id) throws Exception;

	RewardInfo getRewardInfoById(String id) throws Exception;

	List<RewardInfo> getRewardListByUserId(String userId) throws Exception;

	String saveRewardInfo(RewardInfo rewardInfo) throws Exception;

	List<HomeNumVo> getHomeNumListByUserId(String userId) throws Exception;

	HomeNumVo getHomeNumInfoById(String id) throws Exception;

	String saveHomeNumInfo(HomeNumVo homeNum) throws Exception;

	String deleteHomeNum(String id) throws Exception;

}
