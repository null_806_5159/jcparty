package edu.nuaa.party.web.service;

import java.util.List;

import edu.nuaa.party.web.vo.QueryVote;
import edu.nuaa.party.web.vo.SaveVoteVo;
import edu.nuaa.party.web.vo.VoteOptionVo;
import edu.nuaa.party.web.vo.VoteVo;

public interface VoteService {

	List<VoteVo> getVoteList(QueryVote queryVote) throws Exception;

	int getVoteListCount(QueryVote queryVote) throws Exception;

	String getVoteOption(String userId) throws Exception;

	String saveVote(SaveVoteVo vote) throws Exception;

	String updateVoteState(String id, String type) throws Exception;

	List<VoteVo> getWaitVoteList(String userId,String deptId) throws Exception;

	List<VoteOptionVo> getVoteOptionList(String id) throws Exception;

	String saveVoteResult(String chk_valueIds, String voteId, String userId) throws Exception;

	String abstained(String id, String userId) throws Exception;

}
