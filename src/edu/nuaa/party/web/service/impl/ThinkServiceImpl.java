package edu.nuaa.party.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.ThinkMapper;
import edu.nuaa.party.web.service.ThinkService;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.QueryThink;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.ThoughtreportVo;

public class ThinkServiceImpl implements ThinkService {

	@Autowired
	private ThinkMapper thinkMapper;
	
	private Gson gson = new Gson();
	
	//思想汇报上传
	public String saveThink(ThoughtreportVo think) throws Exception {
		int flag = this.thinkMapper.saveThink(think);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >=0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	//获取当前用户的思想汇报上传记录
	public String getThinkUploadList(QueryThink queryThink) throws Exception {
		String result = SysConstant.NULL_DATAGRID_RESULT;
		List<ThoughtreportVo> thinkList = this.thinkMapper.getThinkUploadList(queryThink);
		int total = this.thinkMapper.getThinkUploadListCount(queryThink);
		if(thinkList.size()>0){
			DataGridVo<ThoughtreportVo> datagrid = new DataGridVo<ThoughtreportVo>();
			datagrid.setTotal(total);
			datagrid.setRows(thinkList);
			result = gson.toJson(datagrid);
		}
		return result;
	}

	/**
	 * 根据id获取思想汇报内容
	 */
	public ThoughtreportVo getThinkById(String id) throws Exception {
		ThoughtreportVo think = this.thinkMapper.getThinkById(id);
		return think;
	}

}
