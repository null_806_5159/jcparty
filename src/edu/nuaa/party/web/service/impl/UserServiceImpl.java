package edu.nuaa.party.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.PersonInfoMapper;
import edu.nuaa.party.web.mapper.UserMapper;
import edu.nuaa.party.web.service.UserService;
import edu.nuaa.party.web.utils.StringUtils;
import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.BaseInfoShowVo;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.HomeNumVo;
import edu.nuaa.party.web.vo.PartyInfoShowVo;
import edu.nuaa.party.web.vo.QueryUser;
import edu.nuaa.party.web.vo.RewardInfo;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.SysUser;
import edu.nuaa.party.web.vo.UserDetailVo;
import edu.nuaa.party.web.vo.UserInfo;

public class UserServiceImpl implements UserService {

	private Logger logger = Logger.getLogger(UserServiceImpl.class);
	private Gson gson = new Gson();
	
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private PersonInfoMapper personInfoMapper;
	
	/**
	 * 获取用户信息列表
	 */
	public String getUserInfoList(QueryUser queryUser){
		List<UserInfo> userInfos = new ArrayList<UserInfo>();
		int total = 0;
		try {
			userInfos = userMapper.getUserInfoList(queryUser);
			total = userMapper.getUserInfoCount(queryUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		DataGridVo<UserInfo> datagrid = new DataGridVo<UserInfo>();
		datagrid.setTotal(total);
		datagrid.setRows(userInfos);
		String jsonStr = SysConstant.NULL_DATAGRID_RESULT;
		if(userInfos.size()> 0 && total >0){
			jsonStr = gson.toJson(datagrid);
		}
		logger.info("用户列表===》"+jsonStr);
		return jsonStr;
	}

	/**
	 * 用户清单下载
	 */
	public List<UserInfo> getExportUserInfo(QueryUser queryUser) throws Exception {
		List<UserInfo> userInfos = userMapper.getExportUserInfo(queryUser);
		return userInfos;
	}

	/**
	 * 保存用户信息
	 */
	public String saveUser(SysUser sysUser) throws Exception {
		String id = sysUser.getId();
		String result = "";
		int flag = 0;//保存判断标识
		if(id != null && id.length()>0){//用户Id存在，说明是修改用户信息后的保存
			flag = userMapper.updateUser(sysUser);
		}else{//新增用户
			String userId = StringUtils.getPKByUUID();//UUID生成用户主键，用于插入角色用户中间表
			sysUser.setId(userId);
			sysUser.setToken(SysConstant.USER_TOKEN);
			flag = userMapper.addUser(sysUser);
		}
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}else{
			result = SysConstant.OPERATE_RESULT_FAILE;
		}
		return result;
	}

	/**
	 * 根据Id删除用户
	 */
	public String deleteUserByPK(String id) throws Exception {
		int flag = userMapper.deleteUserByPK(id);//逻辑删除
		String result = "";
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}else{
			result = SysConstant.OPERATE_RESULT_FAILE;
		}
		return result;
	}

	/**
	 *  用户初始化密码
	 */
	public String initPasswd(String id) throws Exception {
		int flag = userMapper.initPasswd(id);//初始化密码MD5()
		String result = "";
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}else{
			result = SysConstant.OPERATE_RESULT_FAILE;
		}
		return result;
	}

	/*根据用户Id获取用户的信息*/
	public String getUserById(String id) throws Exception {
		SysUser user = userMapper.getUserById(id);
		String result = "[]";
		if(user != null){
			result = gson.toJson(user);
		}
		logger.info("根据Id查询的用户信息==》"+result);
		return result;
	}

	//获取辅导员列表
	public String getCounselorList() throws Exception {
		List<BaseComboboxVo> counselorList = this.userMapper.getCounselorList();
		String result = "[]";
		if(counselorList.size()>0){
			result = gson.toJson(counselorList);
		}
		return result;
	}

	/**
	 * 修改密码
	 * @param usercode 用户名
	 * @param newpassword 新密码
	 */
	public String editPasswd(String usercode, String newpassword) throws Exception {
		int flag = this.userMapper.editPasswd(usercode,newpassword);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 根据用户id获取到用户的详细信息
	 * 
	 */
	public UserDetailVo getUserDetailsByUserId(String id) throws Exception {
		UserDetailVo userDetailVo = new UserDetailVo();
		SysUser sysUser = this.userMapper.getUserById(id);
		List<RewardInfo> rewardList = this.personInfoMapper.getRewardListByUserId(id);
		List<HomeNumVo> homeNumList = this.personInfoMapper.getHomeNumListByUserId(id);
		BaseInfoShowVo baseInfoShowVo = this.personInfoMapper.getUserBaseInfoByUserId(id);
		PartyInfoShowVo partyInfo = this.personInfoMapper.getPartyInfoByUserId(id);
		String roleNm = this.userMapper.getRoleNmByUserId(id);
		userDetailVo.setSysUser(sysUser);
		userDetailVo.setRewardList(rewardList);
		userDetailVo.setHomeNumList(homeNumList);
		userDetailVo.setBaseInfoShowVo(baseInfoShowVo);
		userDetailVo.setPartyInfo(partyInfo);
		userDetailVo.setRoleNm(roleNm);
		return userDetailVo;
	}

}
