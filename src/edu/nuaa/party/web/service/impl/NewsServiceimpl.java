package edu.nuaa.party.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.NewsMapper;
import edu.nuaa.party.web.service.NewsService;
import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.IndexNewsVo;
import edu.nuaa.party.web.vo.NewsInfo;
import edu.nuaa.party.web.vo.NewsTypeVo;
import edu.nuaa.party.web.vo.NewsVo;
import edu.nuaa.party.web.vo.QueryNews;
import edu.nuaa.party.web.vo.SysConstant;

public class NewsServiceimpl implements NewsService{

	@Autowired
	private NewsMapper newsmapper;
	private Gson gson = new Gson();
	private Logger logger = Logger.getLogger(NewsServiceimpl.class);
	/**
	 * 获取文章专题列表
	 */
	public String getNewsType() throws Exception {
		List<BaseComboboxVo> newsTypes = newsmapper.getNewsType();
		String jsonStr = "[]";
		if(newsTypes.size()>0){
			jsonStr = gson.toJson(newsTypes);
		}
		return jsonStr;
	}

	/**
	 * 添加文章专题
	 */
	public String addType(NewsTypeVo newsType) throws Exception {
		int flag = newsmapper.addType(newsType);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag > 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 保存文章
	 */
	public String saveNews(NewsVo vo) throws Exception {
		String id = vo.getId();
		int flag = 0;
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(id != null && id.length()>0){//修改文章
			
		}else{//新增文章
			flag = newsmapper.addNews(vo);
		}
		if(flag > 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 获得文章列表
	 */
	public String getNewsList(QueryNews queryNews) throws Exception {
		List<NewsInfo> newsList = newsmapper.getNewsList(queryNews);
		int total = newsmapper.getNewsCount(queryNews);
		String jsonStr = SysConstant.NULL_DATAGRID_RESULT;
		if(newsList.size()>0){
			DataGridVo<NewsInfo> datagrid = new DataGridVo<NewsInfo>();
			datagrid.setRows(newsList);
			datagrid.setTotal(total);
			jsonStr = gson.toJson(datagrid);
		}
		logger.info("文章列表===》"+jsonStr);
		return jsonStr;
	}

	/**
	 * 根据文章Id获取文章详情
	 */
	public NewsInfo getNewsInfo(String id) throws Exception {
		NewsInfo news = newsmapper.getNewsInfo(id);
		return news;
	}

	/**
	 * 获取首页新闻列表
	 */
	public List<IndexNewsVo> getIndexNewsList(String newType) throws Exception {
		List<IndexNewsVo> news = this.newsmapper.getIndexNewsList(newType);
		return news;
	}

	/**
	 * 获取文章专题列表
	 */
	public List<NewsTypeVo> getNewsTypeList() throws Exception {
		List<NewsTypeVo> typeList = this.newsmapper.getNewsTypeList();
		return typeList;
	}

	/**
	 * 文章材料上线、下线
	 */
	public String updateNewsState(String id, String type) throws Exception {
		int flag = this.newsmapper.updateNewsState(id,type);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag>=0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}
	
}
