package edu.nuaa.party.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.PermissionMapper;
import edu.nuaa.party.web.service.PermissionService;
import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.PermissionVo;
import edu.nuaa.party.web.vo.SysConstant;

public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private PermissionMapper permissionMapper;
	private Gson gson = new Gson();
	private Logger logger = Logger.getLogger(PermissionServiceImpl.class);
	/**
	 * 获取角色页面的授权树
	 */
	public String getPermissionTree() throws Exception {
		List<PermissionVo> dataList = this.permissionMapper.getPermissionList();
		List<PermissionVo> menuLever1List = new ArrayList<PermissionVo>();
		List<PermissionVo> menuLever2List = new ArrayList<PermissionVo>();
		List<PermissionVo> pageList = new ArrayList<PermissionVo>();
		for (PermissionVo permissionVo : dataList) {
			if(permissionVo.getFunc_lever().equals("1")){//一级菜单
				menuLever1List.add(permissionVo);
			}else if(permissionVo.getFunc_lever().equals("2")){//二级菜单
				menuLever2List.add(permissionVo);
			}else if(permissionVo.getFunc_lever().equals("3")){//页面上的链接
				pageList.add(permissionVo);
			}
		}
		for (PermissionVo permissionVo : menuLever1List) {
			List<PermissionVo> child = new ArrayList<PermissionVo>();
			for (PermissionVo permission2Vo : menuLever2List) {
				List<PermissionVo> child1 = new ArrayList<PermissionVo>();
				for (PermissionVo permission3Vo : pageList) {
					if(permission3Vo.getParentId().equals(permission2Vo.getId())){
						child1.add(permission3Vo);
					}
				}
				permission2Vo.setChildren(child1);
				if(permission2Vo.getParentId().equals(permissionVo.getId())){
					child.add(permission2Vo);
				}
			}
			permissionVo.setChildren(child);
		}
		String json = gson.toJson(menuLever1List);
		logger.info("权限多选框树===》"+json);
		return json;
	}
	
	/**
	 * 根据权限等级获取父级权限下拉框
	 */
	public String getPermissionByLever(String lever) throws Exception {
		List<BaseComboboxVo> permissionList = this.permissionMapper.getPermissionByLever(lever);
		String result = "[]";
		if(permissionList.size()>0){
			result = gson.toJson(permissionList);
		}
		logger.info("返回父级菜单列表===》"+result);
		return result;
	}

	//权限添加
	public String addPermission(PermissionVo permission) throws Exception {
		int flag = this.permissionMapper.addPermission(permission);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		logger.info("新增权限==》"+result);
		return result;
	}

	//权限修改
	public String editPermission(PermissionVo permission) throws Exception {
		int flag = this.permissionMapper.editPermission(permission);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		logger.info("修改权限==》"+result);
		return result;
	}

	/**
	 * 根据权限id获取权限的信息
	 * @param id
	 */
	public PermissionVo getPermissionById(String id) throws Exception {
		PermissionVo permission = this.permissionMapper.getPermissionById(id);
		return permission;
	}

	/**
	 * 根据权限id删除权限
	 * @param id 权限id
	 */
	public String deletePermission(String id) throws Exception {
		int flag = this.permissionMapper.deletePermission(id);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		logger.info("删除权限==》"+result);
		return result;
	}

	/**
	 * 根据角色id获取已有权限
	 */
	public String getRolePeermission(String roleId) throws Exception {
		List<String> permissions = this.permissionMapper.getRolePeermission(roleId);
		String result = "[]";
		if(permissions.size()>0){
			result = gson.toJson(permissions);
		}
		return result;
	}

}
