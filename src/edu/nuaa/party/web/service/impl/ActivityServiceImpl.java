package edu.nuaa.party.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.ActivityMapper;
import edu.nuaa.party.web.service.ActivityService;
import edu.nuaa.party.web.vo.ActivityInfo;
import edu.nuaa.party.web.vo.ActivityVo;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.QueryActivity;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.weixin.vo.QrcodeVo;

public class ActivityServiceImpl implements ActivityService{

	@Autowired
	private ActivityMapper activityMapper;
	private Gson gson = new Gson();
	private Logger logger = Logger.getLogger(this.getClass());
	
	//获取活动列表的
	public String getActivityList(QueryActivity query) throws Exception {
		List<ActivityInfo> activityList = this.activityMapper.getActivityList(query);
		int count = this.activityMapper.getActivityCount(query);
		String resultStr = SysConstant.NULL_DATAGRID_RESULT;
		if(activityList.size()>0){
			DataGridVo<ActivityInfo> datagrid = new DataGridVo<ActivityInfo>();
			datagrid.setRows(activityList);
			datagrid.setTotal(count);
			resultStr = gson.toJson(datagrid);
		}
		logger.info("活动列表=====》"+resultStr);
		return resultStr;
	}

	/**
	 * 上线活动
	 * @param id 活动id
	 * @param qrcode 活动对应二维码
	 */
	public String upActivity(String id, QrcodeVo qrcode) throws Exception {
		int flag = this.activityMapper.upActivity(id,qrcode.getTicket(),qrcode.getUrl());
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 活动信息修改
	 */
	public String editActivity(ActivityVo activity) throws Exception {
		int flag = this.activityMapper.editActivity(activity);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 活动信息添加
	 */
	public String addActivity(ActivityVo activity) throws Exception {
		int flag = this.activityMapper.addActivity(activity);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 根据id获取活动信息
	 */
	public ActivityVo getActivityById(String id) throws Exception {
		ActivityVo activity = this.activityMapper.getActivityById(id);
		return activity;
	}

	/**
	 * 活动下线
	 */
	public String downActivity(String id) throws Exception {
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = this.activityMapper.downActivity(id);
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

}
