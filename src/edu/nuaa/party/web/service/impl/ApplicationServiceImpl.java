package edu.nuaa.party.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.ApplicationMapper;
import edu.nuaa.party.web.service.ApplicationService;
import edu.nuaa.party.web.vo.ApplyInfo;
import edu.nuaa.party.web.vo.ApproveInfo;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.QueryApply;
import edu.nuaa.party.web.vo.SysConstant;

public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	private ApplicationMapper applicationMapper;
	private Gson gson = new Gson();
	private Logger logger = Logger.getLogger(this.getClass());
	
	//查询我的申请的列表
	public String getApplyList(QueryApply applyCondition) throws Exception {
		List<ApplyInfo> applyInfoList = new ArrayList<ApplyInfo>();
		int total = 0;
		String result = SysConstant.NULL_DATAGRID_RESULT;
		applyInfoList = this.applicationMapper.getApplyList(applyCondition);
		total = this.applicationMapper.getApplyCount(applyCondition);
		if(applyInfoList.size()>0){
			DataGridVo<ApplyInfo> datagrid = new DataGridVo<ApplyInfo>();
			datagrid.setTotal(total);
			datagrid.setRows(applyInfoList);
			result = gson.toJson(datagrid);
		}
		logger.info("我的申请的列表==》"+result);
		return result;
	}

	//保存申请内容
	public String saveApply(ApplyInfo apply) throws Exception {
		int flag = this.applicationMapper.saveApply(apply);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		logger.info("保存申请==》"+result);
		return result;
	}

	/**
	 * 根据受理人id获取当前用户的代办事项
	 */
	public List<ApplyInfo> getApproveList(String acceptor) throws Exception {
		List<ApplyInfo> applyInfoList = this.applicationMapper.getApproveList(acceptor);
		return applyInfoList;
	}

	/**
	 * 根据id获取申请详情
	 */
	public ApproveInfo getApplyDetailById(String id) throws Exception {
		ApproveInfo approve = this.applicationMapper.getApplyDetailById(id);
		return approve;
	}

	/**
	 * 提交审批
	 */
	public String approve(String id, String state, String result) throws Exception {
		int flag = this.applicationMapper.saveApprove(id,state,result);
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}else{
			result = SysConstant.OPERATE_RESULT_FAILE;
		}
		return result;
	}

}
