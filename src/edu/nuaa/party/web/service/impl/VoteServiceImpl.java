package edu.nuaa.party.web.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.VoteMapper;
import edu.nuaa.party.web.service.VoteService;
import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.QueryVote;
import edu.nuaa.party.web.vo.SaveVoteVo;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.VoteOptionVo;
import edu.nuaa.party.web.vo.VoteVo;

public class VoteServiceImpl implements VoteService {

	@Autowired
	private VoteMapper voteMapper;
	private Gson gson = new Gson();
	
	//获取投票列表
	public List<VoteVo> getVoteList(QueryVote queryVote) throws Exception {
		List<VoteVo> voteList = this.voteMapper.getVoteList(queryVote);
		return voteList;
	}
	//获取查询投票列表总数
	public int getVoteListCount(QueryVote queryVote) throws Exception {
		int flag = this.voteMapper.getVoteListCount(queryVote);
		return flag;
	}

	//获取投票候选人列表
	public String getVoteOption(String userId) throws Exception {
		List<BaseComboboxVo> optionList = this.voteMapper.getVoteOption(userId);
		String result = "[]";
		if(optionList.size()>0){
			result = gson.toJson(optionList);
		}
		return result;
	}

	//投票信息保存
	public String saveVote(SaveVoteVo vote) throws Exception {
		String voteId = vote.getId();
		if(voteId == null || voteId.length() == 0){
			voteId = UUID.randomUUID().toString();
			vote.setId(voteId);
		}
		String ids = vote.getCandidateIds();
		String remarks = vote.getContents();
		
		List<VoteOptionVo> voteOptionList = new ArrayList<VoteOptionVo>();//候选人信息列表
		
		String[] idArray = ids.split(",");
		String[] remarkArray = remarks.split("\\|\\|");
		for (int i = 0; i < idArray.length; i++) {
			VoteOptionVo voteOption = new VoteOptionVo();
			
			voteOption.setOptionId(idArray[i]);
			voteOption.setRemark(remarkArray[i]);
			voteOption.setVoteId(voteId);
			voteOptionList.add(voteOption);
		}
		
		int flag = 0;
		String result = SysConstant.OPERATE_RESULT_FAILE;
		
		flag = this.voteMapper.addVoteOption(voteOptionList);
		if(flag < 0){
			return result;
		}else{
			flag = this.voteMapper.addVote(vote);
			if(flag < 0){
				return result;
			}else{
				result = SysConstant.OPERATE_RESULT_SUCCESS;
			}
		}
		return result;
	}
	
	//投票的上线与失效
	public String updateVoteState(String id, String type) throws Exception {
		int flag = this.voteMapper.updateVoteState(id,type);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}
	
	//获取待我投票的投票列表
	public List<VoteVo> getWaitVoteList(String userId,String deptId) throws Exception {
		List<VoteVo> voteList = this.voteMapper.getWaitVoteList(userId,deptId);
		return voteList;
	}
	
	//获取投票中候选人列表
	public List<VoteOptionVo> getVoteOptionList(String id) throws Exception {
		List<VoteOptionVo> voteOptionList = this.voteMapper.getVoteOptionList(id);
		return voteOptionList;
	}
	
	//保存投票结果
	public String saveVoteResult(String chk_valueIds, String voteId, String userId) throws Exception {
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = this.voteMapper.addVoteUser(voteId,userId);
		if(flag >= 0){
			chk_valueIds = "'"+chk_valueIds.replaceAll(",", "','")+"'";
			flag = this.voteMapper.addVoteCount(chk_valueIds);
			if(flag >= 0){
				result = SysConstant.OPERATE_RESULT_SUCCESS;
			}else{
				return result;
			}
		}else{
			return result;
		}
		return result;
	}
	
	//弃权操作
	public String abstained(String id, String userId) throws Exception {
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = this.voteMapper.addAbstainedVote(id,userId);
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	
}
