package edu.nuaa.party.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.NoticeMapper;
import edu.nuaa.party.web.service.NoticeService;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.QueryNotice;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.SysNotice;

public class NoticeServiceImpl implements NoticeService {

	@Autowired
	private NoticeMapper noticeMapper;
	private Gson gson = new Gson();
	private Logger logger = Logger.getLogger(NoticeServiceImpl.class);
	/**
	 * 返回通知列表
	 */
	public String getNoticeList(QueryNotice queryVo) throws Exception{
		List<SysNotice> noticeList = noticeMapper.getNoticeRows(queryVo);
		int total = noticeMapper.getNoticeTotal(queryVo);
		String jsonStr = SysConstant.NULL_DATAGRID_RESULT;
		if(noticeList.size()>0){
			DataGridVo<SysNotice> datagrid = new DataGridVo<SysNotice>();
			datagrid.setRows(noticeList);
			datagrid.setTotal(total);
			jsonStr = gson.toJson(datagrid);
		}
		logger.info("通知列表"+jsonStr);
		return jsonStr;
	}
	/**
	 * 保存公告
	 */
	public String saveNotice(SysNotice notice) throws Exception {
		String id = notice.getId();
		int flag = 0;
		String resultStr = "";
		if(id != null && id.length()>0){//修改通知
			flag = noticeMapper.editNoticeById(notice);
		}else{//新增公告通知
			flag = noticeMapper.addNotice(notice);
		}
		if(flag>=0){
			resultStr=SysConstant.OPERATE_RESULT_SUCCESS;
		}else{
			resultStr=SysConstant.OPERATE_RESULT_FAILE;
		}
		return resultStr;
	}
	/**
	 * 根据Id获取公告信息
	 */
	public SysNotice getNoticeById(String id) throws Exception {
		SysNotice notice = noticeMapper.getNoticeById(id);
		return notice;
	}

	/**
	 * 通知内容审核
	 */
	public String checkNotice(SysNotice notice) throws Exception {
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = this.noticeMapper.checkNotice(notice);
		if(flag>=0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	//通知下线
	public String downNoticeById(String id) throws Exception {
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = this.noticeMapper.downNoticeById(id);
		if(flag>=0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

}
