package edu.nuaa.party.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import edu.nuaa.party.web.mapper.PersonInfoMapper;
import edu.nuaa.party.web.service.PersonInfoService;
import edu.nuaa.party.web.vo.BaseInfoShowVo;
import edu.nuaa.party.web.vo.BaseInfoVo;
import edu.nuaa.party.web.vo.HomeNumVo;
import edu.nuaa.party.web.vo.PartyInfo;
import edu.nuaa.party.web.vo.PartyInfoShowVo;
import edu.nuaa.party.web.vo.RewardInfo;
import edu.nuaa.party.web.vo.SysConstant;

public class PersonInfoServiceImpl implements PersonInfoService {

	@Autowired
	private PersonInfoMapper personInfoMapper;
	private Logger logger = Logger.getLogger(this.getClass());
	
	//修改基本信息
	public String editBaseInfo(BaseInfoVo baseInfo) throws Exception {
		int flag = this.personInfoMapper.editBaseInfo(baseInfo);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		logger.info("修改基本信息==》"+result);
		return result;
	}

	//新增基本信息
	public String addBaseInfo(BaseInfoVo baseInfo) throws Exception {
		int flag = this.personInfoMapper.addBaseInfo(baseInfo);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		logger.info("新增基本信息==》"+result);
		return result;
	}

	/**
	 * 根据userid获取到个人基本信息
	 * @param userid 用户id
	 */
	public BaseInfoShowVo getUserBaseInfoByUserId(String userId) throws Exception {
		BaseInfoShowVo baseInfo = this.personInfoMapper.getUserBaseInfoByUserId(userId);
		return baseInfo;
	}

	/**
	 * 保存党内信息，若Id存在则为修改，否则为新增
	 */
	public String savePartyInfo(PartyInfo partyInfo) throws Exception {
		String id = partyInfo.getId();
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = 0;
		if(id != null && id.length()>0){
			flag = this.personInfoMapper.editPartyInfo(partyInfo);
		}else{
			flag = this.personInfoMapper.addPartyInfo(partyInfo);
		}
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 根据用户id获取党内信息
	 * @param userId 用户id
	 */
	public PartyInfoShowVo getPartyInfoByUserId(String userId) throws Exception {
		PartyInfoShowVo partyInfo = this.personInfoMapper.getPartyInfoByUserId(userId);
		return partyInfo;
	}

	//删除获奖信息
	public String deleteReward(String id) throws Exception {
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = 0;
		flag = this.personInfoMapper.deleteReward(id);
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 根据奖项id获取奖惩信息
	 */
	public RewardInfo getRewardInfoById(String id) throws Exception {
		RewardInfo rewardInfo = this.personInfoMapper.getRewardInfoById(id);
		return rewardInfo;
	}

	/**
	 * 根据用户id获取到对应的获奖集合
	 */
	public List<RewardInfo> getRewardListByUserId(String userId) throws Exception {
		List<RewardInfo> rewards = this.personInfoMapper.getRewardListByUserId(userId);
		return rewards;
	}

	//保存获奖信息
	public String saveRewardInfo(RewardInfo rewardInfo) throws Exception {
		String id = rewardInfo.getId();
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = 0;
		if(id != null && id.length() > 0){
			flag = this.personInfoMapper.editReward(rewardInfo);
		}else{
			flag = this.personInfoMapper.addReward(rewardInfo);
		}
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 根据用户id获取家庭主要成员的信息列表
	 * @param userId 用户id
	 */
	public List<HomeNumVo> getHomeNumListByUserId(String userId) throws Exception {
		List<HomeNumVo> homeNums = this.personInfoMapper.getHomeNumListByUserId(userId);
		return homeNums;
	}

	/**
	 * 根据id获取家庭主要成员的信息
	 * @param id 主键id 
	 */
	public HomeNumVo getHomeNumInfoById(String id) throws Exception {
		HomeNumVo homeNum = this.personInfoMapper.getHomeNumInfoById(id);
		return homeNum;
	}

	/**
	 * 保存家庭主要成员的信息
	 */
	public String saveHomeNumInfo(HomeNumVo homeNum) throws Exception {
		String id = homeNum.getId();
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = 0;
		if(id != null && id.length() > 0){
			flag = this.personInfoMapper.editHomeNumInfo(homeNum);
		}else{
			flag = this.personInfoMapper.addHomeNumInfo(homeNum);
		}
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	/**
	 * 根据id删除家庭成员信息
	 * @param id 主键id
	 */
	public String deleteHomeNum(String id) throws Exception {
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = 0;
		flag = this.personInfoMapper.deleteHomeNum(id);
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

}
