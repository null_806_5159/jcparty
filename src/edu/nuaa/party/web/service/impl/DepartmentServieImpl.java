package edu.nuaa.party.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.DeptMapper;
import edu.nuaa.party.web.service.DepartmentServie;
import edu.nuaa.party.web.vo.BaseCollegeVo;
import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.BaseMajorVo;
import edu.nuaa.party.web.vo.DeptmentVo;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.BaseDeptVo;


public class DepartmentServieImpl implements DepartmentServie {

	private Logger logger = Logger.getLogger(DepartmentServieImpl.class);
	
	@Autowired
	private DeptMapper deptMapper;
	
	private Gson gson = new Gson();
	/**
	 * 获取部门列表
	 */
	public String getDeptList() throws Exception {
		List<BaseMajorVo> dataList = deptMapper.getDeptInfoList();//获取全部数据
		List<BaseMajorVo> majorList = new ArrayList<BaseMajorVo>();//专业列表
		List<BaseDeptVo> deptList = new ArrayList<BaseDeptVo>();//系部列表
		List<BaseCollegeVo> collegeList = new ArrayList<BaseCollegeVo>();//院列表
		List<BaseCollegeVo> treeList = new ArrayList<BaseCollegeVo>();//返回的数据
		for (BaseMajorVo baseVo : dataList) {
			BaseDeptVo dept = new BaseDeptVo();
			BaseMajorVo major = new BaseMajorVo();
			BaseCollegeVo college = new BaseCollegeVo();
			if(baseVo.getLever().equals("0")){//学院
				college.setId(baseVo.getId());//id 
				college.setName(baseVo.getName());//名称
				college.setManagerNm(baseVo.getManagerNm());//负责人
				college.setPid(baseVo.getPid());//父级Id
				college.setLever(baseVo.getLever());
				college.setCreatetime(baseVo.getCreatetime());//创建时间
				college.setDeptNum(baseVo.getDeptNum());//学院编码
				collegeList.add(college);
			}else if(baseVo.getLever().equals("1")){//系部
				dept.setId(baseVo.getId());//id 
				dept.setName(baseVo.getName());//名称
				dept.setManagerNm(baseVo.getManagerNm());//负责人
				dept.setPid(baseVo.getPid());//父级Id
				dept.setLever(baseVo.getLever());
				dept.setCreatetime(baseVo.getCreatetime());//创建时间
				dept.setDeptNum(baseVo.getDeptNum());//系部编码
				deptList.add(dept);
			}else if(baseVo.getLever().equals("2")){//专业
				major.setId(baseVo.getId());//id 
				major.setName(baseVo.getName());//名称
				major.setManagerNm(baseVo.getManagerNm());//负责人
				major.setPid(baseVo.getPid());//父级Id
				major.setLever(baseVo.getLever());
				major.setCreatetime(baseVo.getCreatetime());//创建时间
				major.setDeptNum(baseVo.getDeptNum());//系部编码
				majorList.add(major);
			}
		}
		
		for (BaseCollegeVo college : collegeList) {
			List<BaseDeptVo> depts = new ArrayList<BaseDeptVo>();
			for (BaseDeptVo dept : deptList) {
				List<BaseMajorVo> majors = new ArrayList<BaseMajorVo>();
				for (BaseMajorVo major : majorList) {
					if(major.getPid().equals(dept.getId())){
						majors.add(major);
					}
				}
				dept.setChildren(majors);
				if(dept.getPid().equals(college.getId())){
					depts.add(dept);
				}
			}
			college.setChildren(depts);
			treeList.add(college);
		}
		String deptJson = gson.toJson(treeList);
		logger.info("部门树形网格数据===》"+deptJson);
		return deptJson;
	}

	/**
	 * 获取系部的下拉框
	 */
	public String getDeptComboBox() throws Exception {
		List<BaseComboboxVo> deptList = deptMapper.getDeptComboBox();
		String resultStr = "[]";
		if(deptList.size()>0){
			resultStr = gson.toJson(deptList);
		}
		return resultStr;
	}

	/**
	 * 根据Id删除系部、专业
	 */
	public String deleteDept(String id) throws Exception {
		int flag = deptMapper.deleteDept(id);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag>=0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	@Override
	public String getDeptTreeGrid() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 根据部门等级获取信息
	 */
	public String getDeptByLever(String lever) throws Exception {
		List<BaseComboboxVo> deptList = this.deptMapper.getDeptByLever(lever);
		String result = "[]";
		if(deptList.size()>0){
			result = gson.toJson(deptList);
		}
		return result;
	}

	/**
	 * 保存部门
	 */
	public String saveDept(DeptmentVo dept, String userId) throws Exception {
		String id = dept.getId();
		String result = SysConstant.OPERATE_RESULT_FAILE;
		int flag = 0;
		if(id != null && id.length()>0){
			dept.setUpdator(userId);
			flag = this.deptMapper.editDept(dept);
		}else{
			dept.setCreator(userId);
			flag = this.deptMapper.addDept(dept);
		}
		if(flag>=0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	//根据id获取部门信息
	public DeptmentVo getDeptById(String id) throws Exception {
		DeptmentVo dept = this.deptMapper.getDeptById(id);
		return dept;
	}

	//获取可选择的部门管理人
	public String getDeptMa() throws Exception {
		List<BaseComboboxVo> managerList = this.deptMapper.getDeptMa();
		String result = "[]";
		if(managerList.size()>0){
			result = gson.toJson(managerList);
		}
		return result;
	}

	//设置部门管理人员
	public String setDeptManager(String maId, String id) throws Exception {
		int flag = this.deptMapper.setDeptManager(maId,id);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

}
