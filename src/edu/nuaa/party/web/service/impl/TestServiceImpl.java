package edu.nuaa.party.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.TestMapper;
import edu.nuaa.party.web.service.TestService;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.QueryTest;
import edu.nuaa.party.web.vo.SysConstant;
import edu.nuaa.party.web.vo.TestQuestionVo;
import edu.nuaa.party.web.vo.TestVo;

public class TestServiceImpl implements TestService {

	@Autowired
	private TestMapper testMapper;
	
	private Gson gson = new Gson();
	private Logger logger = Logger.getLogger(TestServiceImpl.class);
	
	//获取测试试题信息列表
	public String getTestList(QueryTest queryTest) throws Exception {
		List<TestVo> testList = this.testMapper.getTestList(queryTest);
		int total = this.testMapper.getTestListCount(queryTest);
		
		String jsonStr = SysConstant.NULL_DATAGRID_RESULT;
		if(testList.size()>0){
			DataGridVo<TestVo> datagrid = new DataGridVo<TestVo>();
			datagrid.setRows(testList);
			datagrid.setTotal(total);
			jsonStr = gson.toJson(datagrid);
		}
		logger.info("试题列表===》"+jsonStr);
		return jsonStr;
	}

	//试卷的上线与失效
	public String updateTestState(String id, String type) throws Exception {
		int flag = this.testMapper.updateTestState(id,type);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

	//获取当前用户尚未进行的试卷
	public List<TestVo> getWaitTestList(String deptId,String userId) throws Exception {
		List<TestVo> testList = this.testMapper.getWaitTestList(deptId,userId);
		return testList;
	}

	//获取试卷下的所有试题
	public List<TestQuestionVo> getQuestionListByTestId(String id) throws Exception {
		List<TestQuestionVo> questionList = this.testMapper.getQuestionListByTestId(id);
		return questionList;
	}

	//根据id获取试卷信息
	public TestVo getTestById(String id) throws Exception {
		TestVo test = this.testMapper.getTestById(id);
		return test;
	}

	//保存答题成绩
	public String saveScore(String score, String testId, String userId) throws Exception {
		int flag = this.testMapper.saveScore(score,testId,userId);
		String result = SysConstant.OPERATE_RESULT_FAILE;
		if(flag >= 0){
			result = SysConstant.OPERATE_RESULT_SUCCESS;
		}
		return result;
	}

}
