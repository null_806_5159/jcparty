package edu.nuaa.party.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.RoleMapper;
import edu.nuaa.party.web.service.RoleService;
import edu.nuaa.party.web.vo.BaseRole;
import edu.nuaa.party.web.vo.DataGridVo;
import edu.nuaa.party.web.vo.PerminssionRoleVo;
import edu.nuaa.party.web.vo.QueryRole;
import edu.nuaa.party.web.vo.RoleInfo;
import edu.nuaa.party.web.vo.SysConstant;

public class RoleServiceImpl implements RoleService{

	private Logger logger = Logger.getLogger(RoleServiceImpl.class);
	private Gson gson = new Gson();
	
	@Autowired
	private RoleMapper roleMapper;
	
	public String getRoleList() {
		List<BaseRole> roleList = new ArrayList<BaseRole>();;
		try {
			roleList = roleMapper.getRoleList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String jsonStr = "[]";
		if(roleList.size()>0){
			jsonStr = gson.toJson(roleList);
		}
		logger.info("角色条件列表===》"+jsonStr);
		return jsonStr;
	}

	public String getRoleDataGrid(QueryRole queryRole) throws Exception{
		List<RoleInfo> roleList = roleMapper.getDataList(queryRole);
		int total = roleMapper.getDataCount(queryRole);
		String jsonStr = "";
		if(roleList.size()>0){
			DataGridVo<RoleInfo> roleDataGrid = new DataGridVo<RoleInfo>();
			roleDataGrid.setTotal(total);
			roleDataGrid.setRows(roleList);
			jsonStr = gson.toJson(roleDataGrid);
		}else{
			jsonStr = SysConstant.NULL_DATAGRID_RESULT;
		}
		logger.info("角色信息列表==》"+jsonStr);
		return jsonStr;
	}

	/**
	 * 角色信息保存
	 */
	public String saveRole(RoleInfo editRoleVo) throws Exception{
		String id = editRoleVo.getId();
		int flag = 0;
		String resultStr = "";
		if(id != null && id.length()>0){//角色修改
			flag = roleMapper.editRole(editRoleVo);
			
		}else{//角色新增
			flag = roleMapper.saveRole(editRoleVo);
		}
		if(flag>=0){
			resultStr=SysConstant.OPERATE_RESULT_SUCCESS;
		}else{
			resultStr=SysConstant.OPERATE_RESULT_FAILE;
		}
		return resultStr;
	}

	/**
	 * 根据角色Id获取到角色的相关信息
	 */
	public String getRoleInfoById(String id)  throws Exception{
		RoleInfo roleInfo = roleMapper.getRoleInfoById(id);
		String jsonStr = "{}";
		if(roleInfo != null){
			jsonStr = gson.toJson(roleInfo);
		}
		return jsonStr;
	}

	/**
	 * 根绝角色id删除
	 */
	public String deleteRoleById(String id) throws Exception {
		int flag = roleMapper.deleteRoleById(id);
		String resultStr = "";
		if(flag>=0){
			resultStr=SysConstant.OPERATE_RESULT_SUCCESS;
		}else{
			resultStr=SysConstant.OPERATE_RESULT_FAILE;
		}
		return resultStr;
	}

	/**
	 * 角色授权
	 * @param funcIds 权限组合串
	 */
	public String authToRole(String funcIds,String roleId) throws Exception {
		int flag = 0;
		String result = SysConstant.OPERATE_RESULT_FAILE;
		List<PerminssionRoleVo> perminssionRoleVos = new ArrayList<PerminssionRoleVo>();
		//先根据角色Id删除掉已经存储的权限记录
		flag = roleMapper.deleteFuncRoleByRoleId(roleId);
		if(flag >= 0){
			//存储权限记录
			String[] funcIdStrs = funcIds.split(",");
			for (int i = 0; i < funcIdStrs.length; i++) {
				PerminssionRoleVo perminssionRoleVo = new PerminssionRoleVo();
				perminssionRoleVo.setRoleId(roleId);
				perminssionRoleVo.setFuncId(funcIdStrs[i]);
				perminssionRoleVos.add(perminssionRoleVo);
			}
			flag = roleMapper.saveFuncRole(perminssionRoleVos);
			if(flag>=0){
				result = SysConstant.OPERATE_RESULT_SUCCESS;
			}
		}
		return result;
	}
	
}
