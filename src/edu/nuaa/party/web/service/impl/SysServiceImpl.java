package edu.nuaa.party.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import edu.nuaa.party.web.mapper.SysMapper;
import edu.nuaa.party.web.mapper.UserMapper;
import edu.nuaa.party.web.service.SysService;
import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.BaseComboboxVo;
import edu.nuaa.party.web.vo.MenuVo;
import edu.nuaa.party.web.vo.SysUser;

public class SysServiceImpl implements SysService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private SysMapper sysMapper;
	private Gson gson = new Gson();
	private Logger logger = Logger.getLogger(SysServiceImpl.class);
	
	//用户信息认证
	public ActiveUser authenticad(String usercode, String password) throws Exception {
		//根据账号查询数据库、
		SysUser user = this.checkUser(usercode,password);
		if(user == null){
			return null;
		}else{
			ActiveUser activeUser =  new ActiveUser();
			List<MenuVo> topMenuList = this.sysMapper.getMenuList(usercode,"1");
			List<MenuVo> menuList = this.sysMapper.getMenuList(usercode,"2");
			activeUser.setTopMenuList(topMenuList);
			activeUser.setMenuList(menuList);
			activeUser.setUsercode(usercode);
			activeUser.setUsername(user.getUsername());
			activeUser.setUserId(user.getId());
			activeUser.setPassword(user.getPassword());
			activeUser.setDeptId(user.getDeptId());
			return activeUser;
		}
		
	}
	
	public SysUser checkUser(String usercode,String password) throws Exception{
		SysUser user = userMapper.checkUser(usercode,password);
		if(user != null){
			return user;
		}
		return null;
	}

	//省列表
	public String getProvince() throws Exception {
		List<BaseComboboxVo> provinceList = this.sysMapper.getProvince();
		String result = "[]";
		if(provinceList.size()>0){
			result = gson.toJson(provinceList);
		}
		logger.info("省列表==》"+result);
		return result;
	}

	/**
	 * @param pcode 省级编码
	 */
	public String getCity(String pcode) throws Exception {
		List<BaseComboboxVo> provinceList = this.sysMapper.getCity(pcode);
		String result = "[]";
		if(provinceList.size()>0){
			result = gson.toJson(provinceList);
		}
		logger.info("市列表==》"+result);
		return result;
	}

	/**
	 * @param pcode 市级编码
	 */
	public String getCounty(String pcode) throws Exception {
		List<BaseComboboxVo> provinceList = this.sysMapper.getCounty(pcode);
		String result = "[]";
		if(provinceList.size()>0){
			result = gson.toJson(provinceList);
		}
		logger.info("区县列表==》"+result);
		return result;
	}
}
