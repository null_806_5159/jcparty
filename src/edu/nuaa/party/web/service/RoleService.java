package edu.nuaa.party.web.service;

import edu.nuaa.party.web.vo.QueryRole;
import edu.nuaa.party.web.vo.RoleInfo;

public interface RoleService {

	String getRoleList();

	String getRoleDataGrid(QueryRole queryRole) throws Exception;

	String saveRole(RoleInfo editRoleVo) throws Exception;

	String getRoleInfoById(String id) throws Exception;

	String deleteRoleById(String id) throws Exception;

	String authToRole(String funcIds,String roleId) throws Exception;

}
