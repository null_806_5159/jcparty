package edu.nuaa.party.web.service;

import edu.nuaa.party.web.vo.ActivityVo;
import edu.nuaa.party.web.vo.QueryActivity;
import edu.nuaa.party.weixin.vo.QrcodeVo;

public interface ActivityService {

	String getActivityList(QueryActivity query) throws Exception;

	String upActivity(String id, QrcodeVo qrcode) throws Exception;

	String editActivity(ActivityVo activity) throws Exception;

	String addActivity(ActivityVo activity) throws Exception;

	ActivityVo getActivityById(String id) throws Exception;

	String downActivity(String id) throws Exception;

}
