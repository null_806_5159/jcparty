package edu.nuaa.party.web.service;

import java.util.List;

import edu.nuaa.party.web.vo.QueryTest;
import edu.nuaa.party.web.vo.TestQuestionVo;
import edu.nuaa.party.web.vo.TestVo;

public interface TestService {

	String getTestList(QueryTest queryTest) throws Exception;

	String updateTestState(String id, String type) throws Exception;

	List<TestVo> getWaitTestList(String deptId,String userId) throws Exception;

	List<TestQuestionVo> getQuestionListByTestId(String id) throws Exception;

	TestVo getTestById(String id) throws Exception;

	String saveScore(String score, String testId, String userId) throws Exception;

}
