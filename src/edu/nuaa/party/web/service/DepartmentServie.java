package edu.nuaa.party.web.service;

import edu.nuaa.party.web.vo.DeptmentVo;

public interface DepartmentServie {

	String getDeptList() throws Exception;

	String getDeptComboBox() throws Exception;

	String deleteDept(String id) throws Exception;

	String getDeptTreeGrid() throws Exception;

	String getDeptByLever(String lever) throws Exception;

	String saveDept(DeptmentVo dept, String userId) throws Exception;

	DeptmentVo getDeptById(String id) throws Exception;

	String getDeptMa() throws Exception;

	String setDeptManager(String maId, String id) throws Exception;
	
}
