package edu.nuaa.party.web.service;

import java.util.List;

import edu.nuaa.party.web.vo.ApplyInfo;
import edu.nuaa.party.web.vo.ApproveInfo;
import edu.nuaa.party.web.vo.QueryApply;

public interface ApplicationService {

	String getApplyList(QueryApply applyCondition) throws Exception;

	String saveApply(ApplyInfo apply) throws Exception;

	List<ApplyInfo> getApproveList(String userId) throws Exception;

	ApproveInfo getApplyDetailById(String id) throws Exception;

	String approve(String id, String state, String result) throws Exception;

}
