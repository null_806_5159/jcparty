package edu.nuaa.party.web.service;

import java.util.List;

import edu.nuaa.party.web.vo.IndexNewsVo;
import edu.nuaa.party.web.vo.NewsInfo;
import edu.nuaa.party.web.vo.NewsTypeVo;
import edu.nuaa.party.web.vo.NewsVo;
import edu.nuaa.party.web.vo.QueryNews;

public interface NewsService {

	String getNewsType() throws Exception;

	String addType(NewsTypeVo newsType) throws Exception;

	String saveNews(NewsVo vo) throws Exception;

	String getNewsList(QueryNews queryNews) throws Exception;

	NewsInfo getNewsInfo(String id) throws Exception;

	List<IndexNewsVo> getIndexNewsList(String newType) throws Exception;

	List<NewsTypeVo> getNewsTypeList() throws Exception;

	String updateNewsState(String id, String type) throws Exception;

}
