package edu.nuaa.party.web.service;

import edu.nuaa.party.web.vo.PermissionVo;

public interface PermissionService {

	String getPermissionTree() throws Exception;

	String getPermissionByLever(String lever) throws Exception;

	String addPermission(PermissionVo permission) throws Exception;

	String editPermission(PermissionVo permission) throws Exception;

	PermissionVo getPermissionById(String id) throws Exception;

	String deletePermission(String id) throws Exception;

	String getRolePeermission(String roleId) throws Exception;

}
