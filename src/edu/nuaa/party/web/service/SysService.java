package edu.nuaa.party.web.service;

import edu.nuaa.party.web.vo.ActiveUser;
import edu.nuaa.party.web.vo.SysUser;

/**
 * 认证授权服务接口
 * @author Administrator
 *
 */
public interface SysService {
	
	//用户身份认证
	ActiveUser authenticad(String usercode,String password) throws Exception;
	
	SysUser checkUser(String usercode,String password) throws Exception;

	String getProvince() throws Exception;

	String getCity(String pcode) throws Exception;

	String getCounty(String pcode) throws Exception;
}
