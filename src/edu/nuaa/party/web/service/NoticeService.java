package edu.nuaa.party.web.service;

import edu.nuaa.party.web.vo.QueryNotice;
import edu.nuaa.party.web.vo.SysNotice;

public interface NoticeService {

	String getNoticeList(QueryNotice queryVo) throws Exception;

	String saveNotice(SysNotice notice) throws Exception;

	SysNotice getNoticeById(String id) throws Exception;

	String checkNotice(SysNotice notice) throws Exception;

	String downNoticeById(String id) throws Exception;

}
