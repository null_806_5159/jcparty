package edu.nuaa.party.weixin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.nuaa.party.weixin.utils.MessageUtils;
import edu.nuaa.party.weixin.utils.SignUtil;
import edu.nuaa.party.weixin.vo.TextMessage;
import edu.nuaa.party.weixin.vo.WeiXinConstant;

@Controller
@RequestMapping("/wechatController")
public class WechatController {

	private static final String TOKEN = "jcparty";
	
	/**
	 * 微信公众号的接入
	 * @param request
	 * @param response
	 * @param signature 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
	 * @param timestamp 时间戳
	 * @param nonce 随机数
	 * @param echostr 随机字符串
	 */
	@RequestMapping(value="/wechat", method = RequestMethod.GET)
	public void wechatGet(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "signature") String signature,
			@RequestParam(value = "timestamp") String timestamp,
			@RequestParam(value = "nonce") String nonce,
			@RequestParam(value = "echostr") String echostr) {

			if (SignUtil.checkSignature(TOKEN, signature,
					timestamp, nonce)) {
				try {
					response.getWriter().print(echostr);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	}
	
	@RequestMapping(value = "/wechat", method = RequestMethod.POST)
	public void wechatPost(HttpServletResponse response,
			HttpServletRequest request) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		Map<String,String> map = MessageUtils.xmlToMap(request);
		String ToUserName = map.get("ToUserName");	//开发者微信号
		String FromUserName = map.get("FromUserName");	//发送方帐号（一个OpenID）
		String MsgType = map.get("MsgType");	//text
		String Content = map.get("Content");	//文本消息内容
		
		String message = "";
		if(WeiXinConstant.MESSAGE_TEXT.equals(MsgType)){
			TextMessage text = new TextMessage();
			text.setFromUserName(ToUserName);
			text.setToUserName(FromUserName);
			text.setMsgType(WeiXinConstant.MESSAGE_TEXT);
			text.setCreateTime(new Date().getTime());
			text.setContent("你好，你发送的内容是"+Content);
			message = MessageUtils.textMessageToXml(text);
		}else if(WeiXinConstant.MESSAGE_EVENT.equals(MsgType)){
			String eventType = map.get("Event");
			if(WeiXinConstant.MESSAGE_SUBSCRIBE.equals(eventType)){
				TextMessage text = new TextMessage();
				text.setFromUserName(ToUserName);
				text.setToUserName(FromUserName);
				text.setMsgType(WeiXinConstant.MESSAGE_TEXT);
				text.setCreateTime(new Date().getTime());
				text.setContent("请单击“个人中心”菜单，进行登录绑定！");
				message = MessageUtils.textMessageToXml(text);
			}else if(WeiXinConstant.MESSAGE_SCAN.equals(eventType)){
				String EventKey = map.get("EventKey");//获取到活动的Id
				TextMessage text = new TextMessage();
				text.setFromUserName(ToUserName);
				text.setToUserName(FromUserName);
				text.setMsgType(WeiXinConstant.MESSAGE_TEXT);
				text.setCreateTime(new Date().getTime());
				text.setContent("您已签到成功，你的openId为==》"+FromUserName);
				message = MessageUtils.textMessageToXml(text);
			}
		}
		PrintWriter out = response.getWriter();
		out.print(message);
		out.close();
	}
}
