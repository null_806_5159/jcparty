package edu.nuaa.party.weixin.service.impl;

import com.google.gson.JsonObject;

import edu.nuaa.party.weixin.service.QrcodeService;
import edu.nuaa.party.weixin.utils.HttpUtils;
import edu.nuaa.party.weixin.vo.QrcodeVo;

/**
 * 微信二维码
 * @author neil
 *
 */
public class QrcodeServiceImpl implements QrcodeService {

	/**
	 * 生成带参数的永久二维码
	 * @param scene_str 场景值ID 长度限制为1到64   活动Id
	 * @return
	 * @throws Exception
	 */
	public QrcodeVo createQrcode(String scene_str,String token) throws Exception{
		String json = "{\"action_name\": \"QR_LIMIT_STR_SCENE\", \"action_info\": "
				    + "{\"scene\": {\"scene_str\": \""+scene_str+"\"}}}";
		String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN".replace("TOKEN", token);
		JsonObject jsonObject = HttpUtils.doPost(url, json);
		QrcodeVo qrcode = new QrcodeVo();
		if(jsonObject != null){
			qrcode.setTicket(jsonObject.get("ticket").getAsString());
			qrcode.setUrl(jsonObject.get("url").getAsString());
		}
		return qrcode;
	}
}
