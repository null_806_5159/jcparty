package edu.nuaa.party.weixin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import edu.nuaa.party.weixin.mapper.AccessTokenMapper;
import edu.nuaa.party.weixin.service.AccessTokenService;
import edu.nuaa.party.weixin.utils.HttpUtils;
import edu.nuaa.party.weixin.vo.AccessToken;
import edu.nuaa.party.weixin.vo.WeiXinConstant;
/**
 * access_token操作类
 * @author Administrator
 *
 */
public class AccessTokenServiceImpl implements AccessTokenService{

	@Autowired
	private AccessTokenMapper sccessTokenMapper;
	
	//返回
	public String getAccessToken() throws Exception{
		String access_token = "";
		//判断数据库中的access_token的有效时间，如果没有超时，就使用数据库中的access_token
		AccessToken accessToken = this.sccessTokenMapper.getAccessToken(WeiXinConstant.ACCESS_TOKEN_ID);
		if((System.currentTimeMillis()+300000) <= accessToken.getExpires_in()){//当前access_token 有效
			access_token = accessToken.getAccess_token();
		}else{//重新获取，并更新数据库中的access_token
			accessToken = HttpUtils.getAccessToken();
			accessToken.setId(WeiXinConstant.ACCESS_TOKEN_ID);
			accessToken.setExpires_in((accessToken.getExpires_in()*1000)+System.currentTimeMillis());
			this.sccessTokenMapper.updateAccessToken(accessToken);
			access_token = accessToken.getAccess_token();
		}
		
		return access_token;
	}
}
