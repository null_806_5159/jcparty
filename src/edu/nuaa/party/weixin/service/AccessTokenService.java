package edu.nuaa.party.weixin.service;

public interface AccessTokenService {

	String getAccessToken() throws Exception;
}
