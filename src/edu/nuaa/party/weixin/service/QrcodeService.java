package edu.nuaa.party.weixin.service;

import edu.nuaa.party.weixin.vo.QrcodeVo;

public interface QrcodeService {

	QrcodeVo createQrcode(String scene_str,String token) throws Exception;

}
