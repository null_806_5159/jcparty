package edu.nuaa.party.weixin.utils;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import edu.nuaa.party.weixin.vo.AccessToken;
import edu.nuaa.party.weixin.vo.WeiXinConstant;
/**
 * Http方法类
 * @author Administrator
 *
 */
public class HttpUtils {

	/**
	 * Get请求
	 * @param url 请求路径
	 * @return
	 */
	public static JsonObject doGet(String url){
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		JsonObject jsonObject = null;
		try {
			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			if(entity!=null){
				String result = EntityUtils.toString(entity,"UTF-8");
				jsonObject = new JsonParser().parse(result).getAsJsonObject();
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return jsonObject;
	}
	/**
	 * post请求
	 * @param url 访问路径
	 * @param outStr 参数串
	 * @return
	 */
	public static JsonObject doPost(String url,String outStr){
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		JsonObject jsonObject = null;
		try {
			httpPost.setEntity(new StringEntity(outStr,"UTF-8"));
			HttpResponse response = httpClient.execute(httpPost);
			String result = EntityUtils.toString(response.getEntity(),"UTF-8");
			jsonObject = new JsonParser().parse(result).getAsJsonObject();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return jsonObject;
	}
	/**
	 * 获取Access_token
	 * @return
	 */
	public static AccessToken getAccessToken(){
		AccessToken accessToken = new AccessToken();
		String url = WeiXinConstant.ACCESS_TOKEN_URL.replace("APPID", WeiXinConstant.APPID).replace("APPSECRET", WeiXinConstant.APPSECRET);
		JsonObject jsonObject = doGet(url);
		if(jsonObject != null){
			accessToken.setAccess_token(jsonObject.get("access_token").getAsString());
			accessToken.setExpires_in(jsonObject.get("expires_in").getAsLong());
		}
		return accessToken;
	}
}
