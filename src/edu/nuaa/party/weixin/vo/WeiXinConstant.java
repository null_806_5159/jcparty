package edu.nuaa.party.weixin.vo;
/**
 * 微信开发相关常量
 * @author neil
 *
 */
public class WeiXinConstant {

	public static final String APPID = "wx291e4905c41943da";
	public static final String APPSECRET = "64ad9859f5702a9f1b5514c037a40685";
	
	//消息类型
	public static final String MESSAGE_TEXT = "text";//文本消息
	public static final String MESSAGE_IMAGE = "image";//图片消息
	public static final String MESSAGE_VOICE = "voice";//语音消息
	public static final String MESSAGE_VIDEO = "video";//视频消息
	public static final String MESSAGE_LINK = "link";//链接消息
	public static final String MESSAGE_LOCATION = "location";//地理位置
	public static final String MESSAGE_EVENT = "event";//事件
	public static final String MESSAGE_SUBSCRIBE = "subscribe";//关注消息
	public static final String MESSAGE_UNSUBSCRIBE = "unsubscribe";//取消关注
	public static final String MESSAGE_CLICK = "CLICK";//
	public static final String MESSAGE_VIEW = "VIEW";
	public static final String MESSAGE_SCAN  = "SCAN";
	
	public static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

	public static final String ACCESS_TOKEN_ID = "1";
	
}
