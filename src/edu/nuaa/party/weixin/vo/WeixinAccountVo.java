package edu.nuaa.party.weixin.vo;

/**   
 * @Title: Entity
 * @Description: 微信公众帐号信息
 *
 */
public class WeixinAccountVo{
	
	/**主键*/
	private java.lang.String id;
	/**公众帐号名称*/
	private java.lang.String accountname;
	/**公众帐号TOKEN*/
	private java.lang.String accounttoken;
	/**公众微信号*/
	private java.lang.String accountnumber;
	/**公众原始ID*/
	private java.lang.String weixin_accountid;
	/**公众号类型*/
	private java.lang.String accounttype;
	/**电子邮箱*/
	private java.lang.String accountemail;
	/**公众帐号描述*/
	private java.lang.String accountdesc;
	/**公众帐号APPID*/
	private java.lang.String accountappid;
	/**公众帐号APPSECRET*/
	private java.lang.String accountappsecret;
	/**ACCESS_TOKEN*/
	private java.lang.String accountaccesstoken;
	/**TOKEN获取时间*/
	private java.util.Date addtoekntime;
	/**所属系统用户**/
	private java.lang.String userName;
	/**jsapi调用接口临时凭证*/
	private java.lang.String jsapiticket;
	/**jsapi调用接口临时凭证的获取时间*/
	private java.util.Date jsapitickettime;
	public java.lang.String getId() {
		return id;
	}
	public void setId(java.lang.String id) {
		this.id = id;
	}
	public java.lang.String getAccountname() {
		return accountname;
	}
	public void setAccountname(java.lang.String accountname) {
		this.accountname = accountname;
	}
	public java.lang.String getAccounttoken() {
		return accounttoken;
	}
	public void setAccounttoken(java.lang.String accounttoken) {
		this.accounttoken = accounttoken;
	}
	public java.lang.String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(java.lang.String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public java.lang.String getWeixin_accountid() {
		return weixin_accountid;
	}
	public void setWeixin_accountid(java.lang.String weixin_accountid) {
		this.weixin_accountid = weixin_accountid;
	}
	public java.lang.String getAccounttype() {
		return accounttype;
	}
	public void setAccounttype(java.lang.String accounttype) {
		this.accounttype = accounttype;
	}
	public java.lang.String getAccountemail() {
		return accountemail;
	}
	public void setAccountemail(java.lang.String accountemail) {
		this.accountemail = accountemail;
	}
	public java.lang.String getAccountdesc() {
		return accountdesc;
	}
	public void setAccountdesc(java.lang.String accountdesc) {
		this.accountdesc = accountdesc;
	}
	public java.lang.String getAccountappid() {
		return accountappid;
	}
	public void setAccountappid(java.lang.String accountappid) {
		this.accountappid = accountappid;
	}
	public java.lang.String getAccountappsecret() {
		return accountappsecret;
	}
	public void setAccountappsecret(java.lang.String accountappsecret) {
		this.accountappsecret = accountappsecret;
	}
	public java.lang.String getAccountaccesstoken() {
		return accountaccesstoken;
	}
	public void setAccountaccesstoken(java.lang.String accountaccesstoken) {
		this.accountaccesstoken = accountaccesstoken;
	}
	public java.util.Date getAddtoekntime() {
		return addtoekntime;
	}
	public void setAddtoekntime(java.util.Date addtoekntime) {
		this.addtoekntime = addtoekntime;
	}
	public java.lang.String getUserName() {
		return userName;
	}
	public void setUserName(java.lang.String userName) {
		this.userName = userName;
	}
	public java.lang.String getJsapiticket() {
		return jsapiticket;
	}
	public void setJsapiticket(java.lang.String jsapiticket) {
		this.jsapiticket = jsapiticket;
	}
	public java.util.Date getJsapitickettime() {
		return jsapitickettime;
	}
	public void setJsapitickettime(java.util.Date jsapitickettime) {
		this.jsapitickettime = jsapitickettime;
	}
}
