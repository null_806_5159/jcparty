package edu.nuaa.party.weixin.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 图文消息
 * @author neil
 *
 */
public class ImageMessage extends BaseMessage{

	private int ArticleCount;
	private List<News> Articles = new ArrayList<News>();
	public int getArticleCount() {
		return ArticleCount;
	}
	public void setArticleCount(int articleCount) {
		ArticleCount = articleCount;
	}
	public List<News> getArticles() {
		return Articles;
	}
	public void setArticles(List<News> articles) {
		Articles = articles;
	}
}
