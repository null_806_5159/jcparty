package edu.nuaa.party.weixin.vo;
/**
 * 二维码
 * @author neil
 *
 */
public class QrcodeVo {

	private String ticket;//二维码ticket
	private String expire_seconds;//二维码有效时间
	private String url;//二维码图片解析后的地址
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getExpire_seconds() {
		return expire_seconds;
	}
	public void setExpire_seconds(String expire_seconds) {
		this.expire_seconds = expire_seconds;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
