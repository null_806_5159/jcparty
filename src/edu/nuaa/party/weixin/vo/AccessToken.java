package edu.nuaa.party.weixin.vo;

public class AccessToken {

	private String id;
	private String access_token;//	获取到的凭证
	private Long expires_in;//	凭证有效时间，单位：秒
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public Long getExpires_in() {
		return expires_in;
	}
	public void setExpires_in(Long expires_in) {
		this.expires_in = expires_in;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
