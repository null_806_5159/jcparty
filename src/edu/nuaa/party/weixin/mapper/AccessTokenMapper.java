package edu.nuaa.party.weixin.mapper;

import edu.nuaa.party.weixin.vo.AccessToken;

public interface AccessTokenMapper {

	AccessToken getAccessToken(String accessTokenId) throws Exception;

	void updateAccessToken(AccessToken accessToken) throws Exception;

	
}
